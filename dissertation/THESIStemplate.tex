%% File: THESIStemplate.tex
%% 
%% Author: Chuck Gartland
%% 
%% A LaTeX template file for masters theses in the College of Arts
%% and Sciences at Kent State University, created to be used with
%% the LaTeX class file `ksuthesis.cls' [2019/08/27 v4.0].
%% 
%% Usage: \documentclass[<options>]{ksuthesis}
%% 
%% Options:
%% 
%%    bound = set left margin to 1.5in (instead of default 1in)---useful
%%            if the thesis is to be printed and bound
%% 
%%   signed = produce a version of the thesis with a Signature Page
%%            suitable for signing, in which case names (advisor, chair,
%%            dean) are printed below the signature lines; otherwise,
%%            names are printed above the signature lines
%% 
%% `ksuthesis.cls' is built on top of the standard LaTeX `report' class,
%% to which all other options are passed.
%% 
%% The default options for `report.cls' (listed below) are used,
%% unless specified otherwise:
%% 
%%   letterpaper, 10pt, oneside, onecolumn, final, openany
%% 
%% Users are advised to read through the `ksuthesis.cls' file, which
%% is well commented, as well as the "Style Guide and Instructions for
%% Preparing Dissertations and Theses for Electronic Submission to
%% OhioLINK", College of Arts and Sciences, Kent State University.
%% 
%% Users are also advised to read a good introductory LaTeX book---the
%% original is
%% 
%%  "LaTeX: A Document Preparation System, 2/e", Leslie Lamport,
%%   Addison-Wesley, 1994
%% 
%% Users are further advised to use the suite of extensions created by
%% the American Mathematical Society for formatting high-quality
%% mathematical output (AMS-LaTeX).  These packages are available
%% in most LaTeX distributions.  Information can be found here:
%% 
%%   http://www.ams.org/publications/authors/tex/amslatex
%% 
%% An old web page of now-outdated (but perhaps still useful) material
%% that was created by a former Math Department Graduate Secretary:
%% 
%%   http://www.math.kent.edu/~mtackett/mathweb/latex/
%% 
%% History:
%% 
%%   2019/08/27: created by Chuck Gartland to accompany the revised
%%               class file `ksuthesis.cls'
%% 
%% Bugs: please communicate any bugs or suggestions to
%%       Chuck Gartland (gartland@math.kent.edu)

\documentclass[11pt]{ksuthesis}                        % 11pt looks best to me

%% Required definitions for (optional) ABSTRACT PAGE

\namedegree{GARTLAND, EUGENE C., JR., M.S.}    % caps, last name first

%% If \graddate{} (below) is omitted, then the next graduation date
%% will be generated by the class file.

\graddate{August 2019}                         % "month year", no comma

\program{Applied Mathematics}                  % will be converted to all caps

\title{Masters Thesis Template}                % will be converted to all caps

\pages{10}       % all pages (including frontmatter) are included in the count

\advisor{John Doe}

\coadvisor{Jane Doe}                                  % omit, if no co-advisor

%% Required definitions for TITLE PAGE (in addition to \title and
%% \submitdate above)

\author{Eugene C. Gartland, Jr.}

%% Required definitions for SIGNATURE PAGE (in addition to \author,
%% \advisor, and \coadvisor above)

\degrees{B.S., Purdue University, 1972 \\
         M.S., Kent State University, 2019}

\chair{Andrew M. Tonge}

\dept{Mathematical Sciences}

%% The currently valid default for \dean is set in the class file [8-27-19].
%% It may be overridden below.

% \dean{James L. Blank}

%% A List of Figures is required if there are more than 3 figures in
%% the thesis; it is optional if there are 1-3 figures; it is omitted
%% if there are no figures.  The TeX switch \iflistoffigures is set to
%% true in the `ksuthesis.cls' file, and so the default behavior is to
%% print a List of Figures.
%% 
%% To suppress the printing of a List of Figures page, use

\listoffiguresfalse

%% A List of Tables is required if there are more than 3 tables in
%% the thesis; it is optional if there are 1-3 tables; it is omitted
%% if there are no tables.  The TeX switch \iflistoftables is set to
%% true in the `ksuthesis.cls' file, and so the default behavior is to
%% print a List of Tables.
%% 
%% To suppress the printing of a List of Tables page, use

\listoftablesfalse

%% If using BibTeX, uncomment one of the lines below.  These are the 4
%% standard LaTeX bib styles.  No special bib style has been developed
%% to accompany ksuthesis.cls

% \bibliographystyle{plain}
% \bibliographystyle{unsrt}
% \bibliographystyle{alpha}
% \bibliographystyle{abbrv}

\begin{document}

%% (optional) Abstract Page.  Omit if an abstract page is not desired.
%% Must precede all other frontmatter if included.  Best not to leave
%% any blank lines after \begin{abstract}.

\begin{abstract}
  The text for the (optional) Abstract goes here.  If included, the
  abstract page must start on the first page of the document.  In
  addition to the text here, the information provided in the preamble
  of your \LaTeX\ source file for \verb|\namedegree|,
  \verb|\graddate|, \verb|\program|, \verb|\title|, \verb|\pages|,
  \verb|\advisor|, and \verb|\coadvisor| (if there is one) will be
  used to produce the abstract page(s).  No page numbers are to be
  printed on any abstract pages.
\end{abstract}

%% Most of the frontmatter (title page, signature page, table of
%% contents, list of figures, list of tables) is generated by the
%% command below.

\makefrontmatter

%% (optional) Preface.  Omit this if there is no Preface.

\begin{preface}
  The (optional) Preface goes here.
\end{preface}

%% (optional) Acknowledgments.  Omit this if there are no Acknowledgments.

\begin{acknowledgments}
  The (optional) Acknowledgments go here.
\end{acknowledgments}

%% The command below marks the break between the frontmatter and the
%% main thesis body.  It resets the page counter and switches the
%% page-numbering style from lowercase Roman numerals (frontmatter)
%% to Arabic numerals (main body). 

\mainmatter

\chapter{FIRST CHAPTER}

The material in the first chapter starts here.  This is the highest
level of sectioning for this \LaTeX\ class, since it is the highest
level of sectioning in the standard \LaTeX\ class \verb|report.cls|
(upon which this class is built).  Chapters are assigned numbers
starting from 1.

Note that whenever any major changes have been made to your \LaTeX\
source file, you will probably need to compile it \emph{twice}.  The
first time, \LaTeX\ will write out to auxiliary files information
about references, figures, tables, chapters, sections, and the like
(numbers, page numbers, captions, etc.).  The second time, \LaTeX\
will read the updated information from those files and use it to make
proper citation numbers, lists of figures and tables, table of
contents, etc.  See, for example, \cite{lamport:94} or
\cite{mittelbach:goossens:04}.

\section{First Section}

The material in the first section goes here.  This is the next level
of sectioning, below chapter.  Each section is assigned a number
associated with the parent chapter and is listed in the Table of
Contents.  The first paragraph of the section is not indented;
subsequent paragraphs are.\footnote{Footnotes look like this.  I've
  made them one-and-a-half spacing, because single spacing looked too
  small to me with the already-small font size used in footnotes.}

\subsection{First subsection}

The material in the first subsection goes here.  This is the next
level of sectioning, below section.  Each subsection is assigned a
number associated with the parent section and is listed in the Table
of Contents.  The first paragraph of the subsection is not indented;
subsequent paragraphs are.

\subsubsection{First subsubsection}

The material in the first subsubsection goes here.  This is the next
level of sectioning, below subsection.  Subsubsections are not
assigned numbers and are not listed in the Table of Contents.  The
first paragraph of the subsubsection is not indented; subsequent
paragraphs are.

\paragraph{First paragraph.}

The material in the first paragraph goes here.  This is the next level
of sectioning, below subsubsection.  Paragraphs are not assigned
numbers and are not listed in the Table of Contents.  The first line
of the paragraph is not indented.

\subparagraph{First subparagraph.}

The material in the first subparagraph goes here.  This is the next
level of sectioning, below paragraph (and the lowest level of
sectioning overall).  Subparagrpaphs are not assigned numbers and are
not listed in the Table of Contents.  The first line of the
subparagraph is indented.

%% If using BibTeX, then comment out the environment below, and
%% provide a \bibliographystyle{} (the 4 standard options are in the
%% preamble above) and a \bibliography{} (the test file template.bib
%% is invoked below).

\begin{thebibliography}{9}
\bibitem{lamport:94} Leslie Lamport.  \textit{\LaTeX: A Document
    Preparation System.}  Addison-Wesley, Reading, Massachusetts, 2nd
  edition, 1994
\bibitem{mittelbach:goossens:04} Frank Mittelbach and Michel
  Goossens.  \textit{The \LaTeX\ Companion.} Addison-Wesley, Boston,
  2nd edition, 2004
\end{thebibliography}

%% If using BibTeX (with test file template.bib), uncomment the line below

% \bibliography{template}

%% The command below changes numbering from CHAPTER 1, 2, etc. to
%% APPENDIX A, B, etc.

\appendix

\chapter{FIRST APPENDIX}

???

\section{First Section}

???

\subsection{First subsection}

???

\subsubsection{First subsubsection}

???

\paragraph{First paragraph.}

???

\subparagraph{First subparagraph.}

???

\end{document}
