set terminal png size 450,500 noenhanced font "Helvetica,20"
set output 'candlesticks-saill.png'
#set boxwidth 0.2 absolute
set xrange[0:4]
set yrange[0:50]
set ylabel "Number of CPU cycles"
set xlabel "Level"

# Data columns: X Min 1stQuartile Median 3rdQuartile Max BoxWidth Titles

# set bars 4.0
set style fill empty
plot 'saill.txt' using 1:3:2:6:5:7:xticlabels(8) with candlesticks title 'Quartiles' whiskerbars, \
  ''         using 1:4:4:4:4:7 with candlesticks lt -1 notitle
