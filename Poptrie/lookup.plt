set terminal png size 800,500 noenhanced font "Helvetica,20"
set output 'output2.png'

set yrange [0:160]
set style line 2 lc rgb 'black' lt 1 lw 1
set style data histogram
set style histogram cluster gap 1
set style fill pattern border -1
set boxwidth 0.5
set ylabel "Number of CPU cycles"
set xlabel "Level where the longest prefix is found"
set xtics format ""
set grid ytics

#set title "Average number of CPU cycle needed for per-packet lookup"
plot "lookup.dat" using 2:xtic(1) title 'SAIL_L' ls 2, \
            '' using 3 title "SAIL-BM" ls 2, \

