set terminal png size 800,500 noenhanced font "Helvetica,20"
set output 'memory.png'

set yrange [0:12]
set style line 2 lc rgb 'black' lt 1 lw 1
set style data histogram
set style histogram cluster gap 1
set style fill pattern border -1
set boxwidth 0.5
set ylabel "Memory consumption (in MB)"
set xlabel "FIB tables from backbone routers"
set xtics format ""
set grid ytics

#set title "Average number of CPU cycle needed for per-packet lookup"
plot "memory.dat" using 2:xtic(1) title 'Poptrie' ls 2, \
            '' using 3 title "PPC" ls 2, \

