#include <iostream>
#include <string>
using std::cout; using std::endl; using std::string;

enum claas NodeType {
  ROUTER,
  COMPUTE,
  SCRATCHPAD
};

//Abstract Node
class Node {
public:
  string    getId() const {return id_;}
  NodeType  getType() const {return type_;}
  Node(string id, NodeType type) {
    id_ = id;
    type_ = type;
  }   
protected:
  string   id_;//This is node's ID  
  NodeType type_;//This is node's type
};

//concrete node Router
class Router : public Node {
  public:
  Router (string id) : Node (id, ROUTER) {}
};

//concrete node Compute
class Compute : public Node {
  public:
  Compute (string id) : Node (id, COMPUTE){}
};

//concrete node Scratchpad
class Scratchpad : public Node {
  public:
  Scratchpad (string id) : Node (id, SCRATCHPAD){}
};

//Factory for all the nodes
class NodeFactory {
public:
  NodeFactory () {
    numRouter = 0;
    numCompute = 0;
    numScratchpad = 0;
  }
  int getRouterCount() const {return routerCount;}
  int getComputeCount() const {return computeCount;}
  int getScratchpadCount() const {return scratchpadCount;}
  
  Node* createCompute() {
    string id = "C" + computeCount++;
    return new Compute(id);
  }
  
  Node* createRouter() {
    string id = "R" + routerCount++;
    return new Router(id);
  }  

  Node* createScratchpad() {
    string id = "S" + scratchpadCount++;
    return new Scratchpad(id);
  }
private:
  int routerCount;//Number of routers created so far
  int computeCount;//Number of CU created so far
  int scratchpadCount;//Number of MU created so far
};
