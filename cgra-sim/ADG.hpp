#include "Node.hpp"

//Architecture dexcription graph
class ADG {
private:
  ADG (int numRow, int numCol) {
    //Construct CU and MU arrays
    PE.resize(numRow);
    for (int i = 0; i < numRow; i++) {
      bool isCompute = i & 1 == 0;
      for (int j = 0; j < numCol; j++, isCompute = !isCompute) {
        PE[i].push_back(isCompute ? nodeFactory.createCompute() : nodeFactory.createScratchpad());
      }
    }
    
    //Construct the routers
    NoC.resize(numRow + 1);
    for (int i = 0; i < numRow + 1; i++) {
      for (int j = 0; j < numCol; j++) {
        NoC[i].push_back(nodeFactory.createRouter());
      }
    }
  }
  
  void show () {
    for (auto pes : PE) {
      for (auto node : pes) {
        cout << node
      }
    }
  }
private:
  NodeFactory nodeFactory;
  int numRow;//Number of rows of CU.
  int numCol;//Number of cols of CU
  vector<vector<Node>> PE;
  vector<vector<Node>> NoC;
};
