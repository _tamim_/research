#include "Node.hpp"
using std::cout; using std::endl; using std::string;

//Abstract factory
class NodeFactory {
 public:
   NodeFactory();
   Node* requestNode() {
     return makeNode();
   }
   
   int getNumRouter() const {return numRouter;}
   int getNumCompute() const {return numCompute;}
   int getNumScratchpad() const {return numScratchpad;} 
 protected:
   virtual Node* makeNode() = 0;
   int numRouter;
   int numCompute;
   int numScratchpad;
};

class RouterFactory : public NodeFactory {
public:
  RouterFactory () {
  
  }
  
  Node* makeNode() override {
    Node* node = new Router();
    node->id_ = "R" + numRouter;
    numRouter++;
    return node;
  }
};

class ComputeFactory : public NodeFactory {
public:
  ComputeFactory () {
  
  }
  
  Node* makeNode() override {
    Node* node = new Compute();
    node->id_ = "C" + numCompute;
    numCompute++;
    return node;
  }
};


class ScratchpadFactory : public NodeFactory {
public:
  ScratchpadFactory () {
  
  }
  
  Node* makeNode() override {
    Node *node = new Scratchpad();
    node->id_ = "S" + numScratchpad;
    numScratchpad++;
    return node;
  }
};
