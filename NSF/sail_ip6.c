#include <stdint.h>

uint8_t N16[2000]; uint16_t C16[2000]; //level 16
uint8_t N24[2000]; uint32_t C24[2000]; //level 24
uint8_t N32[2000]; uint32_t C32[2000]; //level 32
uint8_t N40[2000]; uint32_t C40[2000]; //level 40
uint8_t N48[2000]; uint32_t C48[2000]; //level 48

int8_t sail (uint64_t ip)
{
  uint32_t idx = ip >> 48;
  if (C16[idx]) {
    idx = ((C16[idx] - 1) << 8) + ((ip >> 40) & 0XFF);
    if (C24[idx]) {
      idx = ((C24[idx] - 1) << 8) + ((ip >> 32) & 0XFF);
      if (C32[idx]) {
        idx = ((C32[idx] - 1) << 8) + ((ip >> 24) & 0XFF);
        if (C40[idx]) {
          idx = ((C40[idx] - 1) << 8) + ((ip >> 16) & 0XFF);
          return N48[idx];
        } else {
          return N40[idx];
        }
      } else {
        return N32[idx];
      }
    } else {
      return N24[idx];
    }
  } else {
    return N16[idx];
  }
}




