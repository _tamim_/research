//including the header file (library)
#include <iostream>
using namespace std;

//using std::cout; using std::endl; using std::cin;

//main function
int main () {

  //variable declaration
  // {type} {variable_name};
  //{type} = int (3), float (3.4), char ('a'), double (33333333.333333333333333), bool (true/false)
  int a;
  int b;
  int sum;

  //multiple variable declaration
  int m, n, p, A;

  // Valid variable names
  int abb, _abbb, ab_10, a10;

  //invalid variable names
  //int 3a, ag(g, arr\;

  //varibale inialization
  int test_int = 7;
  float test_float = 4.666;
  char ch = 'a';
  double d = 555555.66666;
  bool test_b = true;

  //cout -- console out (print something)
  //endl -- end line
  cout << "Hello word !" << endl;
  cout << "Welcome to CS1 !" << endl;

  //\n is same as endl
  cout << "Creating new line \n";

  cout << "test_float = " << test_float << " d = " << d << endl;

  cout << "Enter a number = ";
  //cin -- console in (input)
  cin >> a;
  cout << "Enter another number = ";
  cin >> b;

  sum = a + b;
  cout << "The sum is = " << sum << endl;

  //multiplication
  int mul = a * b; 
  cout << "The multiplication is = " << mul << endl;

  //division
  float div = (float)a / b;//not a % b
  cout << "The division is = " << div << endl;

  //modulus
  int mod = a % b;
  cout << "The modulus is = " << mod << endl;
}
