#include <iostream>
using namespace std;
//using std::cout; using std::endl; using std::cin;

int main () {
  int a, b, c, d, m;
  int div;

  cout << "Enter a = ";
  cin >> a;
  cout << "Enter b = ";
  cin >> b;
  cout << "Enter c = ";
  cin >> c;
  cout << "Enter d = ";
  cin >> d;
  cout << "Enter m = ";
  cin >> m;

  //integer division
  div = a / c;
  cout << "div = " << div << endl; 

  float f = (float)a / c;
  cout << "f = " << f << endl; 

  if (d != 0) 
  {
    div = a / d;
    cout << "div = " << div << endl; 
  }

  if (a % 2 == 0)
    cout << "a is even" << endl;
  else
    cout << "a is odd" << endl;

  if (a > b) {
    if (a > c) {
      cout << "a is the largest" << endl;
    } else {
      cout << "c is the largest" << endl;
    }
  } else {
    if (b > c) {
       cout << "b is the largest" << endl;
    } else {
       cout << "c is the largest" << endl;
    }
  }

  if (m == 0) {
    cout << "Zero\n";
  } else if (m == 1) {
    cout << "One\n";
  } else if (m == 2) {
    cout << "Two\n";
  } else if (m == 3) {
    cout << "Three\n";
  } else if (m == 4) {
    cout << "Four\n";
  } else if (m == 5) {
    cout << "Five\n";
  }

  cout << "Testing switch " << endl;
  switch (m) {
    case 0:
      cout << "Zero\n";
      break;
    case 1:
      cout << "One\n";
      break;
    case 2:
      cout << "Two\n";
      break;
    case 3:
      cout << "Three\n";
      break;
    case 4:
      cout << "Four\n";
      break;
    case 5:
      cout << "Five\n";
      break;
  }
}

/*
m: 0 -> 5
m == 0 ---- "Zero"
m == 1 ---- "One"
m == 2 ----  "Two"
..
..
m == 5 --  "Five"
*/
/*
5%2 --- 1
8%2 ---- 0
*/
//multiline comment
/*
=    --   assignment
==   --   Equality
!=   --   Not Equal
*/

