// functor example
// Mikhail Nesterenko
// 3/30/2014

#include <iostream>
#include <functional>
using std::cout; using std::endl;
 
class MyFunctor{
public:
   MyFunctor(int x) : x_(x) {}
   // overloading function operator
   int operator() (int y) {return x_+=y;} 
private:
   int x_;
};
 
int main(){
   MyFunctor addThem(20); // creating a functor object
                          // initializing to one

   // "invoking" it like a regular function
   cout << addThem(3) << ' ';
   cout << addThem(5) << ' ';
   cout << addThem(100) << endl;

   /*
   auto b = std::bind(addThem, 55);
   cout << b() << endl;
   */
}
