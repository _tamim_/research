#include <iostream>

using std::cout; using std::endl; using std::string;

class course;

class student {
public:
  string name;
  int student_id;
  course *c;

  student (string &n, int id) : name(n), student_id(id) {
  }

  student (const student &rhs) {
    name = rhs.name;
    student_id = rhs.student_id;
    //c = rhs.c;//shallow copy
    c = new course(rhs.c->name);//deep copy
  }

  student& operator=(const student &rhs) {
    //prevent self assignment
    if (this == &rhs)
      return *this;

    name = rhs.name;
    stident_id = rhs.stident_id;
    delete c;
    c = new course(rhs.c->name);
  }


  friend void swap(student& first, student& second){
     std::swap(first.name,  second.name);
     std::swap(first.stident_id,  second.stident_id);
     std::swap(first.c, second.c);
  }

  student& operator=(const student rhs) {
    std::swap(*this, rhs);
    return *this;
  } 

  void add_course (string &s) {  
    this->c = new Course (s);
  }

public:
  string get_name ();
  void set_name (string n);
};

class course {
public:
  string name;
  course (string &s): name (s){} 
}


template<typename T>
T add (T a, T b) {
  return a + b;
}

add<int>(5, 10.5);

add (5, 10);
add (5.1, 10.5);
add ('a', 'b');


template <typename T>
MyClass{ 
public:
  T get_b();
/*
  //inline function
  T get_b() {
    return b;
  }
*/ 
private: 
  int a; 
  T b; 
  T *c; 
};


template <typename T> 
T MyClass<T>::get_b() {
  return b;
}






hello world
hello,world
hello kent


wordList
----------------------
hello 3
world 2
kent 1

























