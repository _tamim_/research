#include <fstream>
#include <iostream>
#include <vector>
#include <list>
#include <string>
#include <cstdlib>

using std::ifstream;
using std::string; using std::getline;
using std::list; using std::vector;
using std::cout; using std::endl;
using std::move;

int main () {
/*
  vector<int> vec = {1, 2, 3};

  for(vector<int>::iterator it = vec.begin(); it != vec.end(); ++it)
    cout << *it;
  cout << endl;

  for(auto it = vec.begin(); it != vec.end(); ++it)
    cout << *it;
  cout << endl;

  for(auto e : vec)
    cout << e;
  cout << endl;

  for (int i = 0; i < vec.size(); i++)
    cout << vec[i];
  cout << endl;

  vec.resize(10);
  vec[8] = 9;

  for(auto& e : vec)
    cout << e;
  cout << endl;

  vector<int>::iterator it = vec.begin() + 8;
  cout << it - vec.begin() << "th element is " << *it << endl;

  for(vector<int>::iterator it = vec.begin(); it != vec.end(); ++it)
    *it += 2;

  for(vector<int>::const_iterator it = vec.begin(); it != vec.end(); ++it)
    cout << *it;
  cout << endl;

 for(auto it=vec.cbegin(); it != vec.cend(); ++it)
    cout << *it;
  cout << endl;

  vec.push_back (11);
  vec.push_back (11);
  vec.push_back (11);
  vec.push_back (11);
  vec.push_back (11);

  //vec.pop_back (11);

  for(auto& e : vec)
    cout << e;
  cout << endl;

  it = vec.begin () + 1;
  it = vec.insert (it, 200);
  vec.insert (it , 5, 8);

  for(auto& e : vec)
    cout << e;
  cout << endl;


  vector<int> v1 = {40, 50, 60};
  vec.insert (vec.begin() , v1.begin(), v1.end());

  for(auto& e : vec)
    cout << e;
  cout << endl;

  vec.erase (vec.begin());
  vec.erase (vec.begin(), vec.end() - 1);
  for(auto& e : vec)
    cout << e;
  cout << endl;

//  vec.clear();

*/

/*

  vector <int> v = {1, 5, 10, 15, 20}; 

  for (auto it=v.begin();it!=v.end();it++) {
    if ((*it) == 5) { 
      it = v.insert(it+1, -1); 
    }
  }

  for (auto it=v.begin();it!=v.end();it++) 
    cout << (*it) << " ";
  cout << endl;

*/

/*
  vector <int> v = {1, 5, 10, 15, 20}; 

  for(auto it=v.begin(); it != v.end(); )
    if (*it == 5)
      it = v.erase(it);
    else
      ++it;

  for (auto it=v.begin();it!=v.end();it++) 
    cout << (*it) << " ";
  cout << endl;
*/

/*
  // Changing vector while iterating over it 
  // (This causes iterator invalidation) 
  for (auto it=v.begin();it!=v.end();it++) 
    if ((*it) == 5) 
      v.push_back(-1); 

  for (auto it=v.begin();it!=v.end();it++) 
    cout << (*it) << " ";
  cout << endl;
*/

/*

  vector<int> v(10, 1);
  auto it=v.end();
  *it = 20;
  for (auto& e : v)
    cout << e << endl;
*/

/*
  vector<int> one(5), two(5);
  for(auto it = one.begin(); it != two.end(); ++it)
    cout << *it << endl;  
*/


  vector<int> v = {5, 6}, v2 = {1,2,3,4};

  cout << "v = ";
  for (auto& e : v)
    cout << e;
  cout << endl;

  cout << "v2 = ";
  for (auto& e : v2)
    cout << e;
  cout << endl;

  v = std::move(v2);

  cout << "v = ";
  for (auto& e : v)
    cout << e;
  cout << endl;

  cout << "v2 = ";
  for (auto& e : v2)
    cout << e;
  cout << endl;

}


