// demonstrates two versions of transform
// Mikhail Nesterenko
// 04/01/2014

#include <algorithm>
#include <iostream>
#include <vector>
#include <ctime>

using std::cout; using std::endl;
using std::vector;

int main() {
   vector<int> v1(10), v2(10), v3(10);

   srand(static_cast<unsigned>(time(nullptr)));

   generate(v1.begin(), v1.end(), []{return rand()%10;});

   generate(v2.begin(), v2.end(), []{return rand()%10;});

   cout << "v1 contents: "; 
   for(auto e: v1) cout << e << " "; 
   cout << endl;

   cout << "v2 contents: "; 
   for(auto e: v2) cout << e << " "; 
   cout << endl;

//   transform(v1.begin(), v1.end(), v2.begin(), v3.begin(), std::plus<int>());
   transform(v1.begin(), v1.end(), v2.begin(), v3.begin(), [](int a, int b){return std::max(a, b);});

   cout << "v3 contents after two-range transform(): "; 
   for(auto e: v3) 
      cout << e << " "; 
   cout << endl;
}
