// STL find_if algorithm, using function pointer as a callback
// Mikhail Nesterenko
// 3/18/2014

#include <iostream>
#include <ctime>
#include <cstdlib>
#include <vector>
#include <algorithm>

using std::cout; using std::endl; using std::cin;
using std::vector;

bool passingScore(int s){
   return s >= 50;
}

int main(){

   // auto fp = passingScore;


   // if (fp(60)) cout << "60 is greater than 50" << endl;

   srand(time(nullptr));
   vector <int> vect;

   for(int i=0; i < 10; ++i) vect.push_back(rand()%100);

   cout << "Enter a number: ";
   int num;
   cin >> num;  

   auto it=find_if(vect.begin(),vect.end(), [&]{return s >= num;});

   if(it != vect.end()){
      cout << "Found it at position(s): ";
      do{
         cout << it - vect.begin() << " ";
         it=find_if(++it, vect.end(), passingScore);
      }while(it != vect.end());
   }else
      cout << "Not found it ";


   cout << "in vector ";
   for(auto e: vect) cout << e << " ";
   cout<< endl;
}
