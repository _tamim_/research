#include <iostream>
#include "student.h"

using std::cout; using std::endl; using std::string;

int main(){
  string s("John doe");
  student ob (s, 5);
  ob.set_name(s);
  cout << "Student's name is " << ob.get_name() << endl;
}
