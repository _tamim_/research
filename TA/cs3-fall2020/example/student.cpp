#include "student.h"

string student::get_name() {
  return name;
}

void student::set_name (string n) {
  name = n;
}

/*
int add (int a, int b) {
  return a + b;
}

float add (float a, float b) {
  return a + b;
}


double add (double a, double b) {
  return a + b;
}
*/

template<typename T>
T add (T a, T b) {
  return a + b;
}

add<int>(5, 10.5);

add (5, 10);
add (5.1, 10.5);
add ('a', 'b');


template <typename T>
MyClass{ 
public:
  T get_b();
/*
  T get_b() {
    return b;
  }
*/ 
private: 
  int a; 
  T b; 
  T *c; 
};
