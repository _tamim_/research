
class Visitor {

enum Type {Inspector, Operator};
...
....
}

class CarPart {

enum Type {Wheel, Engine, Transmission};
...
....
}

void inspect (Visitor *v, CarPart *c)
{
  if (v->getType() == Visitor::Inspector && c->getType() == CarPart::Engine) {
    cout << "Inspector is inpecting Engine " << endl;
  } else if (v->getType() == Visitor::Operator && c->getType() == CarPart::Engine) {
    cout << "Operator is inpecting Engine " << endl;
  } else if (v->getType() == Visitor::Inspector && c->getType() == CarPart::Wheel) {
    cout << "Inspector is inpecting Wheel " << endl;
  } else if (v->getType() == Visitor::Operator && c->getType() == CarPart::Wheel) {
    cout << "Operator is inpecting Wheel " << endl;
  } else if....
   ....
}

Jones, Donny
Hoang M. Le 	hle10@kent.edu
