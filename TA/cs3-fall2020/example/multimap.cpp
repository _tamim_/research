// multimap examples
// Mikhail Nesterenko
// 3/16/2014

#include <iostream>
#include <map>
#include <cstdlib>
#include <ctime>

using std::cout; using std::endl;
using std::make_pair; using std::multimap;

#define m1 0x5555555555555555 //binary: 0101...
#define m2 0x3333333333333333 //binary: 00110011..
#define m4 0x0f0f0f0f0f0f0f0f //binary:  4 zeros,  4 ones ...
#define m8 0x00ff00ff00ff00ff //binary:  8 zeros,  8 ones ...
#define m16 0x0000ffff0000ffff //binary: 16 zeros, 16 ones ...
#define m32 0x00000000ffffffff //binary: 32 zeros, 32 ones
#define h01 0x0101010101010101 //the sum of 256 to the power of 0,1,2,3...

#define POPCNT64(x) ((((((((((x) - (((x) >> 1) & m1)) & m2) + ((((x) - (((x) >> 1) & m1)) >> 2) & m2)) + (((((x) - (((x) >> 1) & m1)) & m2) + ((((x) - (((x) >> 1) & m1)) >> 2) & m2)) >> 4)) & m4) + (((((((x) - (((x) >> 1) & m1)) & m2) + ((((x) - (((x) >> 1) & m1)) >> 2) & m2)) + (((((x) - (((x) >> 1) & m1)) & m2) + ((((x) - (((x) >> 1) & m1)) >> 2) & m2)) >> 4)) & m4) >>  8)) + ((((((((x) - (((x) >> 1) & m1)) & m2) + ((((x) - (((x) >> 1) & m1)) >> 2) & m2)) + (((((x) - (((x) >> 1) & m1)) & m2) + ((((x) - (((x) >> 1) & m1)) >> 2) & m2)) >> 4)) & m4) + (((((((x) - (((x) >> 1) & m1)) & m2) + ((((x) - (((x) >> 1) & m1)) >> 2) & m2)) + (((((x) - (((x) >> 1) & m1)) & m2) + ((((x) - (((x) >> 1) & m1)) >> 2) & m2)) >> 4)) & m4) >>  8)) >> 16)) + (((((((((x) - (((x) >> 1) & m1)) & m2) + ((((x) - (((x) >> 1) & m1)) >> 2) & m2)) + (((((x) - (((x) >> 1) & m1)) & m2) + ((((x) - (((x) >> 1) & m1)) >> 2) & m2)) >> 4)) & m4) + (((((((x) - (((x) >> 1) & m1)) & m2) + ((((x) - (((x) >> 1) & m1)) >> 2) & m2)) + (((((x) - (((x) >> 1) & m1)) & m2) + ((((x) - (((x) >> 1) & m1)) >> 2) & m2)) >> 4)) & m4) >>  8)) + ((((((((x) - (((x) >> 1) & m1)) & m2) + ((((x) - (((x) >> 1) & m1)) >> 2) & m2)) + (((((x) - (((x) >> 1) & m1)) & m2) + ((((x) - (((x) >> 1) & m1)) >> 2) & m2)) >> 4)) & m4) + (((((((x) - (((x) >> 1) & m1)) & m2) + ((((x) - (((x) >> 1) & m1)) >> 2) & m2)) + (((((x) - (((x) >> 1) & m1)) & m2) + ((((x) - (((x) >> 1) & m1)) >> 2) & m2)) >> 4)) & m4) >>  8)) >> 16)) >> 32)) & 0x7f)

int main () {
 
  int i;

  i = POPCNT64(0X29492491249);
  cout << i << endl;
  i = POPCNT64(0xFFFFFFFFFFF);
  cout << i << endl;
  i = POPCNT64(31);
  cout << i << endl;

}
