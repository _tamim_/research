set terminal png size 800,300 noenhanced font "Helvetica,17"
set output 'memory.png'

set key inside top right horizontal font "Helvetica, 18" width 1

set yrange [0:8]
set style line 2 lc rgb 'black' lt 1 lw 1
set style data histogram
set style histogram cluster gap 1
set style fill pattern border -1
set boxwidth 0.5
set ylabel "Memory consumption (MB)"
#set xlabel "FIB tables from backbone routers"
set xtics format ""
set grid ytics

#set title "Average number of CPU cycle needed for per-packet lookup"
plot "memory.dat" using 3:xtic(1) title 'Poptrie' ls 2, \
            '' using 4 title "CP-Trie" ls 2, \

#Previously it was as following
#plot "memory.dat" using 2:xtic(1) title 'SAIL_L' ls 2, \
#            '' using 3 title "Poptrie" ls 2, \
#            '' using 4 title "CP-Trie" ls 2, \

