########Prefix traffic####################################################

set terminal png size 850,330 noenhanced font "Helvetica,20"
set output 'lookup-throughput-prefix.png'

set key inside top right horizontal font "Helvetica, 20" width 1

set yrange [0:50]
set style line 2 lc rgb 'black' lt 1 lw 1
set style data histogram
set style histogram cluster gap 1
set style fill pattern border -1
set boxwidth 0.5
set ylabel "Lookup throughput (Mlps)"
#set xlabel "Level where the longest prefix is found"
set xtics format ""
set grid ytics

#set title "Average number of CPU cycle needed for per-packet lookup"
plot "lookup-throughput-prefix.dat" using 3:xtic(1) title 'Poptrie' ls 2, \
            '' using 4 title "CP-Trie" ls 2, \

#Previously it was
#plot "lookup-throughput-prefix.dat" using 2:xtic(1) title 'SAIL_L' ls 2, \
#            '' using 3 title "Poptrie" ls 2, \
#            '' using 4 title "CP-Trie" ls 2, \


########Repeated traffic####################################################

set terminal png size 850,330 noenhanced font "Helvetica,20"
set output 'lookup-throughput-repeated.png'

set key inside top right horizontal font "Helvetica, 20" width 1

set yrange [0:50]
set style line 2 lc rgb 'black' lt 1 lw 1
set style data histogram
set style histogram cluster gap 1
set style fill pattern border -1
set boxwidth 0.5
set ylabel "Lookup throughput (Mlps)"
#set xlabel "Level where the longest prefix is found"
set xtics format ""
set grid ytics

#set title "Average number of CPU cycle needed for per-packet lookup"
plot "lookup-throughput-repeated.dat" using 3:xtic(1) title 'Poptrie' ls 2, \
            '' using 4 title "CP-Trie" ls 2, \

#Previously it was
#plot "lookup-throughput-repeated.dat" using 2:xtic(1) title 'SAIL_L' ls 2, \
#            '' using 3 title "Poptrie" ls 2, \
#            '' using 4 title "CP-Trie" ls 2, \


########Real traffic####################################################

set terminal png size 850,330 noenhanced font "Helvetica,20"
set output 'lookup-throughput-real.png'

set key inside top right horizontal font "Helvetica, 20" width 1

set yrange [0:360]
set style line 2 lc rgb 'black' lt 1 lw 1
set style data histogram
set style histogram cluster gap 1
set style fill pattern border -1
set boxwidth 0.5
set ylabel "Lookup throughput (Mlps)"
#set xlabel "Level where the longest prefix is found"
set xtics format ""
set grid ytics

#set title "Average number of CPU cycle needed for per-packet lookup"
plot "lookup-throughput-real.dat" using 2:xtic(1) title 'SAIL_L' ls 2, \
            '' using 3 title "Poptrie" ls 2, \
            '' using 4 title "CP-Trie" ls 2, \

########Random traffic####################################################

set terminal png size 850,330 noenhanced font "Helvetica,20"
set output 'lookup-throughput-random.png'

set key inside top right horizontal font "Helvetica, 20" width 1

set yrange [0:410]
set style line 2 lc rgb 'black' lt 1 lw 1
set style data histogram
set style histogram cluster gap 1
set style fill pattern border -1
set boxwidth 0.5
set ylabel "Lookup throughput (Mlps)"
#set xlabel "Level where the longest prefix is found"
set xtics format ""
set grid ytics

#set title "Average number of CPU cycle needed for per-packet lookup"
plot "lookup-throughput-random.dat" using 3:xtic(1) title 'Poptrie' ls 2, \
            '' using 4 title "CP-Trie" ls 2, \

#Previously it was
#plot "lookup-throughput-random.dat" using 2:xtic(1) title 'SAIL_L' ls 2, \
#            '' using 3 title "Poptrie" ls 2, \
#            '' using 4 title "CP-Trie" ls 2, \


########Sequential traffic####################################################

set terminal png size 850,330 noenhanced font "Helvetica,20"
set output 'lookup-throughput-sequential.png'

set key inside top right horizontal font "Helvetica, 20" width 1

set yrange [0:20]
set style line 2 lc rgb 'black' lt 1 lw 1
set style data histogram
set style histogram cluster gap 1
set style fill pattern border -1
set boxwidth 0.5
set ylabel "Lookup throughput (Mlps)"
#set xlabel "Level where the longest prefix is found"
set xtics format ""
set grid ytics

#set title "Average number of CPU cycle needed for per-packet lookup"
plot "lookup-throughput-sequential.dat" using 3:xtic(1) title 'Poptrie' ls 2, \
            '' using 4 title "CP-Trie" ls 2, \

#Previously it was
#plot "lookup-throughput-sequential.dat" using 2:xtic(1) title 'SAIL_L' ls 2, \
#            '' using 3 title "Poptrie" ls 2, \
#            '' using 4 title "CP-Trie" ls 2, \

########Decreasing traffic####################################################

set terminal png size 850,330 noenhanced font "Helvetica,20"
set output 'lookup-throughput-decreasing.png'

set key inside top right horizontal font "Helvetica, 20" width 1

set yrange [0:225]
set style line 2 lc rgb 'black' lt 1 lw 1
set style data histogram
set style histogram cluster gap 1
set style fill pattern border -1
set boxwidth 0.5
set ylabel "Lookup throughput (Mlps)"
#set xlabel "Level where the longest prefix is found"
set xtics format ""
set grid ytics

#set title "Average number of CPU cycle needed for per-packet lookup"
plot "lookup-throughput-decreasing.dat" using 2:xtic(1) title 'SAIL_L' ls 2, \
            '' using 3 title "Poptrie" ls 2, \
            '' using 4 title "CP-Trie" ls 2, \

########Matched Prefix length CP-Trie####################################################

set terminal png size 850,330 noenhanced font "Helvetica,20"
set output 'matched-prefix-len-cptrie.png'

set key inside top right horizontal font "Helvetica, 17" width 1

set yrange [0:150]
set style line 2 lc rgb 'black' lt 1 lw 1
set style data histogram
set style histogram cluster gap 1
set style fill pattern border -1
set boxwidth 0.5
set ylabel "Avg. Prefix length"
#set xlabel "Level where the longest prefix is found"
set xtics format ""
set grid ytics

#set title "Average number of CPU cycle needed for per-packet lookup"
plot "matched-prefix-len-cptrie.dat" using 2:xtic(1) title 'Prefix' ls 2, \
            '' using 3 title "Sequential" ls 2, \
            '' using 4 title "Decreasing" ls 2, \
            '' using 5 title "Real" ls 2, \
            '' using 6 title "Random" ls 2, \


########Matched Prefix length Poptrie####################################################

set terminal png size 850,330 noenhanced font "Helvetica,20"
set output 'matched-prefix-len-poptrie.png'

set key inside top right horizontal font "Helvetica, 17" width 1

set yrange [0:150]
set style line 2 lc rgb 'black' lt 1 lw 1
set style data histogram
set style histogram cluster gap 1
set style fill pattern border -1
set boxwidth 0.5
set ylabel "Avg. Prefix length"
#set xlabel "Level where the longest prefix is found"
set xtics format ""
set grid ytics

#set title "Average number of CPU cycle needed for per-packet lookup"
plot "matched-prefix-len-poptrie.dat" using 2:xtic(1) title 'Prefix' ls 2, \
            '' using 3 title "Sequential" ls 2, \
            '' using 4 title "Decreasing" ls 2, \
            '' using 5 title "Real" ls 2, \
            '' using 6 title "Random" ls 2, \

