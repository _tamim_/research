
set interfaces lo0 unit 0 family inet filter input f15
set interfaces lo0 unit 0 family inet address 1.0.0.2/32
set interfaces lo0 unit 0 family inet address 224.0.0.2/32

set firewall family inet filter f15 term 1 from destination-address 224.0.0.2/32
set firewall family inet filter f15 term 1 from protocol udp
set firewall family inet filter f15 term 1 from destination-port 646
set firewall family inet filter f15 term 1 then count c1
set firewall family inet filter f15 term 1 then accept
set firewall family inet filter f15 term 2 then count incoming
set firewall family inet filter f15 term 2 then log
set firewall family inet filter f15 term 2 then accept

mdi@buildcontainer:/baas/mdi/auckland/master$ sb repo diff

project forwarding/exprplus/
diff --git a/applications/common/src/flexbytes/JexprFlexbytesApp.cpp b/applications/common/src/flexbytes/JexprFlexbytesApp.cpp
index 9078792c4..ed3b91789 100644
--- a/applications/common/src/flexbytes/JexprFlexbytesApp.cpp
+++ b/applications/common/src/flexbytes/JexprFlexbytesApp.cpp
@@ -72,13 +72,12 @@ static constexpr uint64_t JEXPR_FLEXBYTES_BIT_MASK_ALL = 0xff;
 //
 static constexpr uint64_t JEXPR_FLEXBYTES_NIBBLE_SHIFT_BY_1 = 0x2;
 static constexpr uint64_t JEXPR_FLEXBYTES_NIBBLE_SHIFT_BY_2 = 0x4;
-static constexpr uint64_t JEXPR_FLEXBYTES_NIBBLE_SHIFT_BY_3 = 0x8;
+static constexpr uint64_t JEXPR_FLEXBYTES_NIBBLE_SHIFT_BY_3 = 0x6;
 
 static constexpr uint64_t JEXPR_FLEXBYTES_L2TP_NIBBLE_SHIFT_0 = 0xe;
 static constexpr uint64_t JEXPR_FLEXBYTES_L2TP_NIBBLE_SHIFT_1 = 0xc;
 static constexpr uint64_t JEXPR_FLEXBYTES_L2TP_NIBBLE_SHIFT_2 = 0xa;
-static constexpr uint64_t JEXPR_FLEXBYTES_L2TP_NIBBLE_SHIFT_3
-	= JEXPR_FLEXBYTES_NIBBLE_SHIFT_BY_3;
+static constexpr uint64_t JEXPR_FLEXBYTES_L2TP_NIBBLE_SHIFT_3 = 0x8;
 static constexpr uint64_t JEXPR_FLEXBYTES_L2TP_NIBBLE_SHIFT_4 = 0x6;
 static constexpr uint64_t JEXPR_FLEXBYTES_L2TP_NIBBLE_SHIFT_5
         = JEXPR_FLEXBYTES_NIBBLE_SHIFT_BY_2;

set firewall family inet filter f1 term 1 from protocol gre
set firewall family inet filter f1 term 1 from gre-key 4294967295
set firewall family inet filter f1 term 1 then count c1
set firewall family inet filter f1 term 1 then accept
set firewall family inet filter f1 term 2 then count incoming
set firewall family inet filter f1 term 2 then log
set firewall family inet filter f1 term 2 then accept

set firewall family inet filter f3 term 1 from protocol gre
set firewall family inet filter f3 term 1 from gre-key 16777215
set firewall family inet filter f3 term 1 then count c1
set firewall family inet filter f3 term 1 then accept
set firewall family inet filter f3 term 2 then count incoming
set firewall family inet filter f3 term 2 then log
set firewall family inet filter f3 term 2 then accept

[pe.slu info] pkt:1789934: gre_key = ffffffff
======================================================
[pe.flt info] pkt:1789934:  flexbytes=0x000000ff00ffffff


[pe.flt info] pkt:1789934: Flt::assemble_alpha_beta_search_argument: [40] FLEX_BYTES[15: 0]=0xffff
[pe.flt info] pkt:1789934: BETA facet 0 search argument 0xffff00
[pe.flt info] pkt:1789934: Flt::do_beta key=0xffff00
[pe.flt info] pkt:1789934: Flt::do_beta bank=2, node=12, index=0, type=TL_FLT_NODE_MEMBER_TYPE_MINUS_NODE, cond=0xffff00
[pe.flt info] pkt:1789934: Flt::do_beta go right
[pe.flt info] pkt:1789934: Flt::do_beta update result
[pe.flt info] pkt:1789934: Flt::do_beta bank=2, node=12, index=2, type=TL_FLT_NODE_MEMBER_TYPE_INVLD_NODE, cond=0x000000
[pe.flt info] pkt:1789934: Flt::do_beta go left
[pe.flt info] pkt:1789934: BETA facet 0 match TL_FLT_FCV_TYPE_BITS_16_RAW_0 @ 1
[pe.flt info] pkt:1789934: Flt::assemble_alpha_beta_search_argument: [39] FLEX_BYTES[31:16]=0xff00
[pe.flt info] pkt:1789934: BETA facet 1 search argument 0x00ff00
[pe.flt info] pkt:1789934: Flt::do_beta key=0x00ff00
[pe.flt info] pkt:1789934: Flt::do_beta bank=2, node=18, index=0, type=TL_FLT_NODE_MEMBER_TYPE_MINUS_NODE, cond=0xffff00
[pe.flt info] pkt:1789934: Flt::do_beta go left
[pe.flt info] pkt:1789934: Flt::do_beta bank=2, node=18, index=1, type=TL_FLT_NODE_MEMBER_TYPE_MINUS_NODE, cond=0x000000
[pe.flt info] pkt:1789934: Flt::do_beta go right
[pe.flt info] pkt:1789934: Flt::do_beta update result
[pe.flt info] pkt:1789934: BETA facet 1 match TL_FLT_FCV_TYPE_BITS_16_RAW_0 @ 0

[pe.slu info] pkt:5238769: gre_key = ffffff
========================================================
[pe.flt info] pkt:5238769:  flexbytes=0x0000000000ffffff

[pe.flt info] pkt:5238769: Flt::assemble_alpha_beta_search_argument: [40] FLEX_BYTES[15: 0]=0xffff
[pe.flt info] pkt:5238769: BETA facet 0 search argument 0xffff00
[pe.flt info] pkt:5238769: Flt::do_beta key=0xffff00
[pe.flt info] pkt:5238769: Flt::do_beta bank=2, node=12, index=0, type=TL_FLT_NODE_MEMBER_TYPE_MINUS_NODE, cond=0xffff00
[pe.flt info] pkt:5238769: Flt::do_beta go right
[pe.flt info] pkt:5238769: Flt::do_beta update result
[pe.flt info] pkt:5238769: Flt::do_beta bank=2, node=12, index=2, type=TL_FLT_NODE_MEMBER_TYPE_INVLD_NODE, cond=0x000000
[pe.flt info] pkt:5238769: Flt::do_beta go left
[pe.flt info] pkt:5238769: BETA facet 0 match TL_FLT_FCV_TYPE_BITS_16_RAW_0 @ 1
[pe.flt info] pkt:5238769: Flt::assemble_alpha_beta_search_argument: [39] FLEX_BYTES[31:16]=0xff00
[pe.flt info] pkt:5238769: BETA facet 1 search argument 0x00ff00
[pe.flt info] pkt:5238769: Flt::do_beta key=0x00ff00
[pe.flt info] pkt:5238769: Flt::do_beta bank=2, node=18, index=0, type=TL_FLT_NODE_MEMBER_TYPE_MINUS_NODE, cond=0x00ff00
[pe.flt info] pkt:5238769: Flt::do_beta go right
[pe.flt info] pkt:5238769: Flt::do_beta update result
[pe.flt info] pkt:5238769: Flt::do_beta bank=2, node=18, index=2, type=TL_FLT_NODE_MEMBER_TYPE_MINUS_NODE, cond=0x010000
[pe.flt info] pkt:5238769: Flt::do_beta go left
[pe.flt info] pkt:5238769: BETA facet 1 match TL_FLT_FCV_TYPE_BITS_16_RAW_0 @ 1

root@EVOvSCAPA1-RE0-fpc1:pfe> show sandbox token 1464 
 AftNode details:
 AftFilterTemplate token:1464 group:7 tag:None nodeMask:Default{FilterName:f1 Index:144100 FilterState:FilterEnd}
 
 JexprHandle details: Handle Type : JexprHandleFilterTemplate, pfeMask: 0x0, progMode: perPfe
 Filter Name: f1
 Filter Index: 144100
 Filter Family: 0
 Fast Filter: No
 
 Terms:
   Term Name: 1
     Term Index: 384001
     Next Term Index: 384002
     Ip version : 0
     Term Matches:
		  Match Type:protocol
 		   Match polarity:1
		   Match-Class = Range
		    max = 47		    min = 47
 
		  Match Type:gre-key
 		   Match polarity:1
		   Match-Class = Range
		    max = 4294967295		    min = 4294967295
 
     Term Actions:
		 Action Type:counter
		 name = c1 index = 384001
		 Action Type:accept
 
   Term Name: 2
     Term Index: 384002
     Next Term Index: 0
     Ip version : 0
     Term Matches:
     Term Actions:
		 Action Type:counter
		 name = incoming index = 384002
		 Action Type:log
		 Action Type:accept
 
 
 FLT Compiler Info for filter: f1
  Num of FLTs: 1
  FLT-type: 33
  Is Two Pass No
 
 
  FLTC-FLT:
   Is-split-flt: false
   num of terms: 2
 
 
   Alpha Inst: 0
    Number of Unique Prefixes: 0
    Number of FCVs: 0
    PSCV Vector:
    0x3
    NSCV Vector:
    0x3
 
   Alpha Inst: 1
    Number of Unique Prefixes: 0
    Number of FCVs: 0
    PSCV Vector:
    0x3
    NSCV Vector:
    0x3
 
 
   Beta-Inst: 0
    Number of Beta Terms: 1
    Number of FCVs: 2
      Beta Node:0   Total-Endpoints:2  Node-Endpoints:2 Out-degree:4
        Endpt:0x0 (Fcv-idx:0)  Endpt:0xffff00 (Fcv-idx:1)
        Bank:0 Adde:0 Child:0
         0 -> NONE
         0 -> NONE
         0 -> NONE
         0 -> NONE
    SCV Vector:
    0x2
 
   Beta-Inst: 1
    Number of Beta Terms: 1
    Number of FCVs: 2
      Beta Node:0   Total-Endpoints:2  Node-Endpoints:2 Out-degree:4
        Endpt:0x0 (Fcv-idx:0)  Endpt:0xffff00 (Fcv-idx:1)
        Bank:0 Addr:0 Child:0
         0 -> NONE
         0 -> NONE
         0 -> NONE
         0 -> NONE
    SCV Vector:
    0x2
 
 
   Tcam Inst: 0
    Number of Match Bits:16
    Number of Terms:2
    Number of TCAM Sets Added:1
    Number of TCAM Sets which are Duplicate:0
    Number of TCAM Sets Added due to Reordering:0
    Number of Processed TCAM Sets:1
 
 Data:0x0000000000002f00
 Mask:0x000000000000ff00
 Rslt Bit-Vec:
    0x0000000000000001
 Negate Bit-Vec:
    0x0000000000000000
 
    Number of FCVs: 1
    PSCV Vector:
    0x2
    NSCV Vector:
    0x2
 
 
   FCV-Inst: 0
    FCV Instance Type:Alpha-FCV
    Number of FCVs: 0
 
   FCV-Inst: 1
    FCV Instance Type:Alpha-FCV
    Number of FCVs: 0
 
   FCV-Inst: 2
    FCV Instance Type:Beta-FCV
    Number of FCVs: 2
 
   FCV-Inst: 3
    FCV Instance Type:Beta-FCV
    Number of FCVs: 2
 
   FCV-Inst: 4
    FCV Instance Type:TCAM-FCV
    Number of FCVs: 1
 
 
   ATV Inst Num: 0
    Bit-vector:
    0x0
 
   PTV Inst Num: 0
    Bit-vector:
    0x0
 
 




   Beta-Inst: 0
    Number of Beta Terms: 1
    Number of FCVs: 2
      Beta Node:0   Total-Endpoints:2  Node-Endpoints:2 Out-degree:4
        Endpt:0x0 (Fcv-idx:0)  Endpt:0xffff00 (Fcv-idx:1)
        Bank:0 Adde:0 Child:0
         0 -> NONE
         0 -> NONE
         0 -> NONE
         0 -> NONE
    SCV Vector:
    0x2
 
   Beta-Inst: 1
    Number of Beta Terms: 1
    Number of FCVs: 2
      Beta Node:0   Total-Endpoints:2  Node-Endpoints:2 Out-degree:4
        Endpt:0x0 (Fcv-idx:0)  Endpt:0xffff00 (Fcv-idx:1)
        Bank:0 Addr:0 Child:0
         0 -> NONE
         0 -> NONE
         0 -> NONE
         0 -> NONE
    SCV Vector:
    0x2

   Beta-Inst: 0
    Number of Beta Terms: 1
    Number of FCVs: 2
      Beta Node:0   Total-Endpoints:3  Node-Endpoints:3 Out-degree:4
        Endpt:0x0 (Fcv-idx:0)  Endpt:0x6400 (Fcv-idx:1)  Endpt:0x6500 (Fcv-idx:0)
        Bank:0 Addr:0 Child:0
         0 -> NONE
         0 -> NONE
         0 -> NONE
         0 -> NONE
    SCV Vector:
    0x2
 
   Beta-Inst: 1
    Number of Beta Terms: 1
    Number of FCVs: 2
      Beta Node:0   Total-Endpoints:2  Node-Endpoints:2 Out-degree:4
        Endpt:0x0 (Fcv-idx:0)  Endpt:0x100 (Fcv-idx:1)
        Bank:0 Addr:0 Child:0
         0 -> NONE
         0 -> NONE
         0 -> NONE
         0 -> NONE
    SCV Vector:
    0x2





set firewall family inet filter f1 term 1 from protocol gre
set firewall family inet filter f1 term 1 from gre-key-except 4300
set firewall family inet filter f1 term 1 then count c1
set firewall family inet filter f1 term 1 then accept
set firewall family inet filter f1 term 2 then count incoming
set firewall family inet filter f1 term 2 then log
set firewall family inet filter f1 term 2 then accept


set firewall family inet filter f1 term t1 from gre-key-except 4683
set firewall family inet filter f1 term t1 from gre-key-except 4300



set chassis aggregated-devices ethernet device-count 10
set interfaces et-1/0/1 ether-options 802.3ad ae0
set interfaces et-1/0/3 ether-options 802.3ad ae0
set interfaces ae0 aggregated-ether-options minimum-links 1
set interfaces ae0 aggregated-ether-options lacp active
set interfaces ae0 unit 0 family inet filter input f1
set interfaces ae0 unit 0 family inet address 21.1.1.1/24


Hi Savitha

I am seeing the PR is working as expected. I created an AE interface as following:

set chassis aggregated-devices ethernet device-count 10
set interfaces et-1/0/1 ether-options 802.3ad ae0
set interfaces et-1/0/3 ether-options 802.3ad ae0
set interfaces ae0 aggregated-ether-options minimum-links 1
set interfaces ae0 aggregated-ether-options lacp active
set interfaces ae0 unit 0 family inet filter input f1
set interfaces ae0 unit 0 family inet address 21.1.1.1/24

The f1 filter is as following:

set firewall family inet filter f1 term 1 from protocol gre
set firewall family inet filter f1 term 1 from gre-key-except 4300-4683
set firewall family inet filter f1 term 1 then count c1
set firewall family inet filter f1 term 1 then accept
set firewall family inet filter f1 term 2 then count incoming
set firewall family inet filter f1 term 2 then log
set firewall family inet filter f1 term 2 then accept

If I send traffic with gre-key 100, I see following:

root@EVOvSCAPA1-RE0-re0# run show firewall filter f1    

Filter: f1                                                     
Counters:
Name                                                Bytes              Packets
c1                                                1062160                 9656
incoming                                                0                    0

After clearing the counters, if I send traffic with gre-key 4300, I see as following: 

root@EVOvSCAPA1-RE0-re0# run show firewall filter f1    

Filter: f1                                                     
Counters:
Name                                                Bytes              Packets
c1                                                      0                    0
incoming                                         56726780               515698


Please reassign me if you find any issue here.

Thanks
Tamim