%% 
%% This is file csthesis.cls.
%%
%% It is based on the standard LaTeX report class.
%% 
%% 
%% This file is part of the LaTeX2e system. 
%% ---------------------------------------- 
%% 
\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{csdissertation}[2007/11/06 CS Dept. Kent State University]
%\newif\ifchap  % true for chap option
%  \chaptrue   % true by default
%\DeclareOption{chap}{\chaptrue} % option to print "Chapter" at each new chapter
\newcommand\docsize{}  % to allow 10pt or 11pt to be specified as option
\DeclareOption{10pt}{\renewcommand\docsize{10pt}}
\DeclareOption{11pt}{\renewcommand\docsize{11pt}}
\DeclareOption{12pt}{\renewcommand\docsize{12pt}}
%  Prepare to load the standard report class (12pt):
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{report}}
\ExecuteOptions{12pt}         % define 11pt as the default doc size
\ProcessOptions
\LoadClass[\docsize]{report}  % load report.cls

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PAGE LAYOUT
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SIDE MARGINS:
% Values for one-sided printing:
\oddsidemargin 0.25in    %   Note that \oddsidemargin = \evensidemargin
%\evensidemargin 36pt
%\marginparwidth 0pt

%\marginparsep 0pt          % Horizontal space between outer margin and
                            % marginal note
\textwidth 6.25in            % width of text
 
% VERTICAL SPACING:
                          % Top of page:
%\topmargin -.5in         %    distance from top of page to running head
\headheight 0.25in         %    Height of box containing running head.
\headsep 0.25in            %    Space between running head and text.
\textheight 9.0in        %    space for text
\footskip 0.25in           %    Distance from baseline of box containing foot
                         %    to baseline of last line of text.

\brokenpenalty=10000
% double space the text
\renewcommand{\baselinestretch}{2}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Upper case names
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\chapterprint{CHAPTER}
\def\appendixprint{APPENDIX}
\def\tableofcontentsprint{TABLE OF CONTENTS}
\def\listoffiguresprint{LIST OF FIGURES}
\def\bibliographyprint{BIBLIOGRAPHY}
\def\listoftablesprint{LIST OF TABLES}

\makeatletter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Modification to verbatim - ???
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\@verbatim{\trivlist \item[]\if@minipage\else\vskip 12pt\fi
\leftskip\@totalleftmargin\rightskip\z@
\parindent\z@\parfillskip\@flushglue\parskip\z@
\@tempswafalse \def\par{\if@tempswa\hbox{}\fi\@tempswatrue\@@par}
\obeylines \tt \baselineskip 12pt \let\do\@makeother \dospecials}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Correct chapter headings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\chapter{\clearpage \thispagestyle{plain} \global\@topnum\z@
\@afterindenttrue \secdef\@chapter\@schapter} 

\def\@makechapterhead#1{ \vspace*{0.03125in} {
 \ifnum \c@secnumdepth >\m@ne
 \begin{center}
	\chapterprint{} \thechapter \\[12pt]
	\fi
	#1 \\[12pt]
 \end{center}
 \markright{}
 \nobreak } }

\def\@makeschapterhead#1{ \vspace*{0.125in} { \parindent 0pt
 \begin{center}
	#1 \\[12pt]
 \end{center}
 \markright{}
 \nobreak } }

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Fix appendix heading
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newif\ifresetcounter
\resetcountertrue
\def\appendix{\par
\ifresetcounter
   \setcounter{chapter}{0}
  \resetcounterfalse
\fi
\setcounter{section}{0}
\def\chapterprint{\appendixprint}
\def\thechapter{\Alph{chapter}}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Correct section headings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\section{\@startsection {section}{1}{\z@}{12pt}{1pt plus -1pt}{\normalsize}
}
\def\subsection{\@startsection{subsection}{2}{\z@}{12pt}{1pt plus -1pt}{\normalsize}}
\def\subsubsection{\@startsection{subsubsection}{3}{\z@}{12pt}{1pt plus -1pt}{\normalsize}}
\def\paragraph{\@startsection
 {paragraph}{4}{\z@}{-12pt}{-1em}{\normalsize}}
\def\subparagraph{\@startsection
 {subparagraph}{4}{\parindent}{-12pt}{-1em}{\normalsize}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%fix add to contents lines -????
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\l@chapter#1#2{\pagebreak[3] 
 \vskip 1.0em plus 1pt \@tempdima 1.5em \begingroup
 \@dottedtocline{0}{0em}{1.4em}{#1}{#2}
 \endgroup}
\def\l@section{\@dottedtocline{1}{1.5em}{2.3em}}
\def\l@subsection{\@dottedtocline{2}{3.8em}{3.2em}}
\def\l@subsubsection{\@dottedtocline{3}{7.0em}{4.1em}}
\def\l@paragraph{\@dottedtocline{4}{10em}{5em}}
\def\l@subparagraph{\@dottedtocline{5}{12em}{6em}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Correct Bibliography heading
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\thebibliography#1{\chapter*{\bibliographyprint\@mkboth
 {\ }{\ }}\list
 {[\arabic{enumi}]}{\settowidth\labelwidth{[#1]}\leftmargin\labelwidth
 \advance\leftmargin\labelsep
 \usecounter{enumi}\addcontentsline{toc}{chapter}{\bibliographyprint} }
 \def\newblock{\hskip .11em plus .33em minus -.07em}
 \sloppy
 \sfcode`\.=1000
 \renewcommand{\baselinestretch}{0.956} \large\normalsize\vspace*{12pt}
 \relax}
\let\endthebibliography=\endlist
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Table of Contents
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\tableofcontents{\@restonecolfalse\if@twocolumn\@restonecoltrue\onecolumn
 \fi\chapter*{\tableofcontentsprint\@mkboth{\ }{\ }}
 \@starttoc{toc}\if@restonecol\twocolumn\fi}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% List of Figures
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\listoffigures{\@restonecolfalse\if@twocolumn\@restonecoltrue\onecolumn
 \fi\chapter*{\listoffiguresprint\@mkboth
 {\ }{\ }}
 \addcontentsline{toc}{chapter}{\listoffiguresprint} 
 \@starttoc{lof}\if@restonecol\twocolumn
 \fi}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% List of Tables
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\listoftables{\@restonecolfalse\if@twocolumn\@restonecoltrue\onecolumn
 \fi\chapter*{\listoftablesprint\@mkboth
 {\ }{\ }}
 \addcontentsline{toc}{chapter}{\listoftablesprint} 
 \@starttoc{lot}\if@restonecol\twocolumn
 \fi}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ?????
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\cl@chapter{\@elt{section}\@elt{footnote}}
\def\thefigure{\@arabic\c@figure}
\def\thetable{\@arabic\c@table}
\def\theequation{\arabic{equation}}

\def\@xfloat#1[#2]{\ifhmode \@bsphack\@floatpenalty -\@Mii\else
   \@floatpenalty-\@Miii\fi\def\@captype{#1}\ifinner
      \@parmoderr\@floatpenalty\z@
    \else\@next\@currbox\@freelist{\@tempcnta\csname ftype@#1\endcsname
       \multiply\@tempcnta\@xxxii\advance\@tempcnta\sixt@@n
       \@tfor \@tempa :=#2\do
                        {\if\@tempa h\advance\@tempcnta \@ne\fi
                         \if\@tempa t\advance\@tempcnta \tw@\fi
                         \if\@tempa b\advance\@tempcnta 4\relax\fi
                         \if\@tempa p\advance\@tempcnta 8\relax\fi
         }\global\count\@currbox\@tempcnta}\@fltovf\fi
    \global\setbox\@currbox\vbox\bgroup 
    \def\baselinestretch{0.956}\@normalsize
    \boxmaxdepth\z@
    \hsize\columnwidth \@parboxrestore}
\long\def\@footnotetext#1{\insert\footins{\def\baselinestretch{1}\footnotesize
    \interlinepenalty\interfootnotelinepenalty 
    \splittopskip\footnotesep
    \splitmaxdepth \dp\strutbox \floatingpenalty \@MM
    \hsize\columnwidth \@parboxrestore
   \edef\@currentlabel{\csname p@footnote\endcsname\@thefnmark}\@makefntext
    {\rule{\z@}{\footnotesep}\ignorespaces
      #1\strut}}}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Variables that must be provided by user or here!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\dept#1{\gdef\@dept{#1}}
\def\area#1{\gdef\@area{#1}}
\def\degrees#1{\gdef\@degrees{#1}}
\def\@degrees{}
\def\submitdate#1{\gdef\@submitdate{#1}}
\def\copyrightyear#1{\gdef\@copyrightyear{#1}}
\def\@title{}\def\@author{}
\def\@dept{Computer Science \vspace*{.5 \baselineskip}}
\def\@area{COMPUTER SCIENCE}
\def\@submitdate{\ifcase\the\month\or
  May\or May\or May\or May\or May\or August\or
  August\or August\or December\or December\or December\or December\fi
  , \number\the\year}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Boolean variables and initial values
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\newif\ifcopyright \newif\iffigurespage \newif\iftablespage
\newif\ifprefacepage

\copyrightfalse \figurespagetrue \tablespagetrue \prefacepagefalse
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Title Page
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\titlep{%
	\thispagestyle{empty}%
	\begin{center}
		\renewcommand{\baselinestretch}{0.956}\small\normalsize
		\vspace*{30pt}
		\vbox to36pt{
			\begin{tabular}{c}
				\uppercase\expandafter{\@title}\\
			\end{tabular}
			\vfill
		}
		\vskip228pt
               A prospectus submitted to  \\
               Kent State University in partial \\
               fulfillment of the requirements for the\\
               degree of Doctor of Philosophy \\
		\vskip78pt
		\renewcommand{\baselinestretch}{1.765}\small\normalsize
		by \\
		\@author \\
		\@submitdate
	\end{center}
	\newpage}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copyright Page - note: untested!
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\copyrightpage{%
	\null\vfill
	\begin{center}
		\Large\copyright\ Copyright \@copyrightyear\\
		by\\
		\@author\\
	\end{center}
	\vfill\newpage}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Signature Page - with names
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\advisor#1{\gdef\@advisor{#1}}
\def\membera#1{\gdef\@membera{#1}}
\def\memberb#1{\gdef\@memberb{#1}}
\def\memberc#1{\gdef\@memberc{#1}}
\def\memberd#1{\gdef\@memberd{#1}}
\def\chair#1{\gdef\@chair{#1}}
\def\dean#1{\gdef\@dean{#1}}
\def\@advisor{}\def\@chair{}\def\@dean{}
\def\@membera{}\def\@memberb{}\def\@memberc{}\def\@memberd{}
\def\signaturepage{%
	\begin{center}
		%\vspace*{30pt}
		\vbox to132pt{
			Dissertation written by \\
			\@author \\
                        \@degrees \\
                        \vfill
		}
		%\vfill
		\vspace*{50pt}
		Approved by \\[8pt]
		\flushleft\rule{7.3cm}{0.4pt}, {Chair, Doctoral Dissertation Committee~~~~}
				\vspace*{-25pt}
				\flushleft\@advisor
				\vspace*{-15pt}
		\flushleft\rule{7.3cm}{0.4pt}, {Members, Doctoral Dissertation Committee}
				\vspace*{-25pt}
				\flushleft\@membera
				\vspace*{-15pt}		
		\flushleft\rule{7.3cm}{0.4pt} {\ }
				\vspace*{-25pt}
				\flushleft\@memberb
				\vspace*{-15pt}		
		\flushleft\rule{7.3cm}{0.4pt} {\ }
						\vspace*{-25pt}
						\flushleft\@memberc
						\vspace*{-15pt}		
		\flushleft\rule{7.3cm}{0.4pt} {\ }
								\vspace*{-25pt}
								\flushleft\@memberd
								\\[4pt]
		 \end{center}										
		%\parbox{71mm}{\raggedleft \@advisor} , \makebox[78mm]{Chair, Doctoral Dissertation Committee~~~~} \\
		%\parbox{71mm}{\raggedleft \@membera} , \makebox[78mm]{Members, Doctoral Dissertation Committee} \\
		%\parbox{71mm}{\raggedleft \@memberb} ~ \makebox[78mm]{\ } \\
		%\parbox{71mm}{\raggedleft \@memberc} ~ \makebox[78mm]{\ } \\
		%\parbox{71mm}{\raggedleft \@memberd} ~ \makebox[78mm]{\ } \\[8pt]
	    \begin{center}
		 Accepted by \\[8pt]
		\flushleft\rule{7.3cm}{0.4pt}, {Chair, Department of \@dept}
						\vspace*{-35pt}
						\flushleft\@chair
						\vspace*{-15pt}
		\flushleft\rule{7.3cm}{0.4pt}, {Dean, College of Arts and Sciences}
								\vspace*{-25pt}
								\flushleft\@dean				
		%Accepted by \\[8pt]
                %\vspace*{\baselineskip}		
		%\parbox{71mm}{\raggedleft \@chair} , \parbox[t]{78mm}{\raggedright Chair, Department of \@dept}  \\[8pt] 
	    %    \parbox{71mm}{\raggedleft \@dean} , \makebox[78mm][l]{Dean, College of Arts and Sciences}
        \end{center}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Preface Section X - adds a preface section
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\prefacesection#1{%
	\chapter*{#1}
	\addcontentsline{toc}{chapter}{#1}
	\prefacepagetrue
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Dedication
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\dedication#1{
    \chapter*{~}
    \addcontentsline{toc}{chapter}{Dedication}
    \prefacepagetrue
    \vspace*{13.5pc}
    \begin{center}
       \emph{#1}
    \end{center}

}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Front matter
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\frontmatter{
	\pagenumbering{roman}
	\pagestyle{plain}
	\titlep
	\ifcopyright\copyrightpage\fi
	\signaturepage
	\newpage
	\tableofcontents
	\newpage
	\iffigurespage
		{\addvspace{10pt}
		\let\saveaddvspace=\addvspace
		\def\addvspace##1{}
		\listoffigures
		\let\addvspace=\saveaddvspace}
		\newpage
	\fi
        \newpage
	\iftablespage
		{\addvspace{10pt}
		\let\saveaddvspace=\addvspace
		\def\addvspace##1{}
		\listoftables
		\let\addvspace=\saveaddvspace}
		\newpage
	\fi
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Start thesis
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\def\startthesis{
	\ifprefacepage
	    \newpage
        \fi
	\pagenumbering{arabic}
	\pagestyle{headings}
}


% Start with pagestyle{headings} in case front matter isn't processed
\pagestyle{headings}
\makeatother
