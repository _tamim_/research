% Copyright 2004 by Till Tantau <tantau@users.sourceforge.net>.
%
% In principle, this file can be redistributed and/or modified under
% the terms of the GNU Public License, version 2.
%
% However, this file is supposed to be a template to be modified
% for your own needs. For this reason, if you use this file as a
% template and not specifically distribute it as part of a another
% package/program, I grant the extra permission to freely copy and
% modify this file as you see fit and even to delete this copyright
% notice. 

\documentclass{beamer}
\usepackage{graphicx,multirow,booktabs,caption, array, listings}
\usepackage{times}  
\usepackage{url}
\usepackage{listings}
\usepackage{textcomp}
\usepackage{gensymb}
\usepackage{float}
\usepackage{adjustbox}
\usepackage{caption} 
\usepackage{subcaption}
\usepackage{array}
\usepackage{listings}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{amsmath}
\usepackage{color, colortbl}
\usepackage{cite}
\usepackage{siunitx}
\usepackage{tikz}

\usetikzlibrary{arrows,shapes,positioning,shadows,trees, snakes}

\tikzset{
	basic/.style  = {draw, text width=3cm, drop shadow, font=\sffamily, rectangle},
	root/.style   = {basic, rounded corners=2pt, thin, align=center,
		fill=green!30},
	level 2/.style = {basic, rounded corners=6pt, thin,align=center, fill=green!60,
		text width=8em},
	level 3/.style = {basic, thin, align=left, fill=pink!60, text width=6.5em}
}

\usepackage{array}


% There are many different themes available for Beamer. A comprehensive
% list with examples is given here:
% http://deic.uab.es/~iblanes/beamer_gallery/index_by_theme.html
% You can uncomment the themes below if you would like to use a different
% one:
%\usetheme{AnnArbor}
%\usetheme{Antibes}
%\usetheme{Bergen}
%\usetheme{Berkeley}
%\usetheme{Berlin}
%Good
%\usetheme{Boadilla}
%\usetheme{boxes}
%\usetheme{CambridgeUS}
%\usetheme{Copenhagen}
%\usetheme{Darmstadt}
%\usetheme{default}
%\usetheme{Frankfurt}
%\usetheme{Goettingen}
%\usetheme{Hannover}
%\usetheme{Ilmenau}
%\usetheme{JuanLesPins}
%\usetheme{Luebeck}
%Good
%\usetheme{Madrid}
%\usetheme{Malmoe}
%\usetheme{Marburg}
%\usetheme{Montpellier}
%\usetheme{PaloAlto}
%\usetheme{Pittsburgh}
%Good
%\usetheme{Rochester}
%Good
\usetheme{Singapore}
%\usetheme{Szeged}
%\usetheme{Warsaw}

\setbeamertemplate{footline}[frame number]{}
\setbeamertemplate{navigation symbols}{}


\title{CP-Trie: Cumulative PopCount based Trie for IPv6 Routing Table Lookup in Software and ASIC}


\author{\underline{MD Iftakharul Islam}, Javed I Khan}
% - Give the names in the same order as the appear in the paper.
% - Use the \inst{?} command only if the authors have different
%   affiliation.

\institute[Kent State University] % (optional, but mostly needed)
{
	Department of Computer Science\\
	Kent State University\\
	Kent, OH, USA.
}

%\date[\today]{}
\date{IEEE HPSR, 2021}

% - Use the \inst command only if there are several affiliations.
% - Keep it simple, no one is interested in your street address.

% This is only inserted into the PDF information catalog. Can be left
% out. 

% If you have a file called "university-logo-filename.xxx", where xxx
% is a graphic format that can be processed by latex or pdflatex,
% resp., then you can add a logo as follows:

% \pgfdeclareimage[height=0.5cm]{university-logo}{university-logo-filename}
% \logo{\pgfuseimage{university-logo}}

% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:


% Let's get started
\begin{document}
	
	\begin{frame}
		\titlepage
	\end{frame}
	
	\begin{frame}{Outline}
		\tableofcontents
		% You might wish to add the option [pausesections]
	\end{frame}
	
\section{Routing table lookup}

	\begin{frame}{Routing table lookup}
		\begin{itemize}
			\item Also known as "IP lookup" or "FIB lookup"
			\item It involves performing the Longest prefix match (LPM) in order to find the next-hop.

\begin {table}[H]
  \caption {An example routing table} 
\resizebox{.7\columnwidth}{!}{%
	\begin{tabular}{ | m{1cm}| m{5.9cm} | m{1.6cm} |} 
		\hline Route & Prefix & Next-hop\\
		\hline r1 & 200a:410:8000::/40 & $2$\\
		\hline r2 & 200a:410:8080::/44 & $3$\\
		\hline r3 & 200a:410:8000:702::/64 & $1$\\		
		\hline r4 & 200a:410:8000:702::0df/128 & $2$\\						
		\hline 
	\end{tabular}% 	
}
\end {table}
		
		\item \textbf{Lookup}(200a:410:8088:500::300) $\implies \textcolor{red}{\{r1,r2\}} \implies 3$
		\item IP lookup is generally performed using \textbf{Trie} or \textbf{Hash} based algorithms. Here, \textcolor{red}{Trie} based algorithm is our main focus.
			
		\end{itemize}
		
	\end{frame}

\begin{frame}{Challenges of IP lookup}
\begin{itemize}

	\item Extremely high throughput needed.
    \begin{itemize}
	\item $4.8$ Tb/s Brodcom Jericho2 chip (used in many core routers) can forward \textcolor{red}{$2$ billion packets per second}.
	\end{itemize}

	\item Forwarding table of a core router can be extremely large
	\begin{itemize}
		\item Already exceeded \textcolor{red}{$100k$+} IPv6 entries and growing very rapidly.
	\end{itemize}	
	
	\item An IPv6 prefix can be $128$ bits while IPv4 prefix can be $32$ bits long. Thus, IPv6 lookup may need \textcolor{red}{$4 \times$} processing and memory compared to IPv4 lookup.
	
\end{itemize}
\end{frame}
	
\section{Existing solutions}

\begin{frame}{Existing solutions}
%based on https://texample.net/tikz/examples/feature/trees/
	
\begin{adjustbox}{max totalsize={\textwidth}{\textheight},center}
		
\begin{tikzpicture}[
level 1/.style={sibling distance=40mm},
edge from parent/.style={->,draw},
>=latex]

% root of the the initial tree, level 1
\node[root] {IP lookup implementation}
% The first level, as children of the initial tree
child {node[level 2] (c1) {Software}}
child {node[level 2] (c2) {Pipelined ASIC}}
child {node[level 2] (c3) {TCAM}}
child {node[level 2] (c4) {FPGA}}
child {node[level 2] (c4) {GPU}};

% The second level, relatively positioned nodes
\begin{scope}[every node/.style={level 3}]
\node [below of = c1, xshift=15pt] (c11) {Trie};
\node [below of = c11] (c12) {Hash};

\node [below of = c2, xshift=15pt] (c21) {Trie};
\node [below of = c21] (c22) {Hash};

\end{scope}

% lines from each level 1 node to every one of its "children"
\foreach \value in {1,2}
\draw[->] (c1.195) |- (c1\value.west);

\foreach \value in {1,...,2}
\draw[->] (c2.195) |- (c2\value.west);

\end{tikzpicture}

\end{adjustbox}
	
	\begin{itemize}
		\item High-speed routers generally use ASIC and TCAM.
		\item This paper focuses on \textcolor{red}{Software and ASIC} based IP lookup.
		\item Pipelined ASIC use algorithmic solution (like software) in hardware.
		\item  Software and ASIC based IP lookup solutions can be categorized as Trie and Hash based solutions.
		\item \textcolor{red}{Trie} based solutions are the main focus of this paper.
	\end{itemize}

\end{frame}

\begin{frame}{Trie based IP lookup algorithms}

\begin{figure}[] 
	\begin{tikzpicture}[]
	% draw horizontal line   
	\draw (0,0) -- (9,0);
	
	
	% draw vertical lines
	\foreach \x in {0,3,6,9}
	\draw (\x cm,40pt) -- (\x cm,-5pt);
	
	\foreach \x in {1.5,4.5,7.5}
	\draw (\x cm,10pt) -- (\x cm,-5pt);
	
	% draw nodes
	\draw (0,0) node[below=3pt] {$1997$} node[above=40pt] {\begin{tabular}{c} \textcolor{blue}{Lule\aa $\ $} \\ {[SIGCOMM]}\end{tabular}};
	\draw (1.5,0) node[below=3pt] {$1999$} node[above=10pt] {\begin{tabular}{c} \textcolor{blue}{LC-Trie} \\ {[JSAC]}\end{tabular}};
	\draw (3,0) node[below=3pt] {$2004$} node[above=40pt] {\begin{tabular}{c} \textcolor{blue}{Tree Bitmap} \\ {[SIGCOMM CCR]}\end{tabular}};
	\draw (4.5,0) node[below=3pt] {$2009$} node[above=10pt] {\begin{tabular}{c} \textcolor{blue}{SST} \\ {[ICNP]}\end{tabular}};
	\draw (6,0) node[below=3pt] {$2011$} node[above=40pt] {\begin{tabular}{c} \textcolor{blue}{OET} \\ {[INFOCOM]}\end{tabular}};
	\draw (7.5,0) node[below=3pt] {$2014$} node[above=10pt] {\begin{tabular}{c} \textcolor{blue}{SAIL} \\ {[SIGCOMM]}\end{tabular}};
	\draw (9,0) node[below=3pt] {$2015$} node[above=40pt] {\begin{tabular}{c} \textcolor{blue}{Poptrie} \\ {[SIGCOMM]}\end{tabular}};
	\end{tikzpicture}
	
\end{figure}
\begin{itemize}
	\item \textbf{Poptrie} is the state of the art IP lookup because it requires very small memory and can perform very fast lookup. 
	
	\item Poptrie however uses $6$-bit stride.
	\item \textbf{CP-Trie} is extension of \textbf{Poptrie} which can uses $8-16$ bit stride, thus results more efficient lookup.
\end{itemize}

\end{frame}


\begin{frame}{Poptrie based IP lookup}

\begin{figure}[!t]
	\centering
	\includegraphics[width=8.4cm]{tree-poptrie}
\end{figure}

\end{frame}

\begin{frame}{Limitation of Poptrie}

\begin{itemize}
	\item Poptrie uses $6$-bit stride. This is because PopCount CPU instruction can only process $64$ bits ($2^6=64$). 	
	\item As a result, Poptrie splits an IPv6 routing table table into  \textcolor{red}{$20$ levels}: level $16$, $22$, $28$, $34$, $40$, $46$, $52$, $58$, $64$, $70$, $76$, $82$, $88$, $94$, $100$, $106$, $112$, $118$, $124$ and $130$.
	\item In CP-Trie, we store \textcolor{red}{cumulative PopCount} along with bitmap with allows it to use \textcolor{red}{longer stride ($8-16$ bit)}.
	\item CP-Trie splits an IPv6 routing table into \textcolor{red}{$15$ levels}: level $16$, $24$, $32$, $40$, $48$, $56$, $64$, $72$, $80$, $88$, $96$, $104$, $112$, $120$ and $128$.
	
\end{itemize}

\end{frame}

\section{CP-Trie based IP lookup}

\begin{frame}{Benefits of longer stride}

\begin{itemize}
	
	\item Fewer number of steps
	\begin{itemize}
		\item Results in \textcolor{red}{faster lookup}
		\item Lower \textcolor{red}{power consumption in ASIC} 
	\end{itemize}
	
	\item Fewer memory accesses (critical in pipelined ASIC).
	\begin{itemize}
		
		\item In pipelined ASIC, \textcolor{red}{\# of memory accesses = \# of SRAM blocks
			needed} (each pipeline stage has to access SRAM in parallel.).
		\begin{figure}[!t]
			\centering
			\includegraphics[width=3cm]{pipelined-asic}			
		\end{figure}
		
		\item It also has been found that \textcolor{red}{fewer large SRAM blocks are more area efficient than many smaller SRAM blocks}. This is because $85\%$ area of a SRAM is used for control logic.
		
		\item This is why, \textcolor{red}{reducing the number of memory access reduces the number of SRAM blocks needed in ASIC which results in smaller area.}
	\end{itemize} 
	
\end{itemize}

\end{frame}

\begin{frame}{CP-Trie based IP lookup}

\begin{figure}[!t]
	\begin{subfigure}[t]{0.5\textwidth}
		\centering
		\includegraphics[width=5.6cm]{treewopush}
	\end{subfigure}%
	~ 	
	\begin{subfigure}[t]{0.5\textwidth}
		\centering
		\includegraphics[width=5.6cm]{tree}
	\end{subfigure}%
	
	\begin{subfigure}[t]{.42\textwidth}
		\centering
		\includegraphics[width=5.9cm]{array}
		
	\end{subfigure}%	
	~
	\begin{subfigure}[t]{.58\textwidth}
		\centering
		\includegraphics[width=3.8cm]{lookup}
		\caption{Lookup in CP-Trie}
	\end{subfigure}%	
	
\end{figure}

\end{frame}



\section{Implementations}

	\begin{frame}{Implementations}
		\begin{itemize}			
			\item Implemented both Poptrie and CP-Trie in C and Verilog RTL. Source code is publicly available at \url{https://tamimcse.github.io/cp-trie/}.
			
			
			\item We use a high-level synthesis (HLS) tool named C2RTL [\textbf{HPSR 2021}] to generate the Verilog RTL code.
			 
			 \item We generated physical chip layout using OpenROAD EDA. We also evaluate power, area and timing characteristics of ASIC using OpenROAD.
			 
\begin{figure}[!t]
	\centering
	\includegraphics[width=5cm]{hls-flow}
	\caption{C code to physical chip layout generation}\label{hls-flow}
\end{figure}

		\end{itemize}
	
		
	\end{frame}

\section{Evaluation in ASIC and Software}
\begin{frame}{Evaluation in ASIC}
\begin {table}[H]
\centering
\resizebox{.8\columnwidth}{!}{%
	\begin{tabular}{ | m{4cm}| m{3cm}| m{3cm} |}
	\hline  & Poptrie & CP-Trie\\
	\hline Clock speed & $1$ GHz & $1$ GHz \\
	\hline Internal Power & $76.5$ mW& $64.6$ mW\\
	\hline Switching Power & $24.4$ mW & $22.2$ mW\\
	\hline Leakage Power & $1.15$ mW& $0.926$ mW\\
	\hline Total Power & $102.05$ mW & $87.726$ mW\\
	\hline Area & $0.0658$ $mm^2$ & $0.0523$ $mm^2$\\
	\hline 
	\end{tabular}%
}
\end {table}
    \begin{itemize}
    	\item SRAM implemation is missing in Open Cell Library. We use registers instead.
    	\item Poptrie needs $79$ SRAM blocks and CP-Trie needs $59$ SRAM blocks. So, we expect CP-Trie to be even more efficient in area and power compared to Poptrie when SRAM is incorporated.
	\end{itemize}

\end{frame}
	

\begin{frame}{Evaluation in Software}
	\begin {table}[H]
	\caption {FIB dataset} \label{fibdataset} 
\resizebox{.7\columnwidth}{!}{%
		\begin{tabular}{ | m{.8cm}| m{2.3cm} | m{3.8cm}  |}
			\hline Name & \# of prefixes &Longest prefix length\\
			\hline fib0 & $105,363$ & $48$ \\
			\hline fib1 & $102,126$ & $48$\\
			\hline fib2 & $79,431$ & $64$\\
			\hline fib3 & $103,067$ & $128$\\
			\hline fib4 & $105,957$ & $128$\\
			\hline fib5 & $102,739$ & $64$\\
			\hline fib6 & $104,235$ & $48$\\
			\hline fib7 & $100,899$ & $64$\\
			\hline fib8 & $102,731$ & $128$\\
			\hline 
		\end{tabular}%
}
	\end {table}
		\begin{itemize}									
		\item We obtained the data set from RouteView project. The snapshot was taken on January $17$, $2021$ at 3 PM EST.				
		\end{itemize}
			
		\end{frame}

	
	\begin{frame}{Memory consumption}
	\begin{figure}[!t]
	\centering
	\includegraphics[width=9cm]{memory}
	\caption{Memory consumption for different FIBs}\label{memorycomp}
	\end{figure}
	\end{frame}
	
\begin{frame}{Lookup traffics}
\begin{itemize}									
	\item \textbf{Prefix traffic} consists of the prefixes in our FIBs. Prefix traffic ensures that the lookups are spread across a FIB.			
	\item \textbf{Repeated traffic} consists of $2^{24}$ randomly selected prefixes from the FIBs (with possible duplicates) and adding random bits at the "don't care" fields of the prefixes. Each IP in repeated traffic is looked up $10$ times repeatedly. Repeated traffic is \textbf{analogous to real Internet trace}.
	
	\item \textbf{Sequential traffic } consists of $2^{8}$ sequentially increasing IPv6 addresses. The start address of the sequential traffic is selected such that all the IPs in sequential traffic requires matching up to the last level of the FIB. We use sequential traffic to measure the worst lookup throughput for a FIB.
	
	\item \textbf{Random traffic } consists of $2^{24}$ random IPv6 addresses within 2000::/4.
\end{itemize}
\end{frame}
	
	\begin{frame}{Lookup performance}
\begin{figure}[!t]
	\begin{subfigure}[t]{0.5\textwidth}
		\centering
		\includegraphics[width=5.8cm]{lookup-throughput-prefix}
		\caption{Prefix traffic}\label{lookup-throughput-prefix}
	\end{subfigure}%
	~ 	
	\begin{subfigure}[t]{0.5\textwidth}
		\centering
		\includegraphics[width=5.8cm]{lookup-throughput-repeated}
		\caption{Repeated traffic (analogous to real Internet trace)}\label{lookup-throughput-repeated}
	\end{subfigure}%
	
	\begin{subfigure}[t]{.5\textwidth}
		\centering
		\includegraphics[width=5.8cm]{lookup-throughput-sequential}
		\caption{Sequential traffic}
	\end{subfigure}%
	~
	\begin{subfigure}[t]{0.5\textwidth}
		\centering
		\includegraphics[width=5.8cm]{lookup-throughput-random}
		\caption{Random traffic (not a realistic traffic)}
	\end{subfigure}%
\end{figure}
	\end{frame}	
	
	\begin{frame}{Update performance}
	\begin{figure}[!t]
		\centering
		\includegraphics[width=9cm]{update}
		\caption{Average insertion time per prefix (smaller is better)}\label{update}
	\end{figure}
	\end{frame}

	
\begin{frame}{}
	\centering \Large
	\Huge{Thank You}
\end{frame}
	
\end{document}


