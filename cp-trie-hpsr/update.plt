set terminal png size 850,320 noenhanced font "Helvetica,20"
set output 'update.png'

set key inside top right horizontal font "Helvetica, 20" width 1

set yrange [0:170]
set style line 2 lc rgb 'black' lt 1 lw 1
set style data histogram
set style histogram cluster gap 1
set style fill pattern border -1
set boxwidth 0.5
set ylabel "microseconds"
#set xlabel "Prefix length"
set xtics format ""
set grid ytics

plot "update.dat" using 3:xtic(1) title "Poptrie" ls 2, \
            '' using 4 title "CP-Trie" ls 2, \

#Previously it was
#plot "update.dat" using 2:xtic(1) title "SAIL_L" ls 2, \
#            '' using 3 title "Poptrie" ls 2, \
#            '' using 4 title "CP-Trie" ls 2, \

