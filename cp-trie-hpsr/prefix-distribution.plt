set terminal png size 950,400 noenhanced font "Helvetica,20"
set output 'prefix-distribution.png'

set yrange [-1:48]
set style line 2 lc rgb 'black' lt 1 lw 1
set style data histogram
set style histogram cluster gap 1
set style fill pattern border -1
#set boxwidth 0.5
set ylabel "Percentage(%)"
set xlabel "Prefix length"
set xtics format ""
set grid ytics

#set title "Prefix Distribution"
plot "prefix-distribution.dat" using 2:xtic(1) notitle

