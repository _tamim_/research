set terminal png size 850,300 noenhanced font "Helvetica,20"
set output 'lookup-fib.png'

set key inside top right horizontal font "Helvetica, 20" width 1

set yrange [0:250]
set style line 2 lc rgb 'black' lt 1 lw 1
set style data histogram
set style histogram cluster gap 1
set style fill pattern border -1
set boxwidth 0.5
set ylabel "CPU cycles"
#set xlabel "Level where the longest prefix was found"
#set xtics format "Level where the longest prefix was found"
set grid ytics

#set title "Average number of CPU cycle needed for per-packet lookup"
plot "lookup-fib.dat" using 2:xtic(1) title 'SAIL_L' ls 2, \
            '' using 3 title "Poptrie" ls 2, \
            '' using 4 title "CP-Trie" ls 2, \


