set terminal png size 800,500 noenhanced font "Helvetica,20"
set output 'update.png'

set yrange [0:180]
set style line 2 lc rgb 'black' lt 1 lw 1
set style data histogram
set style histogram cluster gap 1
set style fill pattern border -1
set boxwidth 0.5
set ylabel "Average number of CPU cycles"
set xlabel "Prefix length"
set xtics format ""
set grid ytics

plot "update.dat" using 2:xtic(1) title "SAIL_L" ls 2, \
            '' using 3 title "SAIL_U" ls 2, \

