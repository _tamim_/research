% Copyright 2004 by Till Tantau <tantau@users.sourceforge.net>.
%
% In principle, this file can be redistributed and/or modified under
% the terms of the GNU Public License, version 2.
%
% However, this file is supposed to be a template to be modified
% for your own needs. For this reason, if you use this file as a
% template and not specifically distribute it as part of a another
% package/program, I grant the extra permission to freely copy and
% modify this file as you see fit and even to delete this copyright
% notice. 

\documentclass{beamer}
\usepackage{graphicx,multirow,booktabs,caption, array, listings}
\usepackage{times}  
\usepackage{url}
\usepackage{listings}
\usepackage{textcomp}
\usepackage{gensymb}
\usepackage{float}
\usepackage{adjustbox}
\usepackage{caption} 
\usepackage{subcaption}
\usepackage{array}
\usepackage{listings}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{amsmath}
\usepackage{color, colortbl}
\usepackage{cite}
\usepackage{siunitx}
\usepackage{tikz}

\usetikzlibrary{arrows,shapes,positioning,shadows,trees, snakes}

\tikzset{
	basic/.style  = {draw, text width=3cm, drop shadow, font=\sffamily, rectangle},
	root/.style   = {basic, rounded corners=2pt, thin, align=center,
		fill=green!30},
	level 2/.style = {basic, rounded corners=6pt, thin,align=center, fill=green!60,
		text width=8em},
	level 3/.style = {basic, thin, align=left, fill=pink!60, text width=6.5em}
}

\usepackage{array}


% There are many different themes available for Beamer. A comprehensive
% list with examples is given here:
% http://deic.uab.es/~iblanes/beamer_gallery/index_by_theme.html
% You can uncomment the themes below if you would like to use a different
% one:
%\usetheme{AnnArbor}
%\usetheme{Antibes}
%\usetheme{Bergen}
%\usetheme{Berkeley}
%\usetheme{Berlin}
%Good
%\usetheme{Boadilla}
%\usetheme{boxes}
%\usetheme{CambridgeUS}
%\usetheme{Copenhagen}
%\usetheme{Darmstadt}
%\usetheme{default}
%\usetheme{Frankfurt}
%\usetheme{Goettingen}
%\usetheme{Hannover}
%\usetheme{Ilmenau}
%\usetheme{JuanLesPins}
%\usetheme{Luebeck}
%Good
%\usetheme{Madrid}
%\usetheme{Malmoe}
%\usetheme{Marburg}
%\usetheme{Montpellier}
%\usetheme{PaloAlto}
%\usetheme{Pittsburgh}
%Good
%\usetheme{Rochester}
%Good
\usetheme{Singapore}
%\usetheme{Szeged}
%\usetheme{Warsaw}

\setbeamertemplate{footline}[frame number]{}
\setbeamertemplate{navigation symbols}{}


\title{C2RTL: A High-level Synthesis System for IP Lookup and Packet Classification}


\author{\underline{MD Iftakharul Islam}, Javed I Khan}
% - Give the names in the same order as the appear in the paper.
% - Use the \inst{?} command only if the authors have different
%   affiliation.

\institute[Kent State University] % (optional, but mostly needed)
{
	Department of Computer Science\\
	Kent State University\\
	Kent, OH, USA.
}

%\date[\today]{}
\date{IEEE HPSR, 2021}

% - Use the \inst command only if there are several affiliations.
% - Keep it simple, no one is interested in your street address.

% This is only inserted into the PDF information catalog. Can be left
% out. 

% If you have a file called "university-logo-filename.xxx", where xxx
% is a graphic format that can be processed by latex or pdflatex,
% resp., then you can add a logo as follows:

% \pgfdeclareimage[height=0.5cm]{university-logo}{university-logo-filename}
% \logo{\pgfuseimage{university-logo}}

% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:


% Let's get started
\begin{document}
	
	\begin{frame}
		\titlepage
	\end{frame}
	
	\begin{frame}{Outline}
		\tableofcontents
		% You might wish to add the option [pausesections]
	\end{frame}
	
\section{Motivation}

\begin{frame}{IP lookup and packet classification}
	
\begin{itemize}

\item Two of the most fundamental operations of a router.

\item IP lookup $\implies$ longest prefix match

\begin {table}[H]
  \caption {An example forwarding table}
\centering
\resizebox{.4\columnwidth}{!}{%
	\begin{tabular}{ | m{3.4cm} | m{2cm} |} 
		\hline Prefix & Next-hop\\
		\hline $131.123.252.42/32$ & $1$\\
		\hline $169.254.0.0/16$ & $2$\\
		\hline $169.254.192.0/18$ & $3$\\						
		\hline 
	\end{tabular}
}
\end {table}

\item Packet classification $\implies$ exact/prefix match to find action

\begin {table}[H]
\caption {An example packet classifier}
\centering
\resizebox{.65\columnwidth}{!}{%
	\begin{tabular}{ | m{.6cm} | m{2cm} | m{2cm} | m{2cm}| m{2cm} | m{1.2cm}| m{1.3cm}|} 
		\hline Rule & Src. IP & Dest. IP& Src. Port & Dest. Port & Protocol & Action\\
		\hline r1 & $01100*$ & $01100*$ & $111$ & $111$ & 80 & enqueue\\
		\hline r2 & $11010*$ & $*$ & $10*$ & $11*$ & 22& drop\\
		\hline r2 & $11*$ & $11011*$ & $10*$ & $11*$ & 22& modify\\
		\hline r4 & $*$ & $*$ & $*$ & $*$ & * & forward\\
		\hline 
	\end{tabular}
}
\end {table}

\end{itemize}		

	\begin{itemize}
	\item Implemented in \textcolor{red}{TCAM or \underline{Pipelined ASIC}}.
	
	\item ASIC generally executes \textbf{Trie} based IP lookup and packet classification algorithms in hardware.

	\end{itemize}
		
\end{frame}

\begin{frame}{ASIC Design}

\begin{itemize}
	
	\item ASIC is developed register-transfer (RTL) level Verilog or VHDL $\implies$ \textcolor{red}{extremely complex, requires huge effort}

	\begin{itemize}
		\item Need to use circuit level abstractions
		\item Need to keep track of path latency to schedule operations at right time.
	\end{itemize}

    \item This calls for designing ASIC in a higher level.

\end{itemize}

\end{frame}

\begin{frame}{High-level Synthesis}

\begin{itemize}
	\item High-level Synthesis (HLS) is a design methodology where pipelined ASIC is developed using high-level languages such as C or SystemC. 
	
	\item HLS generates corresponding Verilog or VHDL RTL from C or SystemC code.
	
	\begin{itemize}
		\item Higher design productivity and lower complexity
		\item Shorter simulation cycle.
	\end{itemize}
	
	\item HLS has not been adopted in routing/switching chips, to the best of our knowledge.
	
\end{itemize}

\end{frame}

\begin{frame}{C2RTL High-level Synthesis }

	\begin{itemize}
  \item  Primarily designed for IP lookup or packet classification, but we would like to extend it for other data plane functions as well. 
  \item C2RTL takes an IP lookup or packet classification algorithm in C as input and generates corresponding Verilog RTL.
  \item Implemented an a GCC plugin
  \item We made the source code publicly available \url{https://tamimcse.github.io/c2rtl}.
	\end{itemize}

\end{frame}
	
\section{Existing HLS tools}

\begin{frame}{Existing HLS tools}
	\begin{itemize}
    \item Current HLS tools focus on C or SystemC based system design.
	\begin{itemize}
	\item Mentor's Catapult
	\item Cadence's Stratus
	\item NEC's CyberWorkBench
	\item Synopsys' Synphony
	\item SWSL \textbf{[ANCS 2013]} (designed for IP lookup)
	\end{itemize}
	\item Switch compiler for programmable ASIC.
	\begin{itemize}
	\item LEAP \textbf{[ANCS 2012]}, Domino \textbf{[SIGCOMM 2016]}
	\end{itemize}
    \item HLS tools for FPGA.
	\begin{itemize}
	\item Xilinx's Vivado
	\item Bambu \textbf{[FPL 2013]}
	\item LegUp \textbf{[FPGA 2011]}
\end{itemize}
\end{itemize}

\end{frame}

\section{Programming convention in C2RTL}


\begin{frame}{Programming convention in C2RTL}

\begin{itemize}
	\item An input program in C2RTL is implemented in C language,
	but with several restrictions.
\end{itemize}

\begin {table}[H]
\caption {programming restrictions in C2RTL} \label{program_restrictions} 
\begin{center}
	\resizebox{.6\columnwidth}{!}{%
		\begin{tabular}{ | m{9cm}|}
			\hline No loop (while, for, do-while)\\
			\hline No unstructured control flow (goto, break, continue) \\
			\hline No ternary operation \\
			\hline No dynamic memory allocation\\
			\hline No global variables\\
			\hline No structure\\
			\hline No switch\\
			\hline No function call\\
			\hline Each branch has to have a separate return statement\\
			\hline 
		\end{tabular}
	} 	
\end{center}
\end {table}

\end{frame}


\section{C2RTL design flow}

\begin{frame}{C2RTL design flow}

\begin{figure}[!t]
	\centering
	\includegraphics[width=8cm]{c2rtl-flow}
\end{figure}

\end{frame}

\begin{frame}{Intermediate code generation by GCC}

\begin{figure}[!t]
	\begin{subfigure}[t]{0.5\textwidth}
		\centering
		\includegraphics[width=5.8cm]{sail_c}
		\caption{Example IPv4 Lookup in C}\label{sail_c}
	\end{subfigure}%
	~ 	
	\begin{subfigure}[t]{0.5\textwidth}
		\centering
		\includegraphics[width=5.8cm]{cfg}
		\caption{Intermediate code in SSA and CFG form}\label{cfg}
	\end{subfigure}%
\end{figure}

\end{frame}

\begin{frame}{Control and Data flow graph (CDFG)}

\begin{figure}[!t]
	\centering
	\includegraphics[width=10cm]{cdfg}
\end{figure}

\end{frame}

\begin{frame}{Scheduling and Register Insertion} 

\begin{figure}[!t]
	\centering
	\includegraphics[width=2.5cm]{scheduling}
\end{figure}

	\begin{itemize}
	\item As Late as Possible (ALAP) scheduling \textbf{[TCAD 1991]}.
	\item We obtained the latency of operations from Bambu's \textbf{[FPL 2013]} $45$ nm Nandgate Open Cell library characterization. 
	\item We insert register when the result of an operation crosses cycle boundary. 
\end{itemize}

\end{frame}


\begin{frame}{RTL Generation} 

\begin{itemize}
	\item We implement each operation using a Verilog module from a component library obtained from Bambu.
	\item We implement each arrays using register file and SRAM.
	 
\end{itemize}

\end{frame}

\begin{frame}{MUX tree generation}

\begin {table}[H]
\centering
\caption {An example predicates for a SSA $\phi$ operation} \label{mux} 
\resizebox{.3\columnwidth}{!}{%
	\begin{tabular}{ | m{1cm}| m{.3cm}| m{.3cm}| m{.3cm}| m{.3cm}|}
		\hline  & \multicolumn{4}{|l|}{Selectors} \\
		\hline Inputs & $S_1$ & $S_2$ & $S_3$ & $S_4$ \\
		\hline $I_1$ & $*$ & $*$ & $*$ & $0$ \\
		\hline $I_2$ & $*$ & $1$ & $1$ & $1$ \\
		\hline $I_3$ & $*$ & $0$ & $1$ & $1$ \\
		\hline $I_4$ & $0$ & $*$ & $0$ & $1$ \\
		\hline $I_5$ & $1$ & $*$ & $0$ & $1$ \\
		\hline 
	\end{tabular} 	
}
\end {table}

\begin{figure}[!t]
	\begin{subfigure}[t]{0.4\textwidth}
		\centering
		\includegraphics[width=3cm]{predicate-splitting-tree}
		\caption{Predicates splitting tree}\label{sail_c}
	\end{subfigure}%
	~ 	
	\begin{subfigure}[t]{0.42\textwidth}
		\centering
		\includegraphics[width=3cm]{mux-tree}
		\caption{Corresponding MUX tree}\label{cfg}
	\end{subfigure}%
\end{figure}

\end{frame}

\section{Evaluation}
\begin{frame}{Evaluation} 

\begin{itemize}
	\item We evaluate C2RTL by implementing SAIL \textbf{[SIGCOMM 2014]}, Poptrie \textbf{[SIGCOMM 2015]} and CP-Trie \textbf{[HPSR 2021]} based IPv6 lookup and TabTree \textbf{[ANCS 2019]} based packet classification. 
	\item We evaluate the resulting Verilog using Icarus Verilog simulator. It shows that the generated Verilog is functionally correct.
\begin{figure}[!t]
	\centering
	\includegraphics[width=3cm]{hls-flow}
	\caption{C code to physical chip layout generation}\label{hls-flow}
\end{figure}
	\item We use OpenROAD to generate physical chip layout from the Verilog.
	
\end{itemize}

\end{frame}




\begin{frame}{Evaluation in ASIC}
\centering
\resizebox{\columnwidth}{!}{%
	\begin{tabular}{ | m{2.9cm}| m{2.1cm}| m{2.3cm}| m{2.1cm} | m{2.6cm} |}
	\hline  & Poptrie & SAIL & CP-Trie & TabTree\\
	\hline Clock speed & $1$ GHz & $1$ GHz & $1$ GHz & $1$ GHz \\
	\hline Internal Power & $76.5$ mW & $0.722$ mW & $64.6$ mW & $0.033$ mW\\
	\hline Switching Power & $24.4$ mW & $0.229$ mW & $22.2$ mW & $0.0054$ mW\\
	\hline Leakage Power & $1.15$ mW & $0.0108$ mW & $0.926$ mW & $0.00061$ mW\\
	\hline Total Power & $102.05$ mW & $0.961$ mW & $87.726$ mW & $0.0391$ mW\\
	\hline Area & $0.0658$ $mm^2$ & $0.00061$ $mm^2$ & $0.0523$ $mm^2$ & $0.000034$ $mm^2$\\
	\hline 
\end{tabular}
}
\end{frame}
	
\begin{frame}{}
	\centering \Large
	\Huge{Thank You}
\end{frame}
	
\end{document}


