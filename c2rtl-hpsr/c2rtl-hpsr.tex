\documentclass[conference]{IEEEtran}

\usepackage{balance}
\usepackage{graphicx,multirow,booktabs,caption, array, listings}
\usepackage{url}
\usepackage{listings}
\usepackage{textcomp}
\usepackage{gensymb}
\usepackage{float}
\usepackage{caption} 
\usepackage{comment}
\usepackage{subcaption}
\usepackage{array}
\usepackage{listings}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{color, colortbl}
\usepackage{siunitx}
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
		T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}


\begin{document}

\title{C2RTL: A High-level Synthesis System for IP Lookup and Packet Classification}

\author{\IEEEauthorblockN{MD Iftakharul Islam, Javed I Khan}
	\IEEEauthorblockA{\textit{Kent State University}\\
		Kent, Ohio, USA \\
		mislam4@kent.edu, javed@cs.kent.edu}
}

\IEEEoverridecommandlockouts
\IEEEpubid{\makebox[\columnwidth]{978-1-6654-4005-9/21/\$31.00~\copyright{}2021 IEEE
		\hfill} \hspace{\columnsep}\makebox[\columnwidth]{ }}

\maketitle

\maketitle

\begin{abstract}
IP lookup and packet classification are two core functions of a router. IP lookup involves performing the longest prefix match (LPM) of the destination IP address. Packet classification involves finding the best match in a multi-field ruleset where each field needs an exact or prefix match. ASIC based IP lookup and packet classification are traditionally designed in a register transfer level (RTL) hardware description language (HDL) such as Verilog or VHDL. However, manually writing hardware logic is notoriously complicated and painful. This paper presents a High Level Synthesis (HLS) system named C2RTL. C2RTL generates hardware logic in Verilog RTL directly from IP lookup or packet classification algorithm implemented in C. C2RTL is implemented as a plugin of GCC compiler. It takes an IP lookup or packet classification algorithm (in C) as an input and generates corresponding synthesizable Verilog RTL code for pipelined ASIC. We developed several IP lookup and packet classification algorithms in C2RTL and generated corresponding Verilog RTL. We evaluated the resulting RTL code with OpenROAD EDA.

\end{abstract}


\begin{IEEEkeywords}
High-level synthesis; Pipelined ASIC; IP lookup; Packet classification
\end{IEEEkeywords}



\section{Introduction}\label{sec:intro}
IP lookup and packet classification are two key functions of a router. IP lookup involves performing the longest prefix match of the desination IP address. For instance, Table \ref{lpm} shows an example forwarding table of a router. If the destination IP address of an incoming packet is $169.254.198.1$, then the packet should be forwarded to next-hop $4$. Again, if the destination IP address is $169.254.190.5$, the packet should be forwarded to next-hop $3$. On the other hand, packet classification involves finding the best match for a packet in a multi-field ruleset to determine the action. Table \ref{classi} shows an example ruleset of a router. Here, the rules in the ruleset consist of a source address (SA), a destination address (DA), a source port (SP), a destination port (DP) and a protocol field. Each rule in the ruleset also has a priority. Here, we can have exact or prefix match. \footnote{Packet classification rules can also have range match (e,g. $20-50$). However, a range can be represented as one or multiple prefixes \cite{li2019tabtree}.} A packet may match with more than one rules. In that case, rules with higher priority determines the action.

IP lookup and packet classification in routing chips mainly fall into two categories: TCAM and pipelined ASIC based solutions. TCAM is very expensive, area-inefficient and power hungry. It also requires very complex incremental update. On the other hand, pipelined ASIC utilizes on-chip SRAM which is cheaper and consumes less power. Pipelined ASIC can implement various algorithmic solutions of IP lookup and packet classification. In this paper, we focus on ASIC-based IP lookup and packet classification.

\begin {table}[H]
\caption {Example forwarding table (for IP lookup)} \label{lpm} 
\begin{center}
	\begin{tabular}{ | m{3cm} | m{1.5cm} |} 
		\hline Prefix & Next-hop\\
		\hline $169.254.0.0/16$ & $3$\\
		\hline $169.254.192.0/18$ & $4$\\		
		\hline $192.168.122.0/24$ & $5$\\						
		\hline 
	\end{tabular} 	
\end{center}
\end {table}

\begin {table}[H]
\caption {Example ruleset (for packet classification)} \label{classi} 
\begin{center}
	\begin{tabular}{ | m{.4cm} | m{.8cm} | m{.8cm} | m{.5cm}| m{.5cm} | m{.8cm}| m{.9cm}|} 
		\hline Rule & SA & DA& SP & DP & Protocol & Action\\
		\hline r1 & $01100*$ & $01100*$ & $111$ & $111$ & 80 & forward\\
		\hline r2 & $11010*$ & $*$ & $10*$ & $11*$ & 22& drop\\
		\hline r3 & $*$ & $01111*$ & $000$ & $001$ & * & enqueue\\
		\hline 
	\end{tabular} 	
\end{center}
\end {table}

Traditionally, ASIC based hardware blocks are implemented in a RTL level HDL such as Verilog or VHDL. However, designing a hardware at RTL level is a very complex and tedious process. Here, designers need to use low level abstraction to implement the logic. The RTL code also needs to be cycle accurate. Thus, designers need to consider the path latency of the circuit and insert registers manually to implement pipelining. Such complex and tedious process calls for designing hardware at a higher level. High-level synthesis (HLS) \cite{coussy2009introduction} allows us to design a pipelined ASIC in a high-level language such as C or SystemC \cite{SystemC}. HLS tools generate RTL level hardware logic directly from programs implemented in high level languages  such as C or SystemC. This has major advantages over traditional RTL design, such as, increased designer productivity, better complexity management, shorter simulation cycle (no hardware/software codesign is needed), rapid design space exploration, higher quality of results (QoRs) and so on \cite{chen2005xpilot, evolutionhls, khailany2018modular}. Despite the benefits, HLS has not been adopted in switching and routing chips. Currently, routing ASICs (e.g. Brodcom Jericho2 \cite{Jericho2}, Cisco Silicon One \cite{SiliconOne}, etc) are designed in a RTL-level HDL, to the best of our knowledge \cite{CiscoAsic}.

This paper presents a high level synthesis system named C2RTL. C2RTL takes an IP lookup or packet classification algorithm (implemented in C) as an input and generates corresponding synthesizable Verilog RTL code. We implemented C2RTL as a GCC plugin. The source code will be found in Github (https://tamimcse.github.io/c2rtl). 

The remaining of the paper proceeds as follows.  Section \ref{relatedworks} presents the related works. Section \ref{algorithms} characterizes IP lookup and packet classification algorithms for ASIC. Section \ref{Convention} describes the programming convention. Section \ref{system-overview} describes the system by explaining the design flow. Section \ref{mux-tree-section} describes MUX tree generation. In Section \ref{evaluation}, we evaluate C2RTL by implementing several IP lookup and packet classification algorithms for it. Section \ref{discussion} discusses the impact of C2RTL. Finally, Section \ref{conclusion} concludes the paper.
 

\section{Related Works}\label{relatedworks}
Early HLS tools mainly used behavioral Verilog or VHDL to generate RTL level Verilog or VHDL code. These tools include Mentor's Monet \cite{elliott1999understanding}, Synopsys' Behavioral Compiler \cite{knapp1996behavioral}, IMPACT \cite{khouri1998impact}  and so on. However, they did not received wide spread adoption because behavioural HDLs were not popular among algorithm designers. 

Since 2000, new generation of HLS tools focused on system design based on C, Hardware C, Handel-C, SystemC, SpecC, SystemVerilog and so on. However, the lack of standard of programming languages slowed down the adoption of HLS \cite{evolutionhls}.

The latest generation of HLS tools primarily use C or SystemC for system design. These tools include Mentor's Catapult \cite{coussy2009introduction, book2010mentor}, Cadence's Stratus (formerly C-to-Silicon Compiler \cite{kondratyev2011realistic} and Forte's Cynthesizer \cite{sanguinetti2012transaction}), NEC's CyberWorkBench\cite{wakabayashi2008all}, Synopsys' Synphony (originated as PICO-NPA \cite{schreiber2002pico} from HP lab) and so on. These tools however are not open source. There is no study on the effectiveness of these tools for IP lookup and packet classification algorithms, to the best of our knowledge. 

There has been relatively little work on high-level synthesis for IP lookup and packet classification. SWSL \cite{kim2013swsl} is an academic high-level synthesis tool that takes an IP lookup program as input and generates synthesizable Verilog RTL. SWSL was developed based on the observation that most network lookup algorithms consist of simple algorithmic steps where each step only accesses variables from its own state. SWSL converts each step to a Verilog module. SWSL has been used to implement simple trie based IP lookup. However, SWSL appears to be abandoned at this moment. 

Several research projects used high-level languages such as C and C++ to program programmable pipeline based ASIC. LEAP \cite{harris2012leap} and PLUG \cite{de2009plug} proposed hardware accelerators and compilers where network lookup or packet classification algorithm can be implemented in a high-level language (C/C++) and compiled to the programmable hardware accelerator. LEAP and PLUG have been used to implement IP lookup and packet classification in hardware \cite{harris2012leap, de2009plug, vaish2011experiences}. Domino \cite{sivaraman2016packet} presents a switch compiler which takes a high level program written in C as an input and generates machine code for programmable pipelined based switches. Domino has been used to implement IP lookup algorithm in switching ASIC\cite{islam2019sail}. Recently, Chipmunk \cite{gao2020switch} used program synthesis to produce switch machine code from C program. They used their method to generate machine code for Barefoot's Tofino switch \cite{bosshart2013forwarding}. In this paper however, we mainly focus on fixed function ASIC rather than programmable ASIC.

There are several high-level synthesis systems for FPGA. These include Xilinx Vivado (formerly  AutoPilot \cite{cong2011high}, originated as xPilot \cite{chen2005xpilot} at UCLA), Bambu \cite{pilato2013bambu}, LegUp \cite{canis2011legup}, SPARK \cite{gupta2007spark}, GAUT \cite{coussy2008gaut} and so on. These solutions however cannot generate RTL for pipelined ASIC, at this moment.

Several projects used other high-level languages to design hardware. These include Chisel \cite{bachrach2012chisel} and PyMTL \cite{lockhart2014pymtl} which use Scala and  Python respectively.

Alladin \cite{shao2014aladdin} proposed an accelerator simulator which takes a C function as an input and generates power and area without creating the actual RTL. Alladin does not support control-flow intensive programs, thus can not be used for IP lookup and packet classification algorithms.

This paper presents a high-level synthesis tool named C2RTL. We implemented several IP lookup and packet classification algorithms in C2RTL. Our experiment shows that C2RTL can be instrumental in designing IP lookup and packet classification in pipelined ASIC. We also evaluated the resulting Verilog RTL using OpenROAD. Thus, C2RTL+OpenROAD flow allows us to generate physical chip layout and area/power characteristics of different algorithms implemented in C. This approach can be instrumental in evaluating different algorithms at system level.

\begin{figure}[!t]
	\centering
	\includegraphics[width=9cm]{sail_code}
	\caption{An example trie based IP lookup in C}\label{motivation}
\end{figure}
 

\section{IP lookup and Packet classification Algorithms in ASIC}\label{algorithms}
Trie based algorithms have widely been deployed for implementing IP lookup \cite{yang2014guarantee, eatherton2004tree, tamimcptrie, baboescu2005tree, basu2003fast, jiang2008beyond, kumar2006camp, hasan2005dynamic} and packet classification \cite{li2019tabtree, hsieh2016many, antichi2014ja, jiang2009large, li2013hybridcuts, li2018cutsplit} in ASIC and FPGA in the past. This paper focuses on trie based IP lookup and packet classification algorithms. Figure \ref{motivation} shows an example trie based IP lookup algorithm named SAIL \cite{yang2014guarantee}. Here, the trie is split among $5$ levels. The lookup algorithm traverses level $16$, $24$, $32$, $40$ and $48$ in order until it finds a leaf (the details would be found in Yang et. al. \cite{yang2014guarantee}). This is why, the IP lookup algorithm requires several nested conditional statements. Note that, here we only showed lookup upto level $48$. An actual IPv6 lookup algorithm needs to visit up to level $128$ as an IPv6 address is 128 bit long. Thus, an actual IPv6 lookup algorithm will need many more nested conditional statements. This is why, trie-based IP lookup algorithms can be characterized as a control-flow intensive (CFI) application. Trie based packet classification also fall into the same category. They also do not require loop. IP lookup or packet classification algorithm also needs to be implemented as a pipeline so that resulting hardware can process packets at line-rate. 

Currently, open source HLS tools such as Bambu \cite{pilato2013bambu} and LegUP \cite{pilato2013bambu} support control-flow intensive applications, however they were primarily designed for FPGA, not ASIC. They also do not support pipelining. This motivated us to design C2RTL. C2RTL is designed to generate piplined ASIC for control-flow intensive applications. 


\begin {table}[H]
\caption {programming restrictions in C2RTL} \label{program_restrictions} 
\begin{center}
	\begin{tabular}{ | m{8cm}|}
		\hline No loop (while, for, do-while)\\
		\hline No unstructured control flow (goto, break, continue) \\
		\hline No ternary operation \\
		\hline No dynamic memory allocation\\
		\hline No global variables\\
		\hline No structure\\
		\hline No switch\\
		\hline No function call\\
		\hline Each branch has to have a separate return statement\\
		\hline 
	\end{tabular} 	
\end{center}
\end {table}


\section{Programming Convention}\label{Convention}
An input program in C2RTL is implemented in C language, but with several restrictions (Table \ref{program_restrictions}). Although C allows us to implement an algorithm in different ways, C2RTL requires us to adhere to a programming convention. These restrictions allow us to produce intermediate code (via GCC compiler) in a specific format. Note that, other HLS tools often impose similar restrictions \cite{book2010mentor, pilato2013bambu}.

C2RTL takes a C function as an input. For instance, Figure \ref{sail_c} shows an example C function which will be processed by C2RTL. Note that, all the arrays here are passed as function arguments. The arrays can be implemented as a SRAM block or mutiport SRAM or mutiport register file. Similar coding convention has also been adopted in many other HLS tools including Mentor Catapult, Bambu and so on \cite{book2010mentor, pilato2013bambu}. As we pass all the input and arrays as function parameter, we do not support global variables. C2RTL also requires each branch to have a separate \textit{return} statement (as shown in Figure \ref{sail_c}). We found that GCC does not produce $\phi$ operation \cite{ssagcc} properly if we do not have a separate \textit{return} statement in each branch. This is why, although C allows us to merge multiple branch statements (i.e. having a single \textit{return} for multiple branches), this is not not allowed in C2RTL. Note that, other HLS systems also impose similar restrictions. For instance, Mentor Catapult requires multiple branch statements to have just one a single \textit{return} statement (opposite of what C2RTL expects) \cite{book2010mentor}. C2RTL also does not support \textit{loop}. A \textit{loop-condition} that can only be evaluated at run-time cannot be implemented as a pipeline ASIC. This is why, we do not support \textit{loop} in C2RTL. 

\begin{figure}[!t]
	\centering
	\includegraphics[width=8cm]{c2rtl-flow}
	\caption{C2RTL design flow}\label{c2rtl-flow}
\end{figure}

\begin{figure*}[!t]
	\begin{subfigure}[t]{0.5\textwidth}
		\centering
		\includegraphics[width=8cm]{sail_c}
		\caption{Example IPv4 Lookup in C}\label{sail_c}
	\end{subfigure}%
	~ 	
	\begin{subfigure}[t]{0.5\textwidth}
		\centering
		\includegraphics[width=9cm]{cfg}
		\caption{Intermediate code in SSA and CFG form}\label{cfg}
	\end{subfigure}%
		
	
	\caption{Intermediate code generation by GCC}\label{cdfg-construction}
\end{figure*}

\begin{figure*}[!t]
	\centering
	\includegraphics[width=18cm]{cdfg}
	\caption{CDFG corresponding to Figure \ref{cfg}}\label{cdfg}
\end{figure*}

\begin{figure}[!t]
	\centering
	\includegraphics[width=3.9cm]{scheduling}
	\caption{Scheduling}\label{scheduling}
\end{figure}


\section{System Overview}\label{system-overview}

Figure \ref{c2rtl-flow} illustrates the design flow of C2RTL. 


\textbf{Step 1: Intermediate code generation.} C2RTL leverages GCC compiler to produce intermediate code from a C function. GCC takes a C function as an input and generates intermediate code in the form of a control flow graph (CFG). Figure \ref{cfg} shows the CFG constructed from the example code shown in Figure \ref{sail_c}. Each node in CFG is a basic block. A basic block contains intermediate code that can be executed sequentially without any branching. Edges between basic blocks represent the transfer of control flow from one basic block to another. Here, each basic block contains intermediate code in Static Single Assignment (SSA) \cite{ssagcc} form. SSA assigns each variable a different name with a version field. When a variable is assigned to a new value, the version field is increased. Thus, SSA guarantees that no versioned variable is assigned twice. This is how, it removes the Write-After-Read and Write-After-Write dependencies. There are only Read-After-Write dependencies remain in SSA. Again, as our input program has multiple control flows, SSA adds a $\phi$ function \cite{ssagcc} in the last basic block of the CFG (shown as $<L4>$ in \textit{bb\_7}). The $\phi$ function selects one of the input flows to produce the output. C2RTL obtains the intermediate code along with CFG from GCC using plugin API \cite{GCCplugin}. 

\textbf{Step 2: Microarchitecture optimization.} C2RTL performs standard compiler optimizations such as replacing multiplications and divisions with bitwise operations, operand width reduction, copy propagation and so on. 


\textbf{Step 3: CDFG construction.} Control and data flow graph (CDFG) is the de facto standard of circuit representation in many HLS tools \cite{khouri1998impact, lattuada2015code, book2010mentor, shao2014aladdin, wakabayashi2008all, kondratyev2011realistic}. C2RTL constructs a CDFG from the intermediate code. Figure \ref{cdfg} shows the CDFG constructed from the intermediate code shown in Figure \ref{cfg}. There are two types of vertices in CDFG: basic block and operation vertex. A basic block vertex represents a basic block (shown as rounded rectangle in Figure \ref{cdfg}). An operation vertex represents an operation within a basic block (shown as rectangles or trapezoids in Figure \ref{cdfg}). An operation vertex that controls the flow of operations is called control operations (shown as trapezoids in Figure \ref{cdfg}). We assign an unique ID to each operation vertex.  There are two types of edges in CDFG: data and control edge. A data edge indicates a data flow, and a control edge indicates a control flow. Data and control edges are shown as solid and dashed arrows respectively. Each basic block preceding a $\phi$ operation also contain a predicate. For instance, Figure \ref{cdfg} shows that $BB4$, $BB5$ and $BB6$ have predicates. A basic block with a predicate will execute only if the predicate is true. Finally, C2RTL generates a MUX tree for the $\phi$ function based on the predicates of the preceding basic blocks (details is discussed later in Section \ref{mux-tree-section}). For instance, C2RTL converts $<L4>$ $\phi$ operation in Figure \ref{cfg} into two MUX operations in Figure \ref{cdfg}. Here, the output of the control operations (e.g. $ifout6$ and $ifout16$) would be used as the selectors. Note that, a CDFG is analogous to a logic circuit. Here, each operation can be implemented as a combinational\footnote{https://en.wikipedia.org/wiki/Combinational\_logic} or sequential\footnote{https://en.wikipedia.org/wiki/Sequential\_logic} logic circuit. In order to implement pipelining, we also need to insert registers among the logic elements. A register holds the output of a logic circuit to be used in the next clock cycle (in the next pipeline stage). Thus, we also need to perform scheduling of each operation in order to divide them among the pipeline stages. 

\textbf{Step 3: Scheduling.} Scheduling is considered as one of the most critical steps of high-level synthesis. C2RTL uses  \textit{as late as possible (ALAP)} scheduling algorithm \cite{hwang1991formal} to schedule each operation in CDFG. Here, we obtained the latencies of the operations from Bambu's \cite{pilato2013bambu} $45$ nm Nandgate Open Cell library characterization. We use the same library for physical synthesis. Our target frequency is $1$ GHz (i.e. each cycle is $1$ ns). Figure \ref{scheduling} shows the scheduling results for the CDFG in Figure \ref{cdfg}. It shows that the program needs $14$ clock cycles ($14$ pipeline stages). Here, the operations scheduled in the same clock cycle will be executed in the same pipeline stage.

\begin{comment}
Scheduling in HLS may vary based on target implementation. For instance, HLS tools for FPGA use SDC scheduling algorithm\cite{cong2006efficient, lattuada2015code}. However, SDC scheduling algorithm cannot use pipelining \cite{bibid}  
\end{comment}

\textbf{Step 4. Register Insertion.} In a pipelined ASIC, if the result of an operation is used by an another operation scheduled in a different clock cycle, we need to insert a register after the former operation to hold its result. For instance, Figure \ref{cdfg} shows that the output of operation $2$ is used as the input to operation $3$. However, Figure \ref{scheduling} shows that operation $2$ and $3$ are scheduled in different clock cycles. In this case, we need to insert a register after operation $2$ to store its value so that operation $3$ can use it in the next clock cycle. C2RTL inserts one or multiple registers after each operation where the result of the operation crosses the cycle boundary. It also inserts a register after the output to hold the result.

\textbf{Step 5: RTL generation.} This step generates synthesizable\footnote{Verilog has many language constructs which are not synthesizable (i.e. cannot be used to generate physical chip layout). We do not use them here.} Verilog RTL from the CDFG. We implement each operation of CDFG using a verilog module from a component library. Here, we adapted the component library from Bambu \cite{pilato2013bambu}. The component library consists of synthesizable Verilog modules that implements primitive components such as arithmetic operations, bitwise operations, MUX, register, SRAM and so on. The \textit{arrays} in the input program are implemented as a multiport register file or SRAM. This is how, C2RTL generates synthesizable Verilog code corresponding to the CDFG. C2RTL also generates a verilog testbench for the input program.


\subsection{Resource Binding}
Resource binding is a critical step of a high-level synthesis tool \cite{coussy2009introduction, book2010mentor}. A component library may have multiple implementations of an operation. There are area, power and timing tradeoffs among the implementations \cite{kondratyev2012exploiting}. A high-level synthesis tool has to perform module selection such that the timing requirements are met while the area and power consumption are minimized. This process is known as resource binding or design space exploration. Modern HLS tools perform resource binding during scheduling (instead of two separate steps namely resource allocation and resource binding) \cite{kondratyev2012exploiting}. However, our component library has only one module for each operation, thus we did not need to perform resource binding in this paper.

\begin {table}[H]
\caption {An example predicates for a SSA $\phi$ operation} \label{mux} 
\begin{center}
	\begin{tabular}{ | m{.6cm}| m{.2cm}| m{.2cm}| m{.2cm}| m{.2cm}|}
		\hline  & \multicolumn{4}{|l|}{Selectors} \\
		\hline Inputs & $S_1$ & $S_2$ & $S_3$ & $S_4$ \\
		\hline $I_1$ & $*$ & $*$ & $*$ & $0$ \\
		\hline $I_2$ & $*$ & $1$ & $1$ & $1$ \\
		\hline $I_3$ & $*$ & $0$ & $1$ & $1$ \\
		\hline $I_4$ & $0$ & $*$ & $0$ & $1$ \\
        \hline $I_5$ & $1$ & $*$ & $0$ & $1$ \\
		\hline 
	\end{tabular} 	
\end{center}
\end {table}

\begin{figure}
	\begin{subfigure}[t]{0.25\textwidth}
	\centering
	\includegraphics[width=4cm]{predicate-splitting-tree}
	\caption{Predicates splitting tree}\label{predicate-splitting-tree}
\end{subfigure}%
~ 	
\begin{subfigure}[t]{0.25\textwidth}
	\centering
	\includegraphics[width=4cm]{mux-tree}
	\caption{Corresponding MUX tree}\label{mux-tree}
\end{subfigure}%
	\caption{MUX tree generation}\label{mux-tree-construction}
\end{figure}

\section{MUX Tree Generation}\label{mux-tree-section}
An SSA $\phi$ operation selects one of the inputs based on the predicates  (as discussed in Section \ref{system-overview}). A predicate consists of multiple selectors and an input. Table \ref{mux} shows example predicates of a $\phi$ operation. Here, the $\phi$ operation needs to select one of the $5$ inputs based on the $4$ selectors. The value of a selector can be $0$, $1$ or $*$ (\textit{don't care}). For instance, if $S_1 = 1$, $S_2 = 0$, $S_3 = 1$ and  $S_4 = 1$, then $\phi$ operation will select $I_3$. We need to create a MUX tree to implement the $\phi$ operation. Our component library only contains a  $2$-to-$1$ MUX. This is why, we construct a MUX tree where each node is a $2$-to-$1$ MUX. 

In order to generate a MUX tree, we first create a predicate splitting tree as shown in Figure \ref{predicate-splitting-tree}. The tree is constructed as follows. The root node will contain all the predicates. We then find a selector for which the value would be either $0$ or $1$  and not $*$ ($S_4$ in this case). We then split the root node by the selector. It divides the predicates into two groups (\{$I_1$\} and \{$I_2$, $I_3$, $I_4$, $I_5$\}). We split \{$I_2$, $I_3$, $I_4$, $I_5$\} further based on a selector for which the value would be  $0$ or $1$ and not $*$ ($S_3$ in this case). Thus, $S_3$ will split the node into two groups: \{$I_2$, $I_3$\} and \{$I_4$, $I_5$\}. This is how, we split each node of the tree until each node has exactly one predicate. We split a node based on a selector for which the value is either $0$ or $1$ and not $*$.

Figure \ref{mux-tree} shows the actual MUX tree corresponding to the predicate splitting tree in Figure \ref{predicate-splitting-tree}. Each split in predicate splitting tree is replaced by a MUX. 


\begin{figure}[!t]
	\centering
	\includegraphics[width=5cm]{hls-flow}
	\caption{C code to physical chip layout generation}\label{hls-flow}
\end{figure}

\begin {table*}[t]
\caption {Power, area and timing in ASIC} \label{powerconsumption_openroad} 
\begin{center}
	\begin{tabular}{ | m{2cm}| m{2cm}| m{2cm}| m{2cm} | m{2cm} |}
		\hline  & Poptrie & SAIL & CP-Trie & TabTree\\
		\hline Clock speed & $1$ GHz & $1$ GHz & $1$ GHz & $1$ GHz \\
		\hline Internal Power & $76.5$ mW & $0.722$ mW & $64.6$ mW & $0.033$ mW\\
		\hline Switching Power & $24.4$ mW & $0.229$ mW & $22.2$ mW & $0.0054$ mW\\
		\hline Leakage Power & $1.15$ mW & $0.0108$ mW & $0.926$ mW & $0.00061$ mW\\
		\hline Total Power & $102.05$ mW & $0.961$ mW & $87.726$ mW & $0.0391$ mW\\
		\hline Area & $0.0658$ $mm^2$ & $0.00061$ $mm^2$ & $0.0523$ $mm^2$ & $0.000034$ $mm^2$\\
		\hline 
	\end{tabular} 	
\end{center}
\end {table*}

\section{Evaluations}\label{evaluation}
We evaluate C2RTL by several IP lookup and packet classification algorithms. We implemented SAIL \cite{yang2014guarantee}, Poptrie \cite{asai2015poptrie} and CP-Trie \cite{tamimcptrie} based IPv6 lookup algorithms and TabTree \cite{li2019tabtree} based packet classification algorithm. Our implementation would be found in C2RTL repository. We generate Verilog RTL from the algorithm implementation using C2RTL. C2RTL uses correct-by-construction approach to generate Verilog RTL code (similar to most other HLS tools including Bambu \cite{pilato2013bambu}, Catapult \cite{book2010mentor} and so on). We still evaluate the generated Verilog RTL codes using Icarus Verilog\cite{iVerilog}. Our Verilog simulation shows that our generated Verilog RTL produces correct output. 

\subsection{Physical Synthesis}
We evaluate our generated RTL code by an electronic design automation (EDA) tool. EDA tools such as OpenROAD \cite{ajayi2019toward}, OpenLANE \cite{openlane} and Synopsis Fusion compiler \cite{icc} enable us to generate physical chip layout directly from Verilog RTL code. Here, we use OpenROAD. OpenROAD takes Verilog RTL code, macros (e.g. SRAM, I/O ports, etc), a cell library and constraints (e.g. clock cycle) as inputs and generates corresponding physical chip layout in GDSII format. Figure \ref{hls-flow} shows our C code to physical chip layout generation mechanism. Here, OpenROAD takes Verilog RTL code and standard cell library as inputs and generates physical chip layout in GDSII format. OpenROAD also produces timing, area and power characteristics of the Verilog code. Here, we use CMOS $45$ nm Nandgate Open Cell Library in OpenROAD. 

Our OpenROAD timing report shows that out implementation of SAIL, Poptrie, CP-Trie and TabTree can achieve $1$ GHz without timing violation. That is, our generated RTL of pipelined ASIC can lookup or classify $1$ packet in every $1$ ns. Thus, they can process $1$ billion packets per second. 

Note that, currently there is no SRAM available for $45$ nm Nandgate cell library, to the best of our knowledge. This is why, we use registers to implement the array. Table \ref{powerconsumption_openroad} shows the summery of OpenROAD report for SAIL, Poptrie, CP-Trie and TabTree. This shows that C2RTL can be instrumental in evaluating different IP lookup and packet classification algorithms at system level. 

\section{Discussion}\label{discussion}
C2RTL is aimed to generate various intellectual property (IP) blocks for routing ASICs. However, more works need be done to achieve the goal.

Currently, C2RTL works for trie based IP lookup and packet classification algorithms. We also need to support other dataplane functions, such as, ethernet lookup \cite{kim2013swsl}, OpenFlow packet classification \cite{hsieh2016many}, queuing \cite{sivaraman2015towards}, router-assisted congestion control \cite{islam2017network, tamimrcp}, heavy-hitter detection \cite{sivaraman2016packet}, hash based IP lookup and packet classification algorithms \cite{pong2011concise, li2020tuple} and so on.

The QoRs of HLS-generated RTL code often lag behind those of manual RTL code \cite{evolutionhls, lahti2018we}. A research should be done to compare the QoRs of C2RTL to those of manually written RTL. Currently, C2RTL depends on GCC to optimize the code. However, it has shown that a custom compiler flow can improve the QoRs of HLS \cite{lattuada2019design}. A research should be done on improving QoRs of C2RTL.

Finally, the IPs generated by C2RTL have be integrated with other IPs in a routing SoC. A research should be done on the integration the IPs. We also need to tape-out the resulting SoC to validate our flow. 

\section{Conclusion}\label{conclusion}
This paper presents a high-level synthesis tool named C2RTL. C2RTL takes an IP lookup or packet classification algorithm (in C) as input and generates corresponding Verilog RTL for pipelined ASIC. Our evaluation shows that the Verilog RTL produced by C2RTL work properly in simulation and physical synthesis.

\bibliographystyle{IEEEtran}
\bibliography{c2rtl-hpsr}

\end{document}
