\PassOptionsToPackage{warn}{textcomp}

\documentclass[sigconf]{acmart}



% Disable / remove copyright boxes
\setcopyright{none}
\settopmatter{printacmref=false}
\renewcommand\footnotetextcopyrightpermission[1]{}

% Increase margin between text and footer
\setlength{\footskip}{20pt}


\usepackage{balance}
\usepackage{graphicx,multirow,booktabs,caption, array, listings}
\usepackage{url}
\usepackage{listings}
\usepackage{textcomp}
\usepackage{gensymb}
\usepackage{float}
\usepackage{caption} 
\usepackage{subcaption}
\usepackage{array}
\usepackage{listings}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{color, colortbl}
\usepackage{siunitx}


% Increase margin between text and footer
\setlength{\footskip}{20pt}

% Add CCR footer
\usepackage{fancyhdr}
\fancypagestyle{plain}{%
	\fancyhf{} %
	\fancyfoot[L]{ACM SIGCOMM Computer Communication Review}%
	\fancyfoot[R]{Volume 57 Issue 1, April 2020}%
}
\pagestyle{plain}

% Add CCR footer on first page
\fancypagestyle{firstpagestyle}{%
	\fancyhf{} %
	\fancyfoot[L]{ACM SIGCOMM Computer Communication Review}%
	\fancyfoot[R]{Volume 57 Issue 1, April 2020}%
}

\begin{document}

\title{C2RTL: A High-level Synthesis System for IP Lookup and Packet Classification}

\author{MD Iftakharul Islam, Javed I Khan}
\affiliation{
	\institution{Kent State University, USA}
}
\email{mislam4@kent.edu, javed@cs.kent.edu}

\begin{abstract}
IP lookup and packet classification are two core functions of a router. IP lookup involves performing longest prefix match (LPM) on the destination IP address. Packet classification involves finding the best match in a multi-field ruleset, generally consists of source and destination IP addresses and port numbers. This paper focuses on pipelined ASIC based IP lookup and packet classification. Pipelined ASICs are traditionally designed in a register transfer level (RTL) hardware description language (HDL) such as Verilog, VHDL and SystemVerilog. However, manually writing hardware logic is notoriously complicated and painful. This paper presents a High Level Synthesis (HLS) system named C2RTL. C2RTL generates hardware logic in Verilog RTL directly from IP lookup and packet classification algorithms implemented in C. C2RTL is implemented as a plugin of GCC compiler. It takes an IP lookup or packet classification algorithm (in C) as an input and generates corresponding synthesizable Verilog RTL code for pipelined ASIC. We developed several IP lookup and packet classification algorithms in C2RTL and generated corresponding Verilog RTL. We evaluated the resulting RTL code with OpenROAD EDA.

\end{abstract}

\ccsdesc[500]{Networks~Routers; Network algorithms; ASIC}

\keywords{High-level synthesis; Pipelined ASIC; IP lookup; Packet classification}

\maketitle

\section{Introduction}\label{sec:intro}
IP lookup and packet classification are two fundamental functions of a router. IP lookup involves performing longest prefix match of the desination IP address. For instance, Table \ref{lpm} shows an example forwarding table of a router. If the destination IP address of an incoming packet is $169.254.198.1$, then the packet should be forwarded to next-hop $1$. Again if the destination IP address is $169.254.190.5$, the packet should be forwarded to next-hop $3$. Again, packet classification involves finding the best match for a packet in a multi-field ruleset. For instance, Table \ref{classi} shows an example ruleset. The ruleset contains source IP, destination IP, source port and destination port. Each rule in the ruleset also has a priority. Here, we can have exact match, prefix match and range match. A packet may match with more than one rules. In that case, rules with higher priority determines the action.

IP lookup and packet classification in routing chips mainly fall into two categories: TCAM and pipelined ASIC based solutions. TCAM is very expensive, area-inefficient and power hungry. It also requires very complex incremental update and does not support range match. On the other hand, pipelined ASIC utilizes on-chip SRAM which is cheaper and consumes less power. Pipelined ASIC can implement various algorithmic solutions of IP lookup and packet classification. In this paper, we focus on ASIC-based IP lookup and packet classification.

\begin {table}[H]
\caption {Example forwarding table (for IP lookup)} \label{lpm} 
\begin{center}
	\begin{tabular}{ | m{3.6cm} | m{3cm} |} 
		\hline Prefix & Next-hop\\
		\hline $10.18.0.0/22$ & $1$\\
		\hline $131.123.252.42/32$ & $2$\\
		\hline $169.254.0.0/16$ & $3$\\
		\hline $169.254.192.0/18$ & $1$\\		
		\hline $192.168.122.0/24$ & $2$\\						
		\hline 
	\end{tabular} 	
\end{center}
\end {table}

\begin {table}[H]
\caption {Example ruleset (for packet classification)} \label{classi} 
\begin{center}
	\begin{tabular}{ | m{.4cm} | m{.9cm} | m{1.4cm} | m{.9cm}| m{1.4cm} | m{.7cm}|} 
		\hline Rule & Source IP & Destination IP& Source port & Destination port & Action\\
		\hline r1 & $011*$ & $011*$ & $2-9$ & $1-2$ & a1\\
		\hline r2 & $110*$ & $*$ & $1-9$ & $7-9$ & a2\\
		\hline r3 & $*$ & $011*$ & $10$ & $10$ & a1\\
		\hline r4 & $*$ & $*$ & $*$ & $*$ & a2\\
		\hline 
	\end{tabular} 	
\end{center}
\end {table}

Traditionally, ASIC based hardware blocks are implemented in a RTL level HDL (e.g. Verilog, VHDL and SystemVerilog). However, designing hardware at RTL level is a very tedious process. Here, designers need to use low level abstraction to implement their logic. The RTL code also needs to be cycle accurate. Thus, designers need to consider the path latency of the circuit and insert registers manually to implement pipelining. Such tedious process calls for designing hardware at a higher level. High-level synthesis \cite{coussy2009introduction} allows us to design pipelined ASIC in a high-level language such as C or SystemC \cite{SystemC}. High level synthesis (HLS) systems generate RTL level hardware logic directly from programs implemented in high level languages  such as C or SystemC. Thus, it can improve designer productivity significantly. SWSL \cite{kim2013swsl} presented a high-level synthesis for IP lookup. However, SWSL is not open source and seems to be abandoned. High level synthesis is very long standing research area \cite{cong2011high}. However, it has not been adopted in routing/switching chips yet, to the best of our knowledge.

High-level synthesis has many other benefits. It does not require  hardware/software codesign. Here, verification can be done entirely in software. It also allows us innovate at system level. In the past, semiconductor industry mainly had focused on shrinking transistor size in order to achieve higher performance. As Moore's law is slowing down, it has become more important to innovate at system level. High-level synthesis allows us to explore different algorithmic solutions at system level. This paper presents a high level synthesis system named C2RTL. C2RTL takes an IP lookup or packet classification algorithm (implemented in C) as an input and generates corresponding synthesizable Verilog RTL code.

The remaining of the paper proceeds as follows.  Section II presents the related works. Section III characterizes IP lookup and packet classification algorithms for ASIC. Section IV describes the programming convention. Section V describes the system overview by explaining the design flow. Section VI discusses the implementation. Section VII shows the experimental results. Finally, Section VIII concludes the paper.
 

\section{Related Works}
The first generation of commercial high-level synthesis tools appeared during the 1990s \cite{coussy2009introduction}. Early HLS tools include Mentor's Monet \cite{elliott1999understanding}, Synopsys' Behavioral Compiler \cite{knapp1996behavioral}, IMPACT \cite{khouri1998impact},  and so on. These tools mainly used behavioral Verilog or VHDL to generate RTL level Verilog or VHDL code.  However, they did not received wide spread adoption because behavioural HDLs were not popular among algorithm designers. 

Since 2000, new generation of high-level synthesis tools focused on system design based on C or SystemC. They take high level program implemented in C or SystemC as input and generate RTL of pipelined ASIC. These tools include Mentor's Catapult \cite{coussy2009introduction, book2010mentor}, Cadence's Stratus (formerly C-to-Silicon Compiler \cite{kondratyev2011realistic} and Forte's Cynthesizer \cite{sanguinetti2012transaction}), NEC's CyberWorkBench\cite{wakabayashi2008all}, Synopsys' Synphony (originated as PICO-NPA \cite{schreiber2002pico} from HP lab) and so on. These tools however are not open source. There is no study on the effectiveness of these high-level synthesis tools for IP lookup and packet classification algorithms, to the best of our knowledge. 

There has been relatively little work on high-level synthesis for IP lookup and packet classification. SWSL \cite{kim2013swsl} is an academic high-level synthesis tool that takes an IP lookup program as input and generates synthesizable Verilog RTL. SWSL was developed based on the observation that most network lookup algorithms consists of simple algorithmic steps where each step only accesses variables from its own state. SWSL converts each step to Verilog RTL. SWSL has been used to implement simple trie based IP lookup. SWSL appears to be abandoned at this moment. 

Several research projects used high-level languages such as C and C++ to program programmable pipeline based ASIC. LEAP \cite{harris2012leap} and PLUG \cite{de2009plug} proposed hardware accelerators and compilers where network lookup or packet classification algorithm can be implemented in a high-level language (C/C++) and compiled to the programmable hardware accelerator. LEAP and PLUG have been used to implement IP lookup and packet classification in hardware \cite{harris2012leap, de2009plug, vaish2011experiences}. Domino \cite{sivaraman2016packet} presents a switch compiler which takes a high level program written in C as input and generate machine code for programmable pipelined based switches. Domino has been used to implement IP lookup algorithm in switching ASIC\cite{islam2019sail}. Recently, Chipmunk \cite{gao2020switch} used program synthesis method to produce switch machine code from C program. They used their method to generate machine code for Barefoot's Tofino switch \cite{bosshart2013forwarding}. Thus, it can also be used to implement data plane function in programmable ASIC. In this paper however, we mainly focus on fixed function ASIC.

There are several high-level synthesis systems for FPGA. These include Xilinx Vivado (formerly  AutoPilot \cite{cong2011high}, originated as xPilot \cite{chen2005xpilot} at UCLA), Bambu \cite{pilato2013bambu}, LegUp \cite{canis2011legup}, ASC \cite{mencer2006asc}, CASH \cite{budiu2004spatial}, SPARK \cite{gupta2007spark}, GAUT \cite{coussy2008gaut}, Trident \cite{tripp2005trident}, ROCCC \cite{guo2008compiler} and so on. These solutions however cannot generate RTL for pipelined ASIC, at this moment.

Alladin \cite{shao2014aladdin} proposed an accelerator simulator which take a C function as input and generates power and area without creating the actual RTL. Alladin does not support control-flow intensive programs, thus can not be used for IP lookup and packet classification algorithms.

This paper presents a high-level synthesis tool named C2RTL. We implemented several IP lookup and packet classification algorithms in C2RTL. Our experiment shows that C2RTL can be instrumental in designing IP lookup and packet classification in pipelined ASIC. We also evaluated the resulting Verilog RTL using OpenROAD. Thus, C2RTL+OpenROAD allows us to generate physical chip layout and area/power characteristics of different algorithms from high level program implemented in C. This approach can be instrumental in evaluating different algorithms at system level.    

\begin{figure}[!t]
	\centering
	\includegraphics[width=9cm]{sail_code}
	\caption{An example trie based IP lookup in C}\label{motivation}
\end{figure}


\section{IP lookup and Packet classification Algorithms for ASIC}
This section characterizes IP lookup and packet classification algorithms for ASIC. IP lookup in ASIC mainly fall into two categories: trie \cite{yang2014guarantee, bando2012flashtrie, baboescu2005tree, basu2005fast, jiang2008beyond, kumar2006camp, hasan2005dynamic} and hash \cite{pong2011concise, pong2009suse, bando2012flashtrie, broder2001using, hasan2006chisel, bando2009flashlook, song2009ipv6} based solutions. In the same way, packet classification algorithms also fall into two categories: decision tree/trie \cite{jiang2009large, li2013hybridcuts, li2018cutsplit, li2019tabtree} and hash \cite{srinivasan1999packet, li2019tabtree, pfaff2015design} based solutions. This paper focuses on trie based IP lookup and packet classification algorithms. 

Figure \ref{motivation} shows an example trie based IP lookup algorithm named SAIL \cite{yang2014guarantee}. Here, the trie is split among multiple levels. The lookup algorithm traverses level $16$, $24$, $32$, $40$ and $48$ in order until it finds a leaf. This is why, the IP lookup algorithm requires several nested conditional statements. Note that, here we only showed lookup upto level $48$. An actual IPv6 lookup algorithm needs to visit up to level $128$ as an IPv6 address is 128 bit long. Thus, an actual IPv6 lookup algorithm will need many more nested conditional statements. This is why, trie-based IP lookup algorithms can be characterized as a control-flow intensive (CFI) application. Tree/trie based packet classification also fall into the same category. They also do not require loop. IP lookup or packet classification algorithm also needs to be implemented as a pipeline so that resulting hardware can process packets at line-rate. 

Currently, open source HLS tools such as Bambu \cite{pilato2013bambu} and LegUP \cite{pilato2013bambu} support control-flow intensive applications, however they were primarily designed for FPGA, not ASIC. They also do not support pipelining. This motivated us to design C2RTL. C2RTL is designed to generate piplined ASIC for control-flow intensive applications. Here, we primarily focus on IP lookup and packet classification algorithms. However, we intend to support other dataplane functions, such as, ethernet lookup \cite{kim2013swsl}, OpenFlow packet classification \cite{li2018cutsplit}, queuing \cite{sivaraman2015towards}, router-assisted congestion control \cite{tamimrcp}, heavy-hitter detection \cite{sivaraman2016packet} and so on in the future.


\begin {table}[H]
\caption {programming restrictions in C2RTL} \label{program_restrictions} 
\begin{center}
	\begin{tabular}{ | m{8cm}|}
		\hline No loop (while, for, do-while)\\
		\hline No unstructured control flow (goto, break, continue) \\
		\hline No ternary operation \\
		\hline No dynamic memory allocation\\
		\hline No global variables\\
		\hline No structure\\
		\hline No switch\\
		\hline No function call\\
		\hline Each branch has to have a separate return statement\\
		\hline 
	\end{tabular} 	
\end{center}
\end {table}


\section{Programming Convention}
An input program in C2RTL is implemented in C language, but with several restrictions (Table \ref{program_restrictions}). Although C allow us to implement an algorithm in different ways, C2RTL requires us to adhere to a programming convention. These restrictions allow us to produce intermediate code (via GCC compiler) in a particular format. Note that, other high-level synthesis tools also often impose similar restrictions \cite{book2010mentor, pilato2013bambu}.

C2RTL takes a C function as an input. For instance, Figure \ref{sail_c} shows an example C function which will be processed by C2RTL. Note that, all the arrays here are passed as function arguments. If an application needs to update state, the states can be implemented as a pointer variable and has to be passed as an argument. Each array or pointer variable can be implemented as a SRAM block or mutiport SRAM or mutiport register file. Similar coding convention has also been adopted in many other HLS tools including Mentor Catapult, Bambu and so on \cite{book2010mentor, pilato2013bambu}. As we pass all the input and arrays as function parameter, we do not support global variables. C2RTL also requires each branch to have a separate \textit{return} statement (as shown in Figure Figure \ref{sail_c}). We found that GCC does not produce PHI operation \cite{ssawiki, ssagcc} properly if we do not have a separate \textit{return} statement in each branch. This is why, although C allow us to merge multiple branch statements (having a single \textit{return} for multiple branches), this is not not allowed in C2RTL. Note that, other HLS systems also impose similar restrictions. For instance, Mentor Catapult requires multiple branch statements to have just one a single \textit{return} statement (opposite of what C2RTL expects) \cite{book2010mentor}. C2RTL also does not support several C constructs including \textit{loop}, \textit{switch}, \textit{ternary operation}, \textit{struct}, \textit{function call}, \textit{goto} and so on. A \textit{loop-condition} that can only be evaluated at run-time cannot be implemented as a pipeline ASIC. This is why, we do not support \textit{loop} in C2RTL. Again, dynamic memory allocation is not possible to implement in hardware. Thus, HLS tools do not support dynamic memory allocation.

\begin{figure}[!t]
	\centering
	\includegraphics[width=8cm]{c2rtl-flow}
	\caption{C2RTL design flow}\label{c2rtl-flow}
\end{figure}

\begin{figure*}[!t]
	\begin{subfigure}[t]{0.5\textwidth}
		\centering
		\includegraphics[width=7cm]{sail_c}
		\caption{Example IP Lookup in C}\label{sail_c}
	\end{subfigure}%
	~ 	
	\begin{subfigure}[t]{0.5\textwidth}
		\centering
		\includegraphics[width=7cm]{cfg}
		\caption{Intermediate code in SSA and CFG form (generated by GCC)}\label{cfg}
	\end{subfigure}%
	
	\begin{subfigure}[t]{.81\textwidth}
		\centering
		\includegraphics[width=14.5cm]{cdfg}
		\caption{CDFG}\label{cdfg}
	\end{subfigure}%
	~ 	
	\begin{subfigure}[t]{0.19\textwidth}
		\centering
		\includegraphics[width=3.5cm]{scheduling}
		\caption{Scheduling}\label{scheduling}
	\end{subfigure}%	
	
	\caption{CDFG construction and scheduling}\label{cdfg-construction}
\end{figure*}


\section{System Overview}

Figure \ref{c2rtl-flow} illustrates the design flow of C2RTL. 


\textbf{Step 1: Intermediate code generation.} C2RTL leverages GCC compiler to produce intermediate code from a C function. GCC takes a C function as an input and generate intermediate code in the form of a control flow graph (CFG). Figure \ref{cfg} shows the CFG constructed from the example code shown in Figure \ref{sail_c}. Each node in CFG is a basic block. A basic block contains intermediate code that can be executed sequentially without any branching. Edges between basic blocks represent the transfer of control flow from one basic block to another. Here, each basic block contains intermediate code in Static Single Assignment (SSA) \cite{ssawiki, ssagcc} form. SSA assigns each variable a different name with a version field. When a variable is assigned to a new value, the version field is increased. Thus, SSA guarantees that no versioned variable is assigned twice. This is how, it removes the Write-After-Read and Write-After-Write dependencies. In SSA instructions, there are only Read-After-Write dependency remains. Again, as our input program has multiple control flows, SSA adds a $\phi$ function \cite{ssawiki, ssagcc} in the last basic block of the CFG (shown as $<L4>$ in \textit{bb\_7}). The SSA $\phi$ function selects output from one of the input flows. C2RTL obtains the intermediate code along with CFG from GCC using plugin API \cite{GCCplugin}. 

\textbf{Step 2: Microarchitecture optimization.} C2RTL performs standard compiler optimizations such as replacing multiplications and divisions with bitwise operations, operand width reduction, copy propagation and so on. 

\textbf{Step 3: CDFG construction.} Control and data flow graph (CDFG) is the de facto standard of circuit representation in almost all kinds of high-level synthesis tools \cite{khouri1998impact, lattuada2015code, book2010mentor, shao2014aladdin, wakabayashi2008all, kondratyev2011realistic}. C2RTL constructs a CDFG from the intermediate code. Figure \ref{cdfg} shows the CDFG constructed from the intermediate code shown in Figure \ref{cfg}. There are two types of vertices in CDFG: basic block and operation vertex. A basic block vertex represents a basic block and an operation vertex represents an operation within a basic block. In Figure \ref{cdfg}, basic block vertices are shown as rounded rectangles and operation vertices are shown as rectangles or trapezoids. We assign an unique ID to each operation vertex. There are two types of edges in CDFG: data and control edge. A data edge indicates data flow and a control edge indicates a control flow. Data and control edges are shown as solid and dashed arrows respectively. Finally, C2RTL generates a MUX tree from the $\phi$ function. For instance, C2RTL converts $<L4>$ $\phi$ operation in Figure \ref{cfg} into two MUX operations in Figure \ref{cdfg}. Note that, a CDFG is analogous to a logic circuit. Here, each operation can be implemented as a combinational\footnote{https://en.wikipedia.org/wiki/Combinational\_logic} or sequential\footnote{https://en.wikipedia.org/wiki/Sequential\_logic} logic circuit. In order to implement pipelining, we also need to insert registers among the logic elements. A register holds the output of a logic circuit to be used in the next clock cycle (in the next pipeline stage). Thus, we also need to perform scheduling of each operation in order to divide them among the pipeline stages. 

\textbf{Step 3: Scheduling.} Scheduling is considered as one of the most critical steps of high-level synthesis. C2RTL uses  \textit{as late as possible (ALAP)} scheduling algorithm \cite{hwang1991formal} to schedule each operation in CDFG. Here, we use the latency as reported in Kondratyev et el. \cite{kondratyev2011realistic} for performing scheduling. We also use $1$ GHz as our target frequency (i.e. each cycle is $1$ ns). Figure \ref{scheduling} shows the scheduling results for the CDFG. Here operations scheduled in the same clock cycle will be executed in the same pipeline stage.

\begin{comment}
Scheduling in HLS may vary based on target implementation. For instance, HLS tools for FPGA use SDC scheduling algorithm\cite{cong2006efficient, lattuada2015code}. However, SDC scheduling algorithm cannot use pipelining \cite{bibid}  
\end{comment}

\textbf{Step 4. Register Insertion.} In pipelined ASIC, if the result of an operation is used in an another operation scheduled in a different clock cycle, we need to insert a register after the former operation to hold its result. For instance, Figure \ref{cdfg} shows that output of operation $2$ is used as the input to operation $3$. However, Figure \ref{scheduling} shows that operation $2$ and $3$ are scheduled in different clock cycles. In this case, we need to insert a register after operation $2$ to store its value so that operation $3$ can use it in the next clock cycle. C2RTL inserts register after each operation where the result of the operation crosses the cycle boundary. It also inserts a register after the output to hold the result.

\textbf{Step 5: RTL generation.} This step generates synthesizable\footnote{Verilog has many language constructs which are not synthesizable (i.e. cannot be used to generate physical chip layout). We do not use them here.} Verilog RTL from the CDFG. We implement each operation of CDFG using a verilog module from a component library. Here, we adapted the component library used in Bambu \cite{pilato2013bambu}. The component library consists of RTL-level synthesizable Verilog modules that implements primitive operations such as arithmetic operations, bitwise operations, MUX, register, SRAM and so on. The \textit{arrays} in the input program are implemented as a multiport register file or SRAM. This is how, C2RTL generates synthesizable Verilog code corresponding to the CDFG. C2RTL also generates a verilog testbench for the input program.

\begin{figure}[!t]
	\centering
	\includegraphics[width=5cm]{hls-flow}
	\caption{C code to physical chip layout generation}\label{hls-flow}
\end{figure}

\subsection{Resource binding}
Resource binding is a critical step of a high-level synthesis tool \cite{coussy2009introduction, book2010mentor}. A component library can have multiple implementations of an operation. There are often area, power and timing tradeoffs among the implementations \cite{kondratyev2012exploiting}. A high-level synthesis tool has to perform module selection such that timing requirements are met while area and power consumption are minimized. This process is known as resource binding or design space exploration. Modern HLS tools perform resource binding during scheduling (instead of two separate steps namely resource allocation and resource binding) \cite{kondratyev2012exploiting}. However, our component library has only one module for each operation, thus we do not perform special resource binding in this paper.

\section{Implementation}
We implemented C2RTL as a GCC plugin (around $3,000$ lines of C code). The source code will be found in the artifact.\footnote{https://www.dropbox.com/sh/ae6zmoxds36mzpn/AAAhXHGcxjFD-Ao6DL9lWQ0Na?dl=0} We will make the source code publicly available in the future. We use $1$ ns clock cycle for scheduling. Thus, the Verilog RTL generated by C2RTL is designed to run at $1$ GHz.

\section{Evaluations}
We implemented several IP lookup and packet classification algorithms in C. We generate corresponding Verilog RTL code from the C implementations. We then evaluate the Verilog RTL codes using Icarus Verilog\footnote{http://iverilog.icarus.com/}. The verilog simulator allows us to do functional verification of the generated Verilog RTL. Our functional verification shows that the generated Verilog RTL produces correct output. 

We further evaluate the generated RTL code by an electronic design automation (EDA) tool. EDA tools such as OpenROAD \cite{ajayi2019toward}, Synopsis Fusion compiler \cite{icc}, Cadence Innovus \cite{innovus} enable us to generate physical chip layout directly from Verilog RTL code. Here, we use OpenROAD. OpenROAD takes Verilog RTL code, macros (e.g. SRAM, I/O ports, etc), a cell library and constraints (e.g. clock cycle) as inputs and generates corresponding physical chip layout in GDSII format. Figure \ref{hls-flow} shows our C code to physical chip layout generation mechanism. Here, OpenROAD takes Verilog RTL code and standard cell library as input and generates physical chip layout in GDSII format. OpenROAD also produces timing, area and power characteristics of the Verilog code. Here, we use CMOS $45$ nm Nandgate Open Cell Library in OpenROAD. We also use $1$ ns clock cycle as constraint. Our OpenROAD reports show that RTL code generated by C2RTL can run at $1$ GHz (i.e. clock cycle is $1$ ns). That is, the FIB lookup module can perform a lookup at every $1$ ns. Thus, the throughput of pipelined ASIC is $1$ billion packets per second. 

Table 1 shows the power and area for the FIB lookup algorithms.   
 

\begin {table}[H]
\caption {OpenROAD synthesis summery} \label{powerconsumption_openroad} 
\begin{center}
	\begin{tabular}{ | m{2.5cm}| m{2cm}| m{2cm} |}
		\hline  & Poptrie & CP-Trie\\
		\hline Clock speed & $400$ MHz & $454$ MHz \\
		\hline Internal Power & $29.5$ mW& $23.3$ mW\\
		\hline Switching Power & $15.8$ mW & $13.2$ mW\\
		\hline Leakage Power & $2.66$ mW& $2.23$ mW\\
		\hline Total Power & $48.00$ mW & $38.8$ mW\\
		\hline Area & $0.687995$ $mm^2$ & $0.687995$ $mm^2$\\
		\hline 
	\end{tabular} 	
\end{center}
\end {table}


\section{Conclusion}
This paper presents a high-level synthesis tool named C2RTL. C2RTL takes IP lookup or packet classification algorithms (in C) as input and generates corresponding Verilog RTL for pipelined ASIC. To evaluate C2RTL, we implemented several IP lookup or packet classification algorithms in C and fed them through C2RTL. The resulting Verilog RTL work properly in simulation and physical synthesis.

\bibliographystyle{abbrv}
\bibliography{c2rtl}

\begin{comment}

\appendix

\section{Software Implementation}
The software implementation will be found in the "CP-Trie" folder of the artifact.\footnote{https://www.dropbox.com/sh/ae6zmoxds36mzpn/AAAhXHGcxjFD-Ao6DL9lWQ0Na?dl=0} The folder contains the implementation of CP-Trie, SAIL and Poptrie. We have implemented all these algorithms in a single program. The folder also contains FIB dataset and a 'README.md' file which contains the instructions to run the program. To reproduce the results of this paper, one needs to download the folder on a Linux machine. Although we conducted the experiment on an Ubuntu Gnome $16.04$, it should work on any Linux platform. Then one can compile the program with a simple 'make' command in the terminal. This will compile the program and create an executable file named 'main\_ipv6'.  Now one can run the program on the terminal as './main\_ipv6'. Then then program will test FIB update and FIB insert for the data set and write the results in a text file named 'summery.data'. 

\section{ASIC Implementation}
The ASIC implementation will be found in the "aladdin" folder of the artifact. To reproduce the results of this paper, one needs to download the "aladdin" folder on a Linux machine and follow the instruction in the 'README.md'. The implementation of CP-Trie, SAIL and Poptrie will be found in "fib\_lookup" folder inside "aladdin". Note that the 'README.md' explains how to run the 'triad' program inside SHOC folder. One needs to follow the similar procedure to run CP-Trie, SAIL and Poptrie programs inside "fib\_lookup" folder.

\end{comment}

\end{document}
