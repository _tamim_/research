% Copyright 2004 by Till Tantau <tantau@users.sourceforge.net>.
%
% In principle, this file can be redistributed and/or modified under
% the terms of the GNU Public License, version 2.
%
% However, this file is supposed to be a template to be modified
% for your own needs. For this reason, if you use this file as a
% template and not specifically distribute it as part of a another
% package/program, I grant the extra permission to freely copy and
% modify this file as you see fit and even to delete this copyright
% notice. 

\documentclass{beamer}
\usepackage{graphicx,multirow,booktabs,caption, array, listings}
\usepackage{times}  
\usepackage{url}
\usepackage{listings}
\usepackage{textcomp}
\usepackage{gensymb}
\usepackage{float}
\usepackage{caption} 
\usepackage{subcaption}
\usepackage{array}
\usepackage{listings}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{amsmath}
\usepackage{color, colortbl}
\usepackage{cite}
\usepackage{siunitx}

\usepackage{array}


% There are many different themes available for Beamer. A comprehensive
% list with examples is given here:
% http://deic.uab.es/~iblanes/beamer_gallery/index_by_theme.html
% You can uncomment the themes below if you would like to use a different
% one:
%\usetheme{AnnArbor}
%\usetheme{Antibes}
%\usetheme{Bergen}
%\usetheme{Berkeley}
%\usetheme{Berlin}
%\usetheme{Boadilla}
%\usetheme{boxes}
%\usetheme{CambridgeUS}
%\usetheme{Copenhagen}
%\usetheme{Darmstadt}
%\usetheme{default}
%\usetheme{Frankfurt}
%\usetheme{Goettingen}
%\usetheme{Hannover}
%\usetheme{Ilmenau}
%\usetheme{JuanLesPins}
%\usetheme{Luebeck}
\usetheme{Madrid}
%\usetheme{Malmoe}
%\usetheme{Marburg}
%\usetheme{Montpellier}
%\usetheme{PaloAlto}
%\usetheme{Pittsburgh}
%\usetheme{Rochester}
%\usetheme{Singapore}
%\usetheme{Szeged}
%\usetheme{Warsaw}

\setbeamertemplate{footline}[frame number]{}
\setbeamertemplate{navigation symbols}{}


\title{SAIL Based FIB Lookup in a Programmable Pipeline Based Linux Router}


\author{MD Iftakharul Islam, Javed I Khan}
% - Give the names in the same order as the appear in the paper.
% - Use the \inst{?} command only if the authors have different
%   affiliation.

\institute[Kent State University] % (optional, but mostly needed)
{
	Department of Computer Science\\
	Kent State University\\
	Kent, OH, USA.
}

\date[\today]{}

% - Use the \inst command only if there are several affiliations.
% - Keep it simple, no one is interested in your street address.

% This is only inserted into the PDF information catalog. Can be left
% out. 

% If you have a file called "university-logo-filename.xxx", where xxx
% is a graphic format that can be processed by latex or pdflatex,
% resp., then you can add a logo as follows:

% \pgfdeclareimage[height=0.5cm]{university-logo}{university-logo-filename}
% \logo{\pgfuseimage{university-logo}}

% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:


% Let's get started
\begin{document}
	
	\begin{frame}
		\titlepage
	\end{frame}
	
	\begin{frame}{Outline}
		\tableofcontents
		% You might wish to add the option [pausesections]
	\end{frame}
	
\section{Problem statement}

	\begin{frame}{Longest Prefix Matching}
		\begin{itemize}
			\item A router needs to perform longest prefix matching to find the outgoing port.
			
		\begin {table}[H]
		\caption {Routing table (also known as FIB table) } \label{lpm} 
		\begin{center}
			\begin{tabular}{ | m{3.6cm} | m{3.6cm} |} 
				\hline Prefix & Outgoing port\\
				\hline $10.18.0.0/22$ & $eth1$\\
				\hline $131.123.252.42/32$ & $eth2$\\
				\hline $169.254.0.0/16$ & $eth3$\\
				\hline $169.254.192.0/18$ & $eth4$\\		
				\hline $192.168.122.0/24$ & $eth5$\\						
				\hline 
			\end{tabular} 	
		\end{center}
		\end {table}
		\item $169.254.198.1 \implies eth4$
		\item $169.254.190.5 \implies eth3$
			
		\end{itemize}
		
	\end{frame}

	
	\begin{frame}{Explosion of Routing Table}
		\begin{itemize}
		\item	
		\begin{figure}[!t]
			\centering
			\includegraphics[width=8cm]{bgpreport.png}
			\caption{The number of routes in the Internet backbone routers}\label{bgp-report}
		\end{figure}		
			
			\item A backbone router needs to perform around $1$ billion routing table lookups per second to sustain the line rate.
			
			\item Performing FIB lookup at such a high rate in such a large routing table is particularly challenging. 
		
			
		\end{itemize}
	\end{frame}
	
\section{A look inside a Linux router}
	
	\begin{frame}{FIB lookup in a Linux router}
		\begin{itemize}

		\item
		\begin{figure}[!t]
			\centering
			\includegraphics[width=5cm]{Linux-Router.png}
			\caption{Linux Router}\label{router-linecard}
		\end{figure}
								
			\item Here Linux kernel works as a control plane and a programmable pipeline based VLIW processor works as a dataplane.
			
			\item We have implemented our FIB lookup in Linux kernel.
			
			\item We also have implemented the FIB lookup in Domino which would be executed on the dataplane.
						
		\end{itemize}
	\end{frame}

\section{SAIL based FIB lookup}

	\begin{frame}{SAIL based FIB Lookup}
		\begin{itemize}

			\item Recently several FIB lookup algorithms have been proposed that exhibit impressive lookup performance.
		\begin{itemize}
			\item These include SAIL \textit{[SIGCOMM 2014]}, Poptrie \textit{[SIGCOMM 2015]}.
			\item We chose SAIL as a basis of our implementation as it outperforms other solutions
		\end{itemize}
		
			\item The main drawback of SAIL is its very high memory consumption. For instance, it consumes $29.22$ MB for our example FIB table with $760K$ routes.
			
			\item We have used \textit{population-counting} (a data structure) that reduces memory consumption up to $80\%$. 
			
			\item SAIL has two variants namely SAIL\_L and SAIL\_U.
			
			\item We have implemented both variants with \textit{population-counting} in both Linux kernel and Domino.
			
			\item Our implementation shows that SAIL is able to perform FIB lookup at line rate in a VLIW processor.
			
			\item We also have compared the performance of SAIL\_L and SAIL\_U (with \textit{population-counting}) in Linux kernel and Domino.
			
		\end{itemize}

	\end{frame}
	
	\begin{frame}{SAIL based FIB lookup}

		\begin{itemize}
			\item We first show how SAIL\_U constructs its data structure.
			\item SAIL divides a routing table into three levels: level $16$, $24$ and $32$.
			\item However for simplicity, in this example, we divide the routing table into level $3$, $6$ and $9$. 
			\item We then show how \textit{population-counting} is used on the data structure.
		\end{itemize}

		\begin{figure}
			
			\begin{subfigure}[t]{0.45\textwidth}
				\centering
				\includegraphics[width=3cm]{routing_table}
				\caption{FIB}\label{routingtable}
			\end{subfigure}%
			~ 
			\begin{subfigure}[t]{0.55\textwidth}
				\centering
				\includegraphics[width=4cm]{treewopush}
				\caption{Binary tree corresponding to the FIB}\label{treewopush}
			\end{subfigure}%
			

		\end{figure}
		
	\end{frame}
	
	\begin{frame}{SAIL based FIB lookup (level pushing)}
		
		\begin{figure}			
			\begin{subfigure}[t]{0.4\textwidth}
				\centering
				\includegraphics[width=4cm]{treewopush}
				\caption{Binary tree}\label{treewopush}
			\end{subfigure}%
			~
			\begin{subfigure}[t]{0.6\textwidth}
				\centering
				\includegraphics[width=5cm]{tree}
				\caption{Solid nodes in level $1-2$ are pushed to level $3$; solid nodes in level $4-5$ are pushed to level $6$; solid nodes in level $7-8$ are pushed to level $9$}\label{routingtable}
			\end{subfigure}%
			
			\caption{Tree construction in SAIL}
		\end{figure}
		
	\end{frame}
	
	\begin{frame}{SAIL based FIB lookup (array construction)}
		
		\begin{figure}			
			\begin{subfigure}[t]{0.6\textwidth}
				\centering
				\includegraphics[width=4cm]{tree}
				\caption{Tree}\label{routingtable}
			\end{subfigure}%

			\begin{subfigure}[t]{\textwidth}
				\centering
				\includegraphics[width=5cm]{array}
				\caption{$N$ is the next-hop array and $C$ is the chunk ID array. There will be a chunk in level $6$ for each prefix in level $3$ which has a longer prefix. \textbf{Most of the entries in $C_6$ remains $0$ in practice. However it consumes around $23.16$ MB in a real backbone router}}\label{chunk}
			\end{subfigure}%
	
			\caption{Tree construction in SAIL}
		\end{figure}
		
	\end{frame}
	
	
\section{SAIL with Population Counting}
	\begin{frame}{Population counting}
		
		\begin{itemize}			
			\item It's a data structure which was presented in the book \textit{ Hacker's Delight (2002)}.
		\end{itemize}			
		
		\begin{figure}			
			
			\begin{subfigure}[t]{\textwidth}
				\centering
				\includegraphics[width=6cm]{array}
				\caption{$N$ and $C$ array}\label{chunk}
			\end{subfigure}%
			
			\begin{subfigure}[t]{\textwidth}
				\centering
				\includegraphics[width=6cm]{chunk}
				\caption{$C_6$ is encoded with bitmap and a revised $C_6$ where all the zero entries are eliminated. This reduces the memory consumption of SAIL by up to $80\%$ in a real backbone router}\label{chunk}
			\end{subfigure}%
			
		\end{figure}
		
	\end{frame}

	\begin{frame}{Population counting}
		\begin{itemize}			
			
			\item {As SAIL processes $8$ bits in every step of the way (level $16$, $24$ and $32$), we maintain a $256$-bit bitmap.
		\begin{figure}[!t]
			\centering
			\includegraphics[width=5cm]{chunkstructure.png}
		\end{figure}}

			\item During FIB lookup, we need to find out how many $1$-bit (\textit{population-count}) are there before $i^{th} (0<i<255)$ bit.
			
			\item This will generally require calling POPCNT CPU instruction $4$($\frac{256}{64}$) times because POPCNT can process only $64$ bit at once.
			
			\item To avoid that, we divide the $256$-bitmap into four parts. Each part maintains its own start index. The start index contains the pre-calculated population count prior to that part. This is why, we don't need to calculate the POPCNT for the whole chunk. Instead we need to calculate the POPCNT for a part of the chunk. 			
			
			\item We map $i$ to a part by simply dividing it by $64$. This is why we only require calling POPCNT only once and a DIVISION operation. 

		\end{itemize}
		
	\end{frame}
	
	\begin{frame}{Population counting in Poptrie}
		\begin{itemize}			
						
				\item Population counting was also used in Poptrie.
				 However they use $64$-bit bitmap.
				
				\item This is why, they can apply POPCNT directly.	However they will require visiting more levels ($16$, $22$, $28$, $34$) than SAIL which reduces its lookup performance.	
				
				\item Our implementation of SAIL uses \textit{population-counting} while visiting just three levels ($16$, $24$, $32$).
				
			\end{itemize}
			
		\end{frame}

	
	\begin{frame}{SAIL based FIB Lookup with \textit{population counting}}
		\begin{figure}[!t]
			\centering
			\includegraphics[width=5cm]{algo.png}			
		\end{figure}
	\end{frame}
	
	\section{Implementation}
	
	\begin{frame}{Implementation}
		\begin{itemize}			
			
			\item We have implemented SAIL\_L and SAIL\_U (with \textit{population counting}) in Linux kernel 4.19 (contains around $2500$ lines of C code).
			
			\item Our implementation include FIB lookup, FIB update, FIB delete and FIB flush.
			
			\item We also have implemented test code in linux kernel to evaluate the performance of our algorithms (around $400$ lines of C and assembly code).
			
			\item Finally we have implemented SAIL\_L and SAIL\_U (with \textit{population counting}) using Domino programming language (around $150$ lines). 
			
			\item We have made our implementation publicly available in Github.
			
			
		\end{itemize}
	\end{frame}	
	
	
	\section{Evaluation of SAIL in a Programmable Pipeline}
	
	\begin{frame}{SAIL in a Programmable Pipeline}
		\begin{itemize}			
			
			\item Domino programming language enables us to develop programs for programmable pipeline based VLIW processors.
			
			\item A Domino program successfully compiled by domino-compiler is guaranteed process packets at line rate (processing $1$ billion packets per second on a $1$ GHz VLIW processor).
			
			\item Our Domino implementation is successfully compiled by domino-compiler.
			
			\item This shows that a programmable pipeline based a VLIW processor can run SAIL with \text{population-counting} at line rate.		
			
		\end{itemize}
		

		
	\end{frame}
	
	
	\begin{frame}{SAIL in a Programmable Pipeline}
		\begin{itemize}			
			
			\item A Domino compiler enables us to evaluate a Domino program without needing actual hardware
			
			\item Actual hardware doesn't exist yet (although Verilog implementation exists).
			
			\item Domino compiler generates a dependency graph that shows how the program would be executed on a pipeline (We have made the graph publicly available)
						
		\end{itemize}
		
		\begin {table}[H]
		\caption {Comparison between SAIL\_U and SAIL\_L (with \textit{population-counting})} \label{sailpipeline} 
		\begin{center}
			\begin{tabular}{ | m{7cm} | m{1.5cm} | m{1.5cm} |}
				\hline  & SAIL\_U& SAIL\_L \\
				\hline Number of pipeline stages & $15$  & $32$ \\
				\hline Maximum \# of atoms (ALU) per stage & $5$ & $6$ \\
				\hline Processing latency (for each packet) & $15$ ns& $32$ ns \\
				\hline 
			\end{tabular} 	
		\end{center}
		\end {table}
		
	\end{frame}
	
\section{Evaluation of SAIL in Linux kernel}
	
	\begin{frame}{Dataset}
		\begin{itemize}			
			
			\item We have evaluated our Linux kernel implementation with FIBs from real backbone router (obtained from RouteView project) 
			
			\item RouteView project provide us with RIB in MRT format. We then convert the MRT RIB to FIB using BGPDump and our custom Python script (\textit{both data and the scripts are publicly available}).
			
			\item We conducted our experiment in a Laptop. We have created $32$ virtual ethernet to emulate a router.
			
		\end{itemize}
		
\begin {table}[H]
\begin{center}
	\begin{tabular}{ | m{.9cm} | m{2cm} | m{2.2cm} | m{2.4cm} | m{2.5cm} |}
		\hline Name & AS Number& \# of prefixes & \# of next-hops  & Prefix length\\
		\hline fib1 & $293$  & $759069$ & $2$ & $0-24$\\
		\hline fib2 & $852$ & $733378$ & $138$ & $0-24$ \\
		\hline fib3 & $19016$ & $552285$ & $236$  & $0-32$ \\
		\hline fib4 & $19151$ & $737125$ & $2$  & $0-32$ \\
		\hline fib5 & $23367$  & $131336$ & $178$ & $0-24$  \\
		\hline fib6 & $32709$ & $760195$ & $140$  & $0-32$ \\
		\hline fib7 & $53828$ & $733192$ & $223$  & $0-24$ \\
		\hline 
	\end{tabular} 	
\end{center}
\end {table}
		
	\end{frame}
	
	
	\begin{frame}{Impact of Population Counting}

\begin {table}[H]
\caption {Impact of population counting on memory consumption (for \textit{fib6})} \label{memory} 
\begin{center}
	\begin{tabular}{ | m{.9cm} | m{1.5cm} | m{1.8cm} | m{1.8cm} | m{1.8cm} |}
		\hline & \multicolumn{2}{|c|}{Without Population Counting} & \multicolumn{2}{|c|}{With Population Counting} \\
		\hline Array & Length & Size & Length & Size\\
		\hline $N_{16}$  & $65536$ & $64$ KB & $65536$ & $64$ KB\\
		\hline $C_{16}$ & $65536$ & $128$ KB & $65536$ & $128$ KB\\
		\hline $N_{24}$ & $6071808$ & $5.79$ MB & $6071808$ & $5.79$ MB\\
		\hline $CK_{24}$  &  & & $366$ & $22.87$ KB\\
		\hline $C_{24}$ & $6071808$ & $23.16$ MB & $366$ & $1.42$ KB\\
		\hline $N_{32}$ & $93696$ & $91.50$ KB & $93696$ & $91.50$ KB\\
		\hline Total &  & $29.22$ MB &  & $6.09$ MB\\		
		\hline 
	\end{tabular} 	
\end{center}
\end {table}
		\begin{itemize}			
			
			\item The memory consumption primarily differs for $C_{24}$. 
			
			\item $98.5\%$ routes in backbone routers are $0-24$ bit long. This is why most of the entries in $C_{24}$ remains $0$. Population counting eliminates those entries results significant reduction in memory consumption.
		
			
		\end{itemize}
		
	\end{frame}
	
	
	\begin{frame}{Impact of Population Counting}
		\begin{figure}[!t]
			\centering
			\includegraphics[width=9cm]{memory}
			\caption{Memory consumption for different FIBs}\label{memorycomp}
		\end{figure}
	\end{frame}
	
	\begin{frame}{Lookup Cost}
\begin{figure}[!t]
	\begin{subfigure}[t]{0.4\textwidth}
		\centering
		\includegraphics[width=4.5cm]{candlesticks-sailbm.png}
		\caption{SAIL\_U}\label{sailbm-lookup}
	\end{subfigure}%
	~ 
	\begin{subfigure}[t]{0.4\textwidth}
		\centering
		\includegraphics[width=4.5cm]{candlesticks-saill.png}
		\caption{SAIL\_L}\label{saill-lookup}
	\end{subfigure}%
	\caption{Lookup cost for different levels .}\label{cpuvsplen}
\end{figure}
	\end{frame}
	
	
	\begin{frame}{Lookup Cost (Lesson Learned)}
		\begin{itemize}			
			
			\item The result shows that a general purpose CPU fail to exhibit deterministic performance.
			
			\item It also shows that both SAIL\_U and SAIL\_L (with \textit{population-counting}) exhibit comparable lookup performance.
			
			\item The result also shows that lookup cost increases for higher level. For instance, the lookup cost is maximum when the longest prefix is found in level $32$. Again the lookup cost is minimum when it is found in level $16$.


		\end{itemize}
	\end{frame}
	
	\begin{frame}{Lookup Cost (Lesson Learned)}
		\begin{itemize}					
			
			\item It is noteworthy that we disable \textit{hyper-threading} and \textit{frequency scaling} while conducting the experiemnt. This avoids unnecessary cache thrashing. 
			
			\item Here only considered the data where SAIL is stored in CPU cache (so that DRAM latency doesn't affect the actual performance of the algorithm)
			
			\item It is noteworthy that FIB lookup in Linux kernel will not act as a dataplane in a Linux router (it will work as a slow path). 	
			
		\end{itemize}
	\end{frame}
	
	\begin{frame}{Update cost}
	\begin{figure}[!t]
		\centering
		\includegraphics[width=9cm]{update}
		\caption{Update cost for different prefix lengths}\label{update}
	\end{figure}
	\end{frame}
	
	
	\begin{frame}{Update Cost (Lesson Learned)}
		\begin{itemize}			
			
			\item The result shows SAIL\_U performs slightly better than SAIL\_U for FIB update (when \textit{population-counting} is used).
			
			\item It also shows that our implementation can perform fast incremental update which is needed for the control plane of a Linux router.
			
		\end{itemize}
	\end{frame}

	
\begin{frame}{}
	\centering \Large
	\Huge{Thank You}
\end{frame}
	
\end{document}


