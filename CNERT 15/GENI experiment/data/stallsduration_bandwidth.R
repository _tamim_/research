library(extrafont)
#font_import()
#loadfonts()

mydata = read.csv("stallduration_bandwidth.csv") 
attach(mydata)
pdf("stallduration_bandwidth.pdf", width=6, height=4 , family="Arial Rounded MT Bold")
plot(Bandwidth, Stalls.GOPBased,  type='o', col="red", xlab="Available Bandwidth (kB/s)",ylab="Stall Duration (sec)", ylim=c(0,35), pch=22) 
lines(Bandwidth, Stalls.Two,  type='o', pch=23, lty=3, col="blue")
lines(Bandwidth, Stalls.Four,  type='o', pch=24, lty=4, col="green")
lines(Bandwidth, Stalls.Eight,  type='o', pch=25, lty=5, col="black")
legend(1200, 33, c("GOP based splicing","2 sec splicing","4 sec splicing","8 sec splicing"), cex=0.8, col=c("red", "blue", "green", "black"), pch=22:25, lty=1:4)
dev.off()

#Embed font. 
Sys.setenv(R_GSCMD = "C:/Program Files/gs/gs9.16/bin/gswin64c.exe")
embed_fonts("stallduration_bandwidth.pdf", outfile="stallduration_bandwidth_embed.pdf")