#Other wise everything will be considered relative to RSpec's directory
cd /users/mislam4/

#Without this, python is not installed properly on Ubuntu 14.04
sudo apt-get -y update
sudo apt-get -y upgrade

sudo apt-get install -y software-properties-common python-software-properties
sudo add-apt-repository -y ppa:webupd8team/java
sudo apt-get update -y

# Enable silent install
echo debconf shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections
echo debconf shared/accepted-oracle-license-v1-1 seen true | sudo debconf-set-selections
 
sudo apt-get install -y oracle-java7-installer
 
# Not always necessary, but just in case...
sudo update-java-alternatives -s java-7-oracle
 
# Setting Java environment variables
sudo apt-get install -y oracle-java7-set-default

#sudo apt-get update -f
#sudo apt-get install -y openjdk*

sudo wget --no-check-certificate https://web.cs.kent.edu/~mislam4/StrToLong.tar.gz
sudo tar -xf StrToLong.tar.gz
sudo chmod a+rwx StrToLong.jar

sudo cp -avr /usr/local/lib ./
sudo cp -avr /usr/local/app.config ./
sudo cp -avr /usr/local/videos ./
sudo chmod a+rwx -R videos
sudo chmod a+rwx -R lib

#Install Unity on Ubuntu
#sudo apt-get -y install ubuntu-desktop

#install X11
#sudo apt-get -y install xorg openbox
#sudo sh -c "echo 'ForwardAgent yes' >> /etc/ssh/ssh_config"
#sudo sh -c "echo 'ForwardX11 yes' >> /etc/ssh/ssh_config"
#sudo sh -c "echo 'ForwardX11Trusted yes' >> /etc/ssh/ssh_config"

#sudo sh -c "echo 'X11Forwarding yes' >> /etc/ssh/sshd_config"

#Install VNC server
#sudo apt-get -y install vnc4server

#install VLC. PLay video by "vlc output.mp4"
sudo apt-get -y install vlc browser-plugin-vlc








