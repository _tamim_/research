1. However this paper lack comparison with state of the art solutions.
2. The effect of the topology on the performances should be questioned.
3. Authors compare this techniques with GOP based segmentation but nothing about the GOP composition/duration has been said in the paper, which is a determinant issue, since the duration of the GOP is important. If the GOP is small, it probably would perform equal or better as no processing should be made to the video (no I-frames would be inserted).
4. Authors should compare rigorously the splicing techniques (with different durations) with several configurations of GOPS sizes in GOP based segmentation, an then extract and present more general conclusions.
5. Such techniques have been experimented on GENI research network, showing that Frame-inaccurate splicing also performs better than frame-accurate splicing in small bandwidth environment.
6. English style should be revised thoroughly. There are several bad sentences and constructions. Too many to point each one.
7. This paper could be called "A Survey on Video Splicing Techniques for P2P Video Streaming", because it describes a few video segmentation methods. 
8. Besides, authors assure that they developped a P2P video streaming application in Java, while they could use existing P2P video streaming applications.
9. The statement �GOP is a unit of I-frame, P-frame and B-frame� stipulates that a GOP is composed from 3 frames. Please clarify.
10. The affirmation �the size of each GOP can be very big� needs to be supported by at least one example and maybe explained if the size of GOP is correlated with the video content.
11. In �Comparison of segmentation strategies� the authors assume that the users are viewing the video sequentially. Thus frame accurate segmentation is inappropriate since is clearly that bigger size segments will cause higher stall.
12. In figures 2 and 4 the bandwidth inferior limit seems close to 0 kbps so the minimum value should be defined.
13. The equations, even if is only one, should be numbered.
14. In �Effect of downloading policy� the pool size effect is evaluated. It is not specified what type of segmentation is used. Is the segmentation type irrelevant?
15. The number of segments defined in equation 1 depends of the time T. What is the value of T in the experimental results?
16. The experimental platform used 100 nodes. Where is this relevant?
17. The results in figures 1-4 present overall values for the entire network or just the one P2P connection? Please clarify. 
From my understanding the splicing techniques are not new. The contributions are the downloading policy and the simulation on GENI. The adaptive download policy proposed is presented simplistic and without consistency. I recommend detailing it (if possible).
The experimental scenario is missing. How many videos have been streamed, what sizes, how many P2P connections.
18. number all equations; refer to them in text using their numbers

--------- Comment 1:
Nicely written paper comparing different strategies to load videos.
However this paper lack comparison with state of the art solutions. Such a comparison would increase the quality of the article.
Plus, the effect of the topology on the performances should be questioned, as it may not be always the case that we have such paterns in real life scenarios.



--------- Comment 2:
== Comment from the Conference Chairs ==
- After successfully registering and uploading the camera-ready on the publisher site, please notify melanie@iaria.org for a cross-check.
- It is acceptable to take additional space as needed (granted, no extra page fees) to comply with reviewer comments.
======================================



--------- Comment 3:
In this paper authors analyze the performance of different video splicing techniques for P2P video streaming. Also, a downloading technique has been proposed to reduce stalls in of P2P video streaming.
Several splicing techniques are defined: frame-accurate, frame-inaccurate splicing and GOP based segmentation. In frame-accurate splicing, an I-frame is inserted manually if the segment doesn�t have one; whereas in frame-inaccurate splicing, no I-frame is inserted, so the segments can be started with a P or a B frame.
Such techniques have been experimented on GENI research network, showing that Frame-inaccurate splicing also performs better than frame-accurate splicing in small bandwidth environment. Authors compare this techniques with GOP based segmentation but nothing about the GOP composition/duration has been said in the paper, which is a determinant issue, since the duration of the GOP is important. If the GOP is small, it probably would perform equal or better as no processing should be made to the video (no I-frames would be inserted).
Authors should compare rigorously the splicing techniques (with different durations) with several configurations of GOPS sizes in GOP based segmentation, an then extract and present more general conclusions.

Few comments:
- English style should be revised thoroughly. There are several bad sentences and constructions. Too many to point each one.
- Revise ref. 10.



--------- Comment 4:
This paper proposes an analyse of the video splicing strategies suitable to P2P video streaming. Indeed, authors study different video splicing techniques in a P2P video streaming application; their experiment is conduted on MPEG-4 video. In addition, as main contribution, authors have proposed a downloading policy where a leecher (peer) maintains a download pool based on bandwidth, segment size and buffered video.  
However, the proposed contribution is very limited.  Hence, this paper could be called "A Survey on Video Splicing Techniques for P2P Video Streaming", because it describes a few video segmentation methods. 
The paper does not present an experimental comparison between presented video splicing schemes. Besides, authors assure that they developped a P2P video streaming application in Java, while they could use existing P2P video streaming applications.



--------- Comment 5:
The paper is interesting and properly structured. I have a few remarks for the theoretical elements and the results presentation.
1. The statement �GOP is a unit of I-frame, P-frame and B-frame� stipulates that a GOP is composed from 3 frames. Please clarify.
2. The affirmation �the size of each GOP can be very big� needs to be supported by at least one example and maybe explained if the size of GOP is correlated with the video content. 3. In �Comparison of segmentation strategies� the authors assume that the users are viewing the video sequentially. Thus frame accurate segmentation is inappropriate since is clearly that bigger size segments will cause higher stall.
4. In figures 2 and 4 the bandwidth inferior limit seems close to 0 kbps so the minimum value should be defined.
5. The equations, even if is only one, should be numbered.
6. In �Effect of downloading policy� the pool size effect is evaluated. It is not specified what type of segmentation is used. Is the segmentation type irrelevant?
7. The number of segments defined in equation 1 depends of the time T. What is the value of T in the experimental results?
8. The experimental platform used 100 nodes. Where is this relevant?
9. The results in figures 1-4 present overall values for the entire network or just the one P2P connection? Please clarify. 
From my understanding the splicing techniques are not new. The contributions are the downloading policy and the simulation on GENI. The adaptive download policy proposed is presented simplistic and without consistency. I recommend detailing it (if possible).
The experimental scenario is missing. How many videos have been streamed, what sizes, how many P2P connections.



--------- Comment 6:
The paper formatting needs some adjustments: 
!! in the affiliation section, eliminate the postal address;

!! Do not insert citations in the abstract

!! end the Introduction with a compact paragraph describing the structure of the paper, section by section; use explicit notations, such as 'In Section 2, ...'

!! Reduce the spacing between text paragraphs, all across the paper

!! number all equations; refer to them in text using their numbers

!! Reduce the spacing between figures and their captions -> correct: Figure 3, Figure 5, etc.

!! Reduce the spacing between Figures 2 and 3, and Figures 4 and 5

!! Center all figures

!! in the text, refer to all figures by "Figure 1", and not by "figure 1"; correct all figure notations in text, all across the paper





!! correct the format for all the References (see template):

- provide the last access date for ALL on-line references in the form [retrieved: month, year] (close to the camera-ready deadline) -> correct: [3], [4], [5], etc.

- some have abbreviated authors' names, some not; follow the required format-> correct: [11], [19], [22], etc.

- The references must use the correct format (abbreviated-firstname lastname):
A. B. Name1, C. D. Name2, and E. F. Name3 ...
- use a space between the abbreviated firstnames "C.C." -> "C. C."
!! Correct all

- add only 'and' when exactly two authors-> correct: [11], [19], [21], etc.

- Do not write names/titles in UPPERCASE -> correct: [27]




!! Correct the typos / grammar. Some examples:

!! expand all the acronyms, even trivial, when firstly used;  examples: P2P, MPEG, etc.

!! use the correct definition:
-- is a sequence of GOP (Group
of Pictures), where
Must be:
--> is a sequence of Group
of Pictures (GOP), where
!! Apply this definition rule for all acronyms.

!! do not expand the acronyms after their first definition (even in the abstract); example:
-- Group of Picture (GOP) is a unit of I-frame
--> GOP is a unit of I-frame

-- Here we are
only dealing with
--> Here, we are
only dealing with

-- Thus it creates long
--> Thus, it creates long
!! always this way, in similar constructions, across the whole paper

-- The advantage of this strategy is we
can create
--> The advantage of this strategy is that we
can create

-- Moreover we will
see that we
--> Moreover, we will
see that we

-- So in other word, the size
--> So in other words, the size

-- on ProtoGENI [10], [11] at
--> on ProtoGENI [10][11] at
!! Correct all citations across the paper

-- Here we measured the
--> Here, we measured the

-- based architecture [13],
[14], [15].
--> based architecture [13][14][15].

-- GOP (Group of Picture) based
segmentation
--> GOP based
segmentation

-- for splicing video[28],
[29].
--> for splicing video [28][29].
!! Check and correct the punctuation and word spacing, all across the paper

-- Furthermore a downloading technique
--> Furthermore, a downloading technique




!! etc., etc.



--------- Comment 7:
1. Input values for experiments could be explained in more details so that the reader does not get confused.
2. More network sizes could be analyzed to see if the performance of evaluated methods changes for smaller or bigger networks.
3. References section should be sorted.
4. When describing different mechanisms, Authors could mention real systems using them.