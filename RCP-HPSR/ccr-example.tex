\documentclass[conference]{IEEEtran}
%\IEEEoverridecommandlockouts
% The preceding line is only needed to identify funding in the first footnote. If that is unneeded, please comment it out.
\usepackage{graphicx,multirow,booktabs,caption, array, listings}
\usepackage{times}  
\usepackage{url}
\usepackage{listings}
\usepackage{textcomp}
\usepackage{gensymb}
\usepackage{float}
\usepackage{caption} 
\usepackage{subcaption}
\usepackage{array}
\usepackage{listings}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{amsmath}
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}
\begin{document}

\title{Leveraging Domino to Implement RCP in a Stateful Programmable Pipeline}

\author{\IEEEauthorblockN{MD Iftakharul Islam, Javed I Khan}
	\IEEEauthorblockA{\textit{Kent State University}\\
		Kent, Ohio, USA \\
		mislam4@kent.edu, javed@cs.kent.edu}
}

\maketitle

\begin{abstract}
RCP is a router-assisted congestion control mechanism where routers allocate a feedback rate to each flow. Here sender nodes regulate their sending rates based on the feedback rates received from the routers. An RCP router calculates the feedback rate based on the queue occupancy, spare capacity and the average RTT of the on-going flows. This is why, an RCP router needs to execute a stateful packet processing program in the dataplane. However stateful packet processing in a very high-speed (e.g. Tb/s) router is particularly challenging. Currently there is no real deployment of RCP, to the best of our knowledge. This paper presents an implementation of RCP protocol in a programmable-pipeline based router. We use Domino programming language to implement the dataplane. Domino is a domain specific language that allows us to develop a packet processing program as well as the hardware pipeline on which  the packet processing program would be executed. Domino programs are guaranteed to run at line rate. This is the first implementation of RCP using Domino, to the best of our knowledge.
\end{abstract}

\begin{IEEEkeywords}
RCP; Domino; Stateful Pipeline Design
\end{IEEEkeywords}

\section{Introduction}\label{sec:intro}
Router-assisted congestion controls such as RCP \cite{dukkipati2005processor}, XCP \cite{katabi2002internet} and NC-TCP \cite{tamim} show very promising results compared to their host-centric counterparts. These protocols require all the routers to compute a feedback throughput in their data-planes. Senders here use the feedback rate to regulate the sending rate. RCP, XCP and NC-TCP have been found to result in much lower queuing delay, higher throughput, lower flow-completion time, fairer bandwidth allocation and so on \cite{dukkipati2005processor, katabi2002internet, tamim}. These protocols however are very hard to implement in a router dataplane. This is why, although they perform much better than the host-centric congestion controls, they are rarely used in practice. Currently there is no real deployment of these protocols, to the best of our knowledge. This paper presents an implementation of RCP protocol in a programmable pipeline based router.

Programmable-pipeline \cite{bosshart2013forwarding} based routers have emerged as a promising alternative to the fixed-function ASIC based routers. Examples of these routers include Barefoot Tofino \cite{tofino}, Cavium Xplaint \cite{xplaint}, Brodcom Jericho2 \cite{Jericho2} and so on. They allow us to program the router dataplane while achieving very high throughput. For instance, Barefoot Tofino $2$ programmable routers can achieve upto $12.8$ Tb/s aggregate throughput. These routers however are not suitable for stateful packet processing \cite{sivaraman2016packet}. They use reconfigurable match-action tables (RMT) to implement the pipeline. Here the pipeline stages are not suitable for manipulating states \cite{sivaraman2016packet}. Although RMT routers contain counters, registers, meters and other stateful memory to maintain states, the pipeline stages cannot update them at line rate. Here the states are not local to a pipeline stage. The states are rather shared by all the pipeline stages.This is why, accessing and updating them from different pipeline stages incur synchronization overhead.

\begin{figure}[!t]
	\centering
	\includegraphics[width=9cm]{abstract-router-model}
	\caption{Stateful pipeline based Banzai router}\label{router-hardware}
\end{figure}

To address the shortcoming, Sivaraman et al. has developed a stateful programmable pipeline named Banzai \cite{sivaraman2016packet}. A Banzai router can process $1$ packet in each clock cycle\footnote{Actually it takes more than $1$ cycle to processing a packet. But it uses pipelining to hide the latency. The pipeline outputs one packet in each clock cycle.}. Fig. \ref{router-hardware} shows a block diagram of a Banzai router. Here packet processing is divided into parser, ingress pipeline and egress pipeline. Both ingress and egress pipeline consists of multiple pipeline stages where each stage contains multiple stateful and stateless atoms. The atoms are circuitry developed based on ALU, MUX and so on. An atom can process a packet in $1$ clock cycle. Atoms in the same stage can operate on packet headers in parallel. Such hardware can be realized using a VLIW\footnote{https://en.wikipedia.org/wiki/Very\_long\_instruction\_word} processor. Here each pipeline stage processes one packet in each clock cycle. The pipeline also does not stall. This is why both ingress pipeline and egress pipeline can process $1$ packet in every clock cycle. Such performance guarantee is very crucial for a router dataplane. For instance, a $64$-port router with a line rate of $10Gb/s$ per port and a minimum packet size of $64$ bytes needs to process $1$ billion packets per second \cite{bosshart2013forwarding}. If each pipeline runs at $1$ GHz, and each pipeline can process $1$ packet in every clock cycle, then the router can process $1$ billion packets per second.

This paper presents an implementation of RCP using Banzai router. We use Domino programming language \cite{sivaraman2016packet} to program the dataplane. Domino is a C like programming language. It allows us to design a packet processing program and ingress/egress pipelines. The packet processing programs here are guaranteed to run on the pipeline at line rate. Our RCP implementation shows that a $15$-stage egress pipeline with $2$ atoms per stage can execute RCP at line rate. 

The remaining of the paper proceeds as follows.  Section II presents the related works. In Section III, we describe how congestion occurs in a router. The Section IV describes RCP protocol. Section V discusses the implementation of RCP in Domino. Section VI shows how the RCP program is executed in the hardware pipeline. Finally Section VII concludes the paper.



\begin{figure}[!t]
	\centering
	\includegraphics[width=8cm]{router-linecard}
	\caption{Packet processing in a router. VLIW processor implements the parser and the ingress and the egress pipeline. Switching fabric works as the queue between the ingress and the egress pipeline.}\label{router-linecard}
\end{figure}


\begin{figure}[!t]
	\centering
	\includegraphics[width=8cm]{router-congestion}
	\caption{Congestion in a router data-plane. RCP program would be executed in the egress pipeline.}\label{router-congestion}
\end{figure}


\section{Related Works}
Dukkipati et al. first implemented RCP in a NetFPGA \cite{dukkipati2007building}. It however could achieve only $700$ packets/millisecond throughput. Hauger et al. has implemented XCP on an Intel IXP network processor \cite{hauger2008quick}. They could achieve $9$ Mpps (million packets per second). This is however significantly smaller than the $1$ billion packets per second needed for a modern router. Network processors generally use SRAM or scratchpad memory to store the program states \cite{comer2004network}. The latency of these memories have still remained constant at $12-20$ clock cycle. This is why network processors are not suitable for implementing RCP. 

Recently Sharma et al. has implemented XCP and RCP for RMT routers (e.g. Cavium Xplaint, Barefoot Tofino) \cite{sharma17evaluating}. They however found many limitations in a RMT router. The ALUs in those switches do not support division and multiplication which are essential for implementing RCP. They have used bit shifting operations to implement approximate versions of division and multiplication. Another limitation of an RMT router is that it is not suitable for stateful packet processing. For instance, RMT routers maintain stateful memories such as counters, registers and meters in a centralized manner. Accessing those centralized memories from different pipelines requires explicit synchronization which is not scalable and not suitable for a high speed router. Domino solves this problem by making each state local to a pipeline \cite{sivaraman2016packet}. This is why, the pipeline stages in Domino can update a state without a synchronization overhead. Finally, Domino enables us to design a custom atom in a pipeline. This is why, a Domino pipeline can implement division and multiplication in hardware. Domino has been used to implement various network functions including load balancing \cite{sivaraman2016packet}, routing table lookup \cite{tamimsail}, network monitoring \cite{narayana2017language} and so on. This paper uses Domino to implement router-assisted congestion control, RCP in the dataplane.

Sivaraman et al. has stated that they also have implemented RCP using Domino \cite{sivaraman2016packet}. However their version of RCP lacked many essential elements of RCP. For instance, RCP requires the router to calculate the spare capacity and the running average of RTTs. Their implementation does not calculate these\footnote{https://github.com/packet-transactions/domino-examples/blob/master/domino\_programs/rcp.c}. Their implementation also neither calculate a feedback rate nor write the feedback rate in the packet header. This paper presents a complete implementation of RCP using Domino.   

\section{Congestion in a Router}
This section shows how a packet is processed in a router. It then discusses how congestion occurs in a router. Finally, it shows why RCP is needed to be implemented in the egress pipeline.

A router consists of a fabric card and one or multiple line cards as in Fig. \ref{router-linecard}. Here an incoming packet to a port in one line card can be egressed through a port in another line card. Each line card contains a VLIW processor that contains the parser and the ingress and the egress pipeline. The VLIW processor acts as the dataplane. The switching fabric is used for forwarding packets across multiple ports. It acts as the queue between the ingress and the egress pipeline. When a packet is received by a port, it is processed by a parser followed by the ingress pipeline of the VLIW processor. The ingress pipeline performs longest-prefix matching to find the destination port. The ingress pipeline however cannot forward the packet directly to the destination port. It rather forwards the packet to the switching fabric. The switching fabric forwards the packet to the egress pipeline. The egress pipeline here performs additional operation on the packet header before forwarding it to the destination port.     

A router dataplane becomes congested when one or multiple line cards try to forward packets to another line card at a higher rate than its capacity. For instance, in Fig. \ref{router-congestion}, line card $1$ and $3$ are causing congestion to line card $2$. Here the capacity of each line card is $640$ Gb/s. Here both line card $1$ and $3$ are receiving packets at $640$ Gb/s. The ingress pipelines of the line cards forward the packets at line-rate. This is why, line card $2$ will receive packets from the switching fabric at $1280$ Gb/s. However the egress pipeline of line card $2$ can transmit packets at $640$ Gb/s. This is why line card $2$ will become congested. It is noteworthy that congestion occurs due to the inability of the egress pipeline to forward packets at the incoming rate. This is why RCP protocol is implemented in the egress pipeline.

\section{RCP Protocol}
RCP protocol calculates the feedback rate as following:
\begin{equation}\label{rcp1}R(t) = R(t-RTT_{a}) + \frac{\alpha.S - \beta.\frac{Q(t)}{RTT_{a}}}{\hat{N(t)}}\end{equation}

where $RTT_a$ is the moving average of the RTTs of all the incoming packets, $S$ is the spare link capacity and $Q(t)$ is the queue size. $\alpha$ and $\beta$ are stability constants. We have chosen $\alpha=1.0$ and $\beta=0.5$ in this paper \cite{dukkipati2005processor}. $\hat{N(t)}$ is the estimated number of on-going flows. Note that here the congestion controller maximizes the link utilization while minimizing the queuing delay. If there a spare capacity ($S > 0$), RCP increases the feedback rate. Again if there a persistent queue ($Q > 0$), RCP decreases the feedback rate. Note that here we divide the feedback rate equally among the on-going flows. Such flow sharing is only applicable when RCP flows compete among themselves. Such flow sharing was also used by NC-TCP \cite{tamim} for video delivery networks where video flows only compete among themselves.

An RCP router however does not use the equation \ref{rcp1}. Calculating the number of on-going flows in router dataplane is very expensive \cite{estan2003bitmap, fan2014cuckoo}. RCP approximates the number of on-going flows as $\hat{N(t)} = \frac{C}{R(t-RTT_{a})}$ where $C$ is the link capacity. It has shown that such approximation is very accurate for long-lived flows \cite{dukkipati2005processor}. This is why, the equation \ref{rcp1} can be written as 

\begin{equation}\label{rcp2}
\begin{split}
R(t) & = R(t-RTT_{a}) + \frac{\alpha.S - \beta.\frac{Q(t)}{RTT_{a}}}{\frac{C}{R(t-RTT_{a})}} \\
& = R(t-RTT_{a}) [1 + \frac{\alpha.S - \beta.\frac{Q(t)}{RTT_{a}}}{C}]
\end{split}
\end{equation}

Here $RTT_a$ is used as the control interval. However we want the control interval to be smaller than the $RTT_a$ so that the router can react to the spare capacity and queuing delay sooner. RCP, in that case, scale the aggregate change by $\frac{T}{RTT_a}$ where $T$ ($T < RTT_a$) is the desired control interval \cite{dukkipati2005processor}. The equation \ref{rcp2} can be rewritten as:

\begin{equation}\label{rcp3}
R(t) = R(t-T) [1 + \frac{\frac{T}{RTT_a}.(\alpha.S - \beta.\frac{Q(t)}{RTT_{a}})}{C}]
\end{equation}

This paper uses equation \ref{rcp3} to calculate the feedback throughput. Here $RTT_a$ is the running average which is calculated as $RTT_a = .98 * RTT_a + .02 * RTT_{packet}$ for each incoming packet \cite{dukkipati2005processor}. We also use $\alpha = 1$, $\beta = .5$ and $T=50$ ms in equation \ref{rcp3}. We calculate the spare capacity $S = C - \frac{B}{1000T}$ MB where $B$ is the number of bytes received during last $T$ milliseconds.


\section{Implementation}
Listing $1$ shows an implementation of RCP protocol in Domino\footnote{http://web.mit.edu/domino/} programming language. Our website\footnote{\url{https://web.cs.kent.edu/~mislam4/rcp-using-domino.html}} also contains the instructions to compile the program.

\begin{lstlisting}[language=C, caption=rcp.c]
//Capacity of the line card in megabytes
#define C 64000 
#define T 50 //Control Interval in ms
#define A 50000 //1000*T

/*********State Variables****************/
// Running average of RTT in ms
int RTT = 200; 
int R = 200; //RCP feedback rate in MB/s
int B = 0; //Number of Bytes received
int S = 0; //Spare capacity in MB/s

/****Packet headers and meta-data********/
struct Packet {
int size_bytes;
int rtt; //Parsed from RCP header
int queue;
int Rp; //RCP feedback rate
int tick;
};

/*********Packet Transaction****************/
void func(struct Packet pkt) {
//Calculate running average of RTT
RTT = (RTT * 49 + pkt.rtt)/50;

//Write the throughput in packet header
if (pkt.Rp > R)
pkt.Rp = R;  

//Control interval has expired, so
// calculate the feeback throughput
// and reset the state variables
if (pkt.tick % T == 0) {
S = C - B/A;
B = 0;
R *= 1+((S-((pkt.queue/RTT)/2))*T/RTT)/C;
}
else {
B += pkt.size_bytes; 
}
}
\end{lstlisting}

Domino uses a notion called \textit{packet transaction} where programmers write a packet processing program such that the program would be applied on each packet at once. Here programmers do not need to worry about how the packet headers and states are manipulated. Programmers do not need to hold a lock before accessing or updating a state. Domnio-compiler transforms a Domino program into multiple pipeline stages. Each papipeline stage executes an atomic operation. The atomic operations are executed by the atoms in a pipeline stage. This is why, the atoms in Domino are designed such that they can execute an atomic operation in one clock cycle, as well as, the atoms can run at $1$ GHz \cite{sivaraman2016packet}. These pipelining mechanism of Domino allow programmers to manipulate the states without holding a lock. Again, as each pipeline stage executes operations in an atomic fashion, any change made to a state by one packet would be visible to the following packet. As a result, each pipeline stage will always hold the correct value of a state variable. 

Note that RCP does not calculate the feedback rate for each packet. It calculates the feedback rate in every $T$ ms (Listing $1$). It however inspects the packet header of each incoming packet. Each packet in RCP carries a feedback rate field $R_p$. If the feedback rate $R(t)$ of the router is smaller than $R_p$, it updates the packet header with $R(t)$ (Listing 1). 

It is noteworthy that Domino is not responsible for programming the packet parser. Domino simply assumes that a packet has already been parsed. The packet headers and meta-data are made available to the Domino transaction via \textit{Packet} structure (Listing $1$). Actual packet parsing has been adopted in P4 language \cite{bosshart2014p4}. It is also noteworthy that the time (\textit{tick} in this case) and the queue length are made available to the Banzai pipeline as a packet meta-data (Listing 1). Exposing queue length as a packet meta-data has been adopted by switch manufacturer \cite{kim2015band}. Such scheme is also used in many other Domino programs \cite{sivaraman2016packet}. Sharma et. el. also have used similar scheme while developing RCP for RMT routers \cite{sharma17evaluating}.

\begin{figure}[!t]
	\centering
	\includegraphics[width=8cm]{atom.png}
	\caption{Atom used for compiling RCP. Here MUX is a multiplexer. RELOP is a relational operator $(>, <, ==, !=)$. $x$ is a state variable. ALU is the arithmetic logic unit. $pkt.f1$
		and $pkt.f2$ are packet fields. $Const$ is a constant operand.}\label{atom}
\end{figure}

\begin{figure*}[!t]
	\centering
	\includegraphics[width=15cm]{pipeline.PNG}
	\caption{Execution of the RCP program in a $15$-stage pipeline}\label{rcp-pipeline}
\end{figure*}



Domino-compiler\footnote{https://github.com/packet-transactions/domino-compiler} compiles a Domino program for a Banzai router. Here the domino-compiler needs to be provided with a Banzai router configuration. These configurations include how many pipeline stages it has, how many atoms it has in each stage and the type of the stateful atom. For instance, here we use a Banzai router with $15$-stage pipeline where each pipeline stage contains $2$ atoms. The atom we use is shown in Fig. \ref{atom}. Domino currently has $9$ atoms. Here we use \textit{sub} atom \cite{sivaraman2016packet}. It has shown that the atom can operate at $1$ GHz \cite{sivaraman2016packet}. Domino internally uses Sketch language \cite{solar2006combinatorial} to bridge the gap between a Domino program and the atom. The Sketch representation of the atom would be found in \textit{domino-example}\footnote{https://github.com/packet-transactions/domino-examples/blob/master/banzai\_atoms/sub.sk}.

It is noteworthy that a Domino program only compiles if the program can be implemented in the dataplane at line-rate. Domino follows \textit{all-or-nothing} approach. Domino-compiler compiles a program only if it is guaranteed to run at line-rate. If a packet processing program cannot run at line-rate for a given Banzai router, the compilation fails. Domino-compiler compiles our RCP implementation successfully. This is why, it is guaranteed to run at line-rate.


\section{Execution of RCP in The Pipeline}
Domino-compiler generates a pipeline from a Domino program. The pipeline can execute the Domino program at line rate. Fig. \ref{rcp-pipeline} shows the pipeline generated from the RCP program. The pipeline has $15$ stages where the maximum number of atoms-per-stage is $2$. Each rectangle in the figure represents an atom. Here the grew boxes represent a stateful atoms and the white boxes represent stateless atoms. Each stage in the pipeline processes one packet in each clock cycle. This is why the pipeline can output one packet in each clock cycle. On the other hand, the pipeline takes $15$ cycles to process each packet. This is why, RCP program introduces $15$ ns latency to each packet\footnote{each pipeline stage takes $1$ ns if it runs at $1$ GHz}. The Fig. \ref{rcp-pipeline} shows how the RCP program in Listing $1$ is executed in a $15$-stage pipeline. It is noteworthy that a Domino program is statically mapped to a pipeline. This is why there is no runtime overhead. On the contrary, in a general purpose CPU, programs are scheduled on pipeline dynamically. This is why, there is a run-time overhead in a general purpose CPU. 

Fig. \ref{rcp-pipeline} can also viewed as a dependency graph. Here the arrow represents the dependency between atoms in different states. The instructions in a Domino program are reordered based on the dependency. For instance, although our RCP program assigned the feedback rate towards the beginning of the program, Domino-compiler reordered it and moved it to the very last pipeline stage (Fig. \ref{rcp-pipeline}) based on the dependency graph.

\section{Conclusion}
Although RCP is widely regarded as a superior alternative compared to the host-centric TCPs, there is currently no real deployment of RCP, to the best of our knowledge. Modern router provides multiple Tb/s aggregate throughput. Implementing RCP at such a high throughput is challenging. This paper presents an implementation of RCP using Domino. Our RCP implementation can be executed by a $15$-stage pipeline of a VLIW processor. Each pipeline stage here can operate at $1$ GHz. Again each pipeline stage can process $1$ packet in each clock cycle. This is why our implementation can process $1$ billion packets per second on a $1$ GHz VLIW processor. Again our pipeline requires $15$ ns ($15$ cycles) to implement RCP on the packet. This is the first implementation of RCP based Domino language. Here we have utilized Domino to produce the hardware pipeline needed to run the program at line-rate. 

\bibliographystyle{abbrv}
\bibliography{hotnets17}

\end{document}
