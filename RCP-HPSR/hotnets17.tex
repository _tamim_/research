\documentclass{hotnets17}

\usepackage{times}  
\usepackage{hyperref}
\usepackage{listings}
\usepackage{textcomp}
\usepackage{gensymb}
\usepackage{float}
\usepackage{caption} 
\usepackage{subcaption}
\usepackage{array}
\usepackage{listings}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{amsmath}
%\usepackage[a4paper]{geometry}
%\usepackage{draftwatermark}
%\hypersetup{pdfstartview=FitH,pdfpagelayout=SinglePage}

\setlength\paperheight {11in}
\setlength\paperwidth {8.5in}
\setlength{\textwidth}{7in}
\setlength{\textheight}{9.25in}
\setlength{\oddsidemargin}{-.25in}
\setlength{\evensidemargin}{-.25in}

\begin{document}

% \conferenceinfo{HotNets 2017} {}
% \CopyrightYear{2017}
% \crdata{X}
% \date{}

%%%%%%%%%%%% THIS IS WHERE WE PUT IN THE TITLE AND AUTHORS %%%%%%%%%%%%
\def \name {Multilevel-map}
\def \namespace {multilevel-map }
\def \namelower {multilevel-map}
\def \namespectwo {two-level map}
\def \namespecthree {three-level map}
\def \nameupper {MULTILEVEL-MAP}
\def \expl {}
\title{Routing Table Lookup in Linux XDP for Very Large Routing Tables }

\author{\alignauthor
	MD Iftakharul Islam, Javed I Khan\\
	\affaddr{Kent State University}\\
	\email{mislam4@kent.edu, javed@cs.kent.edu}}

\maketitle

%%%%%%%%%%%%%  ABSTRACT GOES HERE %%%%%%%%%%%%%%
\begin{abstract}
Linux kernel has a fast data-path called XDP \cite{hoiland2018express} to implement packet forwarding. Here the routing-table lookup is implemented in software. However the lookup does not perform well when the routing table is very large in size. For instance, the number of routes in backbone routers already have exceeded $700$K and growing rapidly. Routing table lookup in XDP performs very poorly in such cases. Recently a software routing table lookup mechanism SAIL\_L \cite{yang2014guarantee} has been proposed that can perform a fast lookup in such a large routing table. The impressive performance of SAIL\_L results from utilizing the CPU cache. However the memory consumption of SAIL\_L is often too large to fit in a traditional CPU cache. This paper presents a variant of SAIL\_L called SAIL-BM (SAIL Bitmap) that reduces the memory consumption significantly. SAIL-BM can store the whole routing table in CPU cache. We have implemented SAIL-BM in Linux XDP. Our experimental results with routing table from real backbone router show that SAIL-BM is a clear improvement over SAIL\_L.
   
\end{abstract}

\section{Introduction}
Linux kernel has a fast data-path called eXpress Data Path (XDP) \cite{hoiland2018express} to implement packet forwarding. This allows Linux to work as a software router. One key functionality of a router is to extract the destination IP address of an incoming packet and find the destination NIC based on the longest prefix match (LPM) algorithm. For instance, Table \ref{lpm} shows an example routing table of a router. If the destination IP address of an incoming packet is $169.254.198.1$, then according to the longest prefix match algorithm, the packet should be forwarded to $eth4$. Currently Linux uses LC-trie \cite{nilsson1999ip} for routing table lookup. However LC-trie does not perform well when the routing table is very large in size. It has shown that the number of routes in a backbone router already have exceeded 700K and growing \cite{bgpreport}. LC-trie performs very poorly with the routes from real backbone routers \cite{asai2015poptrie, yang2014guarantee}. 

\begin {table}[H]
\caption {Routing table} \label{lpm} 
\begin{center}
	\begin{tabular}{ | m{3.6cm} | m{3.6cm} |} 
		\hline Prefix & Destination NIC\\
		\hline $10.18.0.0/22$ & $eth1$\\
		\hline $131.123.252.42/32$ & $eth2$\\
		\hline $169.254.0.0/16$ & $eth3$\\
		\hline $169.254.192.0/18$ & $eth4$\\		
		\hline $192.168.122.0/24$ & $eth5$\\						
		\hline 
	\end{tabular} 	
\end{center}
\end {table}

Recently several data structures have been proposed that exhibit impressive lookup performance. These include SAIL\_L \cite{yang2014guarantee}, Poptrie \cite{asai2015poptrie} and DXR \cite{zec2012dxr}. Here SAIL\_L is particularly interesting to us because it outperforms Poptrie and DXR in most of the cases \cite{asai2015poptrie}. SAIL\_L splits a routing table into three parts: level $16$, level $24$ and level $32$. It then stores the level $16$ and a part of level $24$ in the CPU cache and the rest in the main memory. It does so because the memory consumption of the whole routing table often exceeds a traditional CPU cache. For instance, our example routing table with $734K$ routes takes $24.08$ MB. As a CPU cache is limited in size, SAIL\_L stores parts of the routing table in the main memory. This however degrades the lookup performance significantly when it needs to access the main memory \cite{asai2015poptrie}. It is noteworthy that main memory access latency has remained very high (around $50$ CPU cycles) compared to CPU cache. In this paper, we propose a variant of SAIL\_L called SAIL-BM which reduces the memory consumption significantly. For instance, SAIL-BM consumes $4.97$ MB for the same example routing table $734K$ routes. This enables SAIL-BM to store the entire routing table in the CPU cache. Again the computation cost of SAIL-BM is comparable to that of SAIL\_L. This is why SAIL-BM can perform very fast routing table lookup without needing to access the main-memory.

The remaining of the paper proceeds as follows. In Section II, we describe how SAIL-BM represents a routing table with arrays and bitmaps. The Section III describes the routing table lookup algorithm. Section IV discusses the implementation and Section V shows the experimental results. Section VI presents the previous works and Section VII concludes the paper.

\begin{figure*}[!t]
	
	\begin{subfigure}[t]{0.2\textwidth}
		\centering
		\includegraphics[width=3.5cm]{routing_table}
		\caption{Routing table}\label{routingtable}
	\end{subfigure}%
	~ 
	\begin{subfigure}[t]{0.3\textwidth}
		\centering
		\includegraphics[width=5.5cm]{treewopush}
		\caption{Binary tree generated from the routing table}\label{treewopush}
	\end{subfigure}%
	~ 	
	\begin{subfigure}[t]{0.5\textwidth}
		\centering
		\includegraphics[width=9cm]{tree}
		\caption{Binary tree after \textit{level-pushing}. Solid nodes in level $1-2$ are pushed to level $3$; solid nodes in level $4-5$ are pushed to level $6$; solid nodes in level $7-8$ are pushed to level $9$}\label{binarytree}
	\end{subfigure}%
	
	\begin{subfigure}[t]{.66\textwidth}
		\centering
		\includegraphics[width=12cm]{array}
		\caption{$N$ and $C$ array are constructed based on the nodes in level $3, 6$ and $9$ (as described in SAIL \cite{yang2014guarantee}).}\label{array}
	\end{subfigure}%	
	~ 	
	\begin{subfigure}[t]{0.34\textwidth}
		\centering
		\includegraphics[width=6cm]{chunk}
		\caption{$C_6$ in Figure \ref{array} is encoded with bitmap and a revised $C_6$ where all the zero entries are eliminated}\label{chunk}
	\end{subfigure}%	
	\caption{SAIL-BM arrays are constructed from the routing table}\label{tree}
\end{figure*}

\section{Proposed Approach} 
Similar to SAIL\_L, SAIL-BM also splits a routing table into three levels: level 16, level 24 and level 32. It then represents those levels with arrays and bitmaps. This section shows how SAIL-BM represents a routing table with arrays and bitmaps. Here however we explain the approach with a simpler example where the routing table is split into three levels: level 3, level 6 and level 9.

Figure \ref{routingtable} shows an example routing table. The routing table is then represented as a binary tree as in the Figure \ref{treewopush}. Here all the nodes that have a next hop are called solid nodes. All the nodes that do not have a next hop are called internal nodes. We then construct the binary tree in Figure \ref{binarytree} by \textit{level-pushing}.  Here all the solid nodes in level $1-2$ are pushed to level $3$; all the solid nodes in level $4-5$ are pushed to level $6$, and solid nodes in level $7-8$ are pushed to level $9$. This is called \textit{level-pushing} which was initially proposed in SAIL\_L.
We then represent the level 3, 6 and 9 with nexthop ($N$) and chunk ID array ($C$) as in SAIL \cite{yang2014guarantee}. Let us use $N_i$ and $C_i$ to represent $N$ and $C$ of level $i$. There can be at most $8 (2^3)$ nodes in level $3$ in Figure \ref{binarytree}. This is why, the size of both $N_3$ and $C_3$ is $8$. $N_3$ and $C_3$ are constructed based on the solid nodes in level $3$ of Figure \ref{binarytree}. $N_3$ contains the next-hop in level 3. On the other hand, $C_3[i]$ indicates if there is a longer prefix exist than the prefix corresponding to $N_3[i]$. $C_3[i] = 0$ indicates that there is no longer prefix exist. Again $C_3[i] > 0$ indicates that there is a longer prefix exists and the longer prefix will be found in chunk $C_3[i]$. As the size of a chunk is fixed ($2^3$  in this case), chunk ID enables us to locate the longer prefix in the $N_6$. 

Let us assume that we want to lookup an IP address $11001011b$ in the routing table. It proceeds as follows: It extracts $0$-th to $2$-th bit (three leftmost bit) from the IP address ($6$ in this case). $N_3[6]=P2$ indicates that $P2$ is the potential next-hop for this IP address. However $C_3[6]=3$ indicates that there might be a longer prefix exist for the IP address. The longer prefix will be found in chunk 3. SAIL-BM then extracts $3$-th bit to $5$-th bit from the IP address ($2$ in this case). This is why, the longer prefix will be found in $N_6[(3-1)*8+2]$. $N_6[18]=P4$ indicates that $P4$ is the potential next-hop for the IP address. As $C_6[18]=0$ indicates that there is no longer-prefix exist for this IP address, thus $P4$ is the actual next-hop.

\textbf{Compress $C_6$ with the aid of bitmaps:} $C_6$ in Figure \ref{array} contains the chunk IDs for level $9$. However most of the entries of the array remain $0$ in practice. SAIL-BM uses bitmap to compress the array. Here the bitmap encodes the indices which are not zero. This allows us to eliminate all the zero entries from $C_6$. Figure \ref{chunk} shows how $C_6$ in Figure \ref{array} can be compressed with the aid of a chunk array $CK_6$. Here $CK_6$ contains a bitmap and start index for each chunk. Bitmap encodes the indices which are not zero in the chunk. Start index on the other hand shows where the chunk starts from in the revised $C_6$ (in Figure \ref{chunk}). For instance, $CK_6[1].bitmap=00110000$ indicates that the chunk $2$ contains two elements and the elements are at the index $4$ and $5$ inside the chunk. Again, $CK_6[1].start\_index=1$ indicates that the chunk $2$ start from $C_6[1]$ in the revised $C_6$ in Figure \ref{chunk}. It is noteworthy that Poptrie \cite{asai2015poptrie} also use similar scheme to store population count as a bitmap.

\begin {table}[H]
\caption {Data structures for SAIL-BM} \label{symbols} 
\begin{center}
	\begin{tabular}{ | m{2.5cm} | m{5cm} |}
		\hline $def\_nh$ & Default next-hop (level 0)\\		
		\hline $N_{16}$ & Next-hop array for level $16$\\
		\hline $C_{16}$ & Chunk ID array for level $16$\\
		\hline $N_{24}$ & Next-hop array for level $24$\\
		\hline $CK_{24}$ & Chunk array for level $24$\\		
		\hline $C_{24}$ & Chunk ID array for level $24$ where zero entries are eliminated\\
		\hline $N_{32}$ & Next-hop array for level $32$\\
		\hline $CK\_SZ$ & Chunk Size ($256$ in this case)\\						
		\hline 
	\end{tabular} 	
\end{center}
\end {table}

\begin{lstlisting}[language=C, caption=Chunk structure]
struct chunk {
//256-bit bitmap.
u64 bitmap[4];
//Index to C24 where the chunk is started.
u64 start_index[4];
};
\end{lstlisting}

\section{Lookup Algorithm}
This section describes how SAIL-BM performs routing table lookup in an IPv4 routing table. An IP address in IPv4 is $32$ bit long. This is why, a prefix can be up to $32$ bit long. SAIL-BM represents the routing table as in Figure \ref{tree}. It however pushes all the solid nodes in level $1-15$ to level $16$; solid nodes in level $17-23$ are pushed to level $24$; and solid nodes in level $25-31$ are pushed to level $32$. Table \ref{symbols} shows the data structure used by SAIL-BM. It is noteworthy that there is no $C_{32}$. As level $32$ is the last level, it does not need $C_{32}$. Here the size of both $N_{16}$ and $C_{16}$ is $65536$ ($2^{16}$). Again the size of each chunk in level $24$ and $32$ is $256$ ($2^8$). $CK_{24}$ is a chunk array where each chunk consist of a bitmap and start index. As the size of each chunk is $256$, the size of the bitmap needs to be $256$. The chunk structure is shown in Listing 1. Note that instead of having just one start index for the whole chunk, we divide the chunk into 4 parts and save start index for each part.

\begin{algorithm}
	\caption{Routing Table Lookup}\label{euclid}	
	\textbf{Input}: Destination IP address of the packet $ip$ \\
	\textbf{Output}: Next-hop $nexthop$					
	\begin{algorithmic}[1]				
		\Procedure{Lookup}{$ip$}
		\State $nexthop \gets def\_nh$
		\State /*Extract 16 bits from $ip$ */
		\State $idx \gets ip \  >> \ 16$
		\If {$N_{16}[idx] \ != \  0$}
		\State $nexthop \gets N_{16}[idx]$
		\EndIf				
		\If {$C_{16}[idx] \ != \  0$}		
		\State /*Extract bit 17-24 from $ip$ */		
		\State $ck\_off \gets ((ip \ \& \ 65280)>>8)$
		\State $ck\_id \gets C_{16}[idx]$
		\State $idx24 \gets (ck\_id - 1)*CK\_SZ + ck\_off$		
		\Else
		\State {return $nexthop$}	
		\EndIf		
		\If {$N_{24}[idx24] \ != \  0$}
		\State $nexthop \gets N_{24}[idx24]$
		\EndIf
		\State /*Check if there is a chunk in level $32$ for this IP*/
		\State $part\_idx \gets \frac{ck\_off}{64}$
		\State $part\_off \gets ck\_off \ \% \ 64$
		\State $bitmap \gets CK_{24}[ck\_id-1].bitmap[part\_idx]$
		\State $idx \gets CK_{24}[ck\_id-1].start\_index[part\_idx]$
		\If {$bitmap \ \& \ (1ULL << part\_off)$}		
		\State /*Calculate the index to $C_{24}$*/
		\State $i \gets idx + BITCOUNT(bitmap, part\_off)$
		\State /*Extract 8 bits from the MSB of $ip$ and calculate index to $N_{32}$*/		
		\State $idx32 \gets (C_{24}[i] - 1)*CK\_SZ + ip \  \& \ 255$		
		\Else
		\State {return $nexthop$}	
		\EndIf
		\If {$N_{32}[idx32] \ != \  0$}
		\State $nexthop \gets N_{32}[idx32]$
		\EndIf		
		\State {return $nexthop$}
		\EndProcedure
		
		\State /*counts the number of $1$s in left-most $n$ bits of $X$*/
		\Procedure{bitcount}{$X, n$}		
		\State {return $POPCNT(((1ULL << n) - 1) \ \& \ X)$}
		\EndProcedure
		
	\end{algorithmic}
\end{algorithm}

Algorithm 1 shows the look up process in SAIL-BM. It starts by initializing $nexthop$ to the default route (Line $2$). It then extracts 16 bits from the LSB of the IP address. This will be used as the index to $N_{16}$ and $C_{16}$. If $N_{16}[i] > 0$, SAIL-BM will update the $nexthop$ (Line $5$-$6$). If $C_{16}$ contains a valid chunk ID for the IP address, then SAIL-BM will further calculate the index to $N_{24}$; otherwise it will return the $nexthop$ (Line $7$-$13$). SAIL-BM then checks if $N_{24}[i] > 0$; if yes, then it updates the $nexthop$ (line $14$-$15$). It now needs to check if there is chunk in level $32$ for this IP address. It does so by checking the bitmap. Note that a chunk (in Listing 1) can be divided into $4$ parts. It first finds the part and the offset inside the part for the IP address (Line $17-18$). Line $21$ checks if there is a chunk for this IP address in level $32$; if yes, it then calculates the index to $C_{24}$ (Line $23$). Note that it uses $POPCNT$ instruction \cite{fog2011instruction} to count number of bits set to $1$. Most of the modern  $X86$ processor supports $POPCNT$ instruction. This allows us to convert the bitmap back to offset. Finally it calculates the index to $N_{32}$ in Line $25$. It is noteworthy that SAIL-BM does not need to backtrack in order to find the longest-prefix match. It iterates level $16$, $24$ and $32$ in order and updates $nexthop$ in each step. This is why $nexthop$ will contain the actual next-hop according to the LPM algorithm.

  
\section{SAIL-BM Implementation} 
We have implemented SAIL-BM in Linux kernel. The implementation contains $1195$ lines of $C$ code. We have made the source code available as a patch \cite{sailpatch}. This patch has been produced with Linux kernel $4.19.0$-$rc6+$. It is noteworthy that in actual implementation, $N_{16}$, $N_{24}$ and $N_{32}$ contain next-hop indices instead of actual next-hops. This allows us to use just one byte instead of a pointer ($8$ bytes in a $64$-bit machine).

\textbf{Incremental Update}: SAIL-BM supports incremental update. It stores prefix length for each prefix. This is why it also maintains an array $P_{16}$ which stores the prefix length of each prefix in $N_{16}$. In the same way, it maintains array $P_{24}$  and $P_{32}$ which store prefix lengths corresponding to prefixes in $N_{24}$ and $N_{32}$ respectively. $P_{16}$, $P_{24}$ and $P_{32}$ are only used for route insertion and route deletion. They are not used in routing table lookup. The details of the route insertion and route deletion will be found in our implementation.

\textbf{Lockless routing table lookup}: SAIL-BM needs to synchronize between routing table lookup and routing table update. Here we have used RCU (Read-Copy-Update) \footnote{https://lwn.net/Articles/262464/} to accomplice this. RCU is an alternative to \textit{reader-writer} locking. Here the reader-side does not need to hold any lock. This improves the lookup (\textit{reader}-side) performance  significantly because it does not need to hold any lock. On the other hand, routing table update (\textit{writer}-side) defers all the modification until all the routing table lookups are finished. It is noteworthy that LC-trie in Linux kernel also uses RCU for routing table lookup. RCU is mainly used when a data structure is more frequently read than updated. As a routing table is more frequently looked up than updated, it is often implemented as an RCU.   




%\begin {table}[H]
%\caption {CPU cache usage of SAIL\_L} \label{cachesaill} 
%\begin{center}
%	\begin{tabular}{ | m{2.5cm} | m{2.5cm} | m{2.5cm} |}
%		\hline Array & Length & Size\\		
%		\hline $N_{16}$  & 65536 & 0.0625MB\\
%		\hline $C_{16}$ & 65536 & 0.125MB\\
%		\hline $N_{24}$ & 6400000 & 6.12MB\\
%		\hline $C_{24}$ & 6400000 & 12.24MB\\
%		\hline $N_{32}$ & 256000000 & 244.14MB\\
%		\hline 
%	\end{tabular} 	
%\end{center}
%\end {table}

 \section{Evaluation}
Currently Linux XDP uses LC-trie for routing table lookup. However the lookup performance of SAIL\_L is significantly faster than that of LC-trie \cite{asai2015poptrie, yang2014guarantee}. This is why this paper compares the performance of SAIL-BM to that of SAIL\_L. Here we evaluate SAIL-BM with a routing table of a backbone router of an ISP in United States. The routing table snapshot is taken on Nov. $1$, $2018$. We have made the routing tables available \cite{fibbackbone}. The routing table has $734,362$ prefixes and $128$ unique next-hops. The routing table only contains IPv4 prefixes. We have obtained the routing table from Route Views \cite{routeviews} projects. The Route Views project has several BGP message collectors in several locations across the Internet. The collectors establishes a peering relationship with different backbone routers. That is how they receive BGP messages from those routers. Here we have used the collector in \text{Chicago}.

\subsection{Experimental Setup}
The experiment is conducted on a laptop running the our updated Linux kernel $4.19.0$-$rc6+$. The laptop however does not have many physical network interfaces needed to emulate a backbone router. This is why we create $32$ virtual network interfaces (\textit{veth}\footnote{http://man7.org/linux/man-pages/man4/veth.4.html} kernel module) and use them as the next-hop of the routing table. The laptop has an Intel(R) Core (TM) i7-4510U processor (2GHz, $4$ MB L-$3$ cache) and 16 GB DRAM. The processor here has two physical cores (i.e. when the \textit{hyper-threading}\footnote{https://en.wikipedia.org/wiki/Hyper-threading}  is disabled). It is noteworthy that \textit{hyper-threading} results in unnecessary cache thrashing. This is why we disable hyper-threading before conducting the experiment.  We also disable \textit{frequency-scaling}\footnote{https://en.wikipedia.org/wiki/Dynamic\_frequency\_scaling} of the CPU in order to get a deterministic performance. 

\begin {table}[H]
\caption {Comparison between SAIL\_L and SAIL-BM in memory consumption} \label{memory} 
\begin{center}
	\begin{tabular}{ | m{1cm} | m{1.5cm} | m{1.5cm} | m{1.5cm} | m{1.5cm} |}
		\hline & \multicolumn{2}{|c|}{SAIL\_L} & \multicolumn{2}{|c|}{SAIL-BM} \\
		\hline Array & Length & Size & Length & Size\\
		\hline $N_{16}$  & $65536$ & $64$KB & $65536$ & $64$KB\\
		\hline $C_{16}$ & $65536$ & $128$KB & $65536$ & $128$KB\\
		\hline $N_{24}$ & $4995328$ & $4.76$MB & $4995328$ & $4.76$MB\\
		\hline $CK_{24}$  &  & & $365$ & $22.81$KB\\
		\hline $C_{24}$ & $4995328$ & $19.05$MB & $365$ & $1.42$KB\\
		\hline $N_{32}$ & $93440$ & $91.25$KB & $93440$ & $91.25$KB\\
		\hline Total &  & $24.08$MB &  & $4.97$MB\\		
		\hline 
	\end{tabular} 	
\end{center}
\end {table}

\subsection{Memory consumption} 
Table \ref{memory} compares the memory consumption between SAIL\_L and SAIL-BM for the example routing table \cite{fibbackbone} with $734$K routes. Here the main difference arises from $C_{24}$. The size of $C_{24}$ in SAIL\_L is $19.05$MB whereas the size of $C_{24}$ in SAIL-BM is only $1.42$KB. The reason for this is as follows. It has found that more than $98.5\%$ routes in backbone routers are $0\sim24$ bit long \cite{bgpreport}. There are very few routes with length $25\sim32$ in a backbone routers. This is because a backbone router is very less likely to have a direct link to an end host. This is why very few chunks are needed in level $32$. As a result, in SAIL\_L, most of the entries in $C_{24}$ remains zero. SAIL-BM eliminates all the zero entries in $C_{24}$ by having an additional $CK_{24}$. This reduces the memory consumption of SAIL-BM significantly. This allows SAIL-BM to store the whole routing table in the CPU cache. On the other hand, memory consumption of SAIL\_L often exceeds typical CPU cache size. This is why it stores part of the routing table in main memory which often decrease the lookup speed significantly. \cite{asai2015poptrie}.


\begin{figure}[!t]
	\centering
	\includegraphics[width=9cm]{output2}
	\caption{Average number of CPU cycles needed per lookup (smaller is better)}\label{cpuvsplen}
\end{figure}

\subsection{Lookup performance}
\textbf{SAIL\_L vs SAIL-BM:} We measure the lookup performance based on the number of CPU cycles needed per lookup. We measure the CPU cycles using RDTSC instruction \cite{x86instructions}.  We have observed that the lookup performance depends on the level where the longest prefix is found. Both SAIL\_L and SAIL-BM looks up in level $16$ at first; it may then proceed to lookup in level $24$; and then it may proceed to lookup in level $32$. The lookup process does not need to backtrack to a lower level. This is why the lookup cost depends on the level where the longest prefix is found. The lookup cost will be smallest if the longest prefix is found in level $16$. Again the lookup cost will be the highest if the longest prefix is found in level $32$. Figure \ref{cpuvsplen} compares the lookup cost of SAIL\_L and SAIL-BM where the longest prefix is found in different levels. It shows that SAIL\_L perform slightly better than SAIL-BM when the longest prefix is found in level $16$ or level $24$. This is because SAIL-BM requires one more lookup than SAIL\_L in that case. If SAIL\_L find a prefix in $N_{16}$ or $N_{24}$, it immediately knows that it is the longest prefix. On the other hand, SAIL-BM needs to check $C_{16}$ or $C_{24}$ to determine if it is the longest prefix. However as both $C_{16}$ and $C_{24}$ resides in the CPU cache, the lookup cost is minimal. Again SAIL-BM outperforms SAIL\_L when the longest prefix is in level $32$. This is because SAIL\_L needs to access $C_{24}$ and $N_{32}$ which are stored in DRAM. On the other hand, $C_{24}$ and $N_{32}$ in SAIL-BM are very small and fit in a conventional CPU cache. This is why SAIL-BM does not need to access slow DRAM.

\textbf{LC-trie vs SAIL-BM:}The number of CPU cycles needed for LC-trie is abnormally high compare to that of SAIL-BM. LC-trie takes around $2800$ CPU cycles for our example routing table with $734,362$ routes whereas SAIL-BM takes around $40$ CPU cycles for the same IP addresses. In other words, SAIL-BM is around $70$X faster than LC-trie. Another drawback of LC-trie is that the lookup performance degrades with additional routes. On the other hand, the performance of SAIL-BM is relatively constant for backbone routers. Its performance does not vary with the size of a routing table as long as the most of the prefixes are $0-24$ bit long (typical scenario in backbone routers). 

 \section{Related Works}
 CPU based routing table lookup is a long standing research problem. The Lule\aa $\ $ algorithm \cite{degermark1997small} used bitmap to reduce the memory footprint of the routing tables. It however does not support incremental update and its lookup process requires many CPU cycles. LC-trie \cite{nilsson1999ip} is a classic algorithm adopted by Linux Kernel. LC-trie combines path compression and level-compression which leads to small memory footprint. It however exhibits poor the cache behavior resulting slow lookup speed. Tree Bitmap \cite{eatherton2004tree} is a variant of Lule\aa $\ $ algorithm. It supports incremental update. Tree Bitmap has been adopted by Cisco routers. It however also does not perform very well on general purpose CPU \cite{asai2015poptrie}. DIR-24-8-BASIC \cite{gupta1998routing} has been developed based on the observation that the most of the prefixes in backbone router are at most $24$ bit long. It maintains an array based on the first $24$ bits of the prefixes. This enables it to lookup IP address at $O(1)$. R\'etv\'ari et al. \cite{retvari2013compressing} proposed a routing table compression algorithm that can compress a routing table with $440$K routes with just $100$-$400$KB. However small routing table does not always results in good lookup performance. For instance, their routing table lookup consume $194$ CPU cycles resulting very slow lookup performance. 
  
 Recently SAIL \cite{yang2014guarantee}, DXR \cite{zec2012dxr} and Poptrie \cite{asai2015poptrie} have been proposed that exhibit impressive lookup performance. DXR represents a routing table with an array of address range. It then performs binary search in the array during lookup. Here the binary search is the primary bottleneck. Poptrie, on the other hand, divides a routing table into multiple levels. Here each level is represented by one or more nodes. Nodes in level are stored consecutively in the memory. This is why Poptrie exhibits cache-friendly behavior. Again each node in Poptrie is represented as a bitmap. This results in very small memory footprint. For instance, a routing table with $500$K routes consumes about $1.1$MB in Poptrie. The lookup process of Poptrie is as following: in each step, it needs to extract $6$ bits from the IP address which leads it to the next level. It visits each level until it finds a leaf. This is why an IPv4 address lookup may require visiting upto $6$ levels. This is however the main bottleneck of Poptrie. Visiting $6$ nodes and consuming CPU cycles in each node is computationally expensive. SAIL\_L, on the other hand, divides a routing table into three levels. Each level is represented by an array. Here the index to the array is directly found from the IP address. This is why SAIL\_L can perform very fast routing table lookup. SAIL\_L however consumes large memory space which often exceeds CPU cache. This is why SAIL\_L often needs to access DRAM which degrades its lookup performance significantly. This paper has presented a variant of SAIL called SAIL-BM. SAIL\_BITBAP consumes significantly smaller memory than SAIL\_L. Again the computation cost of SAIL-BM is comparable to that of SAIL\_L. This is why SAIL-BM can perform fast routing table lookup without needing to access DRAM.
 
  \section{Conclusion}

\bibliographystyle{abbrv} 
\begin{small}
\bibliography{hotnets17}
\end{small}

\end{document}

