% Copyright 2004 by Till Tantau <tantau@users.sourceforge.net>.
%
% In principle, this file can be redistributed and/or modified under
% the terms of the GNU Public License, version 2.
%
% However, this file is supposed to be a template to be modified
% for your own needs. For this reason, if you use this file as a
% template and not specifically distribute it as part of a another
% package/program, I grant the extra permission to freely copy and
% modify this file as you see fit and even to delete this copyright
% notice. 

\documentclass{beamer}
\usepackage{graphicx,multirow,booktabs,caption, array, listings}
\usepackage{times}  
\usepackage{url}
\usepackage{listings}
\usepackage{textcomp}
\usepackage{gensymb}
\usepackage{float}
\usepackage{caption} 
\usepackage{subcaption}
\usepackage{array}
\usepackage{listings}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}
\usepackage{amsmath}
\usepackage{color, colortbl}
\usepackage{cite}
\usepackage{siunitx}

\usepackage{array}


% There are many different themes available for Beamer. A comprehensive
% list with examples is given here:
% http://deic.uab.es/~iblanes/beamer_gallery/index_by_theme.html
% You can uncomment the themes below if you would like to use a different
% one:
%\usetheme{AnnArbor}
%\usetheme{Antibes}
%\usetheme{Bergen}
%\usetheme{Berkeley}
%\usetheme{Berlin}
%\usetheme{Boadilla}
%\usetheme{boxes}
%\usetheme{CambridgeUS}
%\usetheme{Copenhagen}
%\usetheme{Darmstadt}
%\usetheme{default}
%\usetheme{Frankfurt}
%\usetheme{Goettingen}
%\usetheme{Hannover}
%\usetheme{Ilmenau}
%\usetheme{JuanLesPins}
%\usetheme{Luebeck}
\usetheme{Madrid}
%\usetheme{Malmoe}
%\usetheme{Marburg}
%\usetheme{Montpellier}
%\usetheme{PaloAlto}
%\usetheme{Pittsburgh}
%\usetheme{Rochester}
%\usetheme{Singapore}
%\usetheme{Szeged}
%\usetheme{Warsaw}

\setbeamertemplate{footline}[frame number]{}
\setbeamertemplate{navigation symbols}{}


\title{Leveraging Domino to Implement RCP in a Stateful Programmable Pipeline}


\author{MD Iftakharul Islam, Javed I Khan}
% - Give the names in the same order as the appear in the paper.
% - Use the \inst{?} command only if the authors have different
%   affiliation.

\institute[Kent State University] % (optional, but mostly needed)
{
	Department of Computer Science\\
	Kent State University\\
	Kent, OH, USA.
}

\date[\today]{}

% - Use the \inst command only if there are several affiliations.
% - Keep it simple, no one is interested in your street address.

% This is only inserted into the PDF information catalog. Can be left
% out. 

% If you have a file called "university-logo-filename.xxx", where xxx
% is a graphic format that can be processed by latex or pdflatex,
% resp., then you can add a logo as follows:

% \pgfdeclareimage[height=0.5cm]{university-logo}{university-logo-filename}
% \logo{\pgfuseimage{university-logo}}

% Delete this, if you do not want the table of contents to pop up at
% the beginning of each subsection:


% Let's get started
\begin{document}
	
	\begin{frame}
		\titlepage
	\end{frame}
	
	\begin{frame}{Outline}
		\tableofcontents
		% You might wish to add the option [pausesections]
	\end{frame}
	
\section{Problem statement}

	\begin{frame}{Rate Control Protocol (RCP)}
		\begin{itemize}
			\item {RCP is a router-assisted congestion control mechanism where routers allocate a feedback rate to each flow.
				\begin{figure}[!t]
					\centering
					\includegraphics[width=7cm]{workflow}
					\caption{Workflow of RCP}\label{workflow}
				\end{figure}
				} 
	
			
		\end{itemize}
		
	\end{frame}
	
	\begin{frame}{RCP Implementation}
		\begin{itemize}
			\item Router assisted congestion control algorithms such as RCP \textit{[IWQoS 2005]}, XCP \textit{[SIGCOMM 2002]}  and NC-TCP \textit{[ICNP 2017]} exhibit very promising results compared to their host centric counterparts.
			\begin{itemize}
				\item Reduces flow completion time
				\item Reduces queuing delay
				\item Increases throughput and fairness
			\end{itemize}
			
			\item They however are very hard to implement in the router dataplane.
			
			 
			
			\item Currently there is no real deployment of RCP (to the best of our knowledge).
			
			\begin{itemize}
				\item There are several implementations of RCP based on RMT switch, FPGA, NPU and so on.
				
				\item None of them can achieve the line rate needed for a high-speed router (around $1$ billion packets per second)
			\end{itemize}
			
			\item This paper presents an implementation of RCP that can achieve line rate.
			
		\end{itemize}
		
	\end{frame}


\section{Challenges of line-rate RCP implementation}

	\begin{frame}{Challenges of line-rate RCP implementation}
		\begin{itemize}
			
			\item RCP calculates throughput based on queue occupancy, spare capacity, RTT and so on.
			
			\item This is why, RCP needs to maintain states in the router data plane.
			
			\item Recently programmable pipeline based RMT switches have emerged as an alternative to hardware switches. 
			\begin{itemize}
				\item Example: Barefoot Tofino, Cavium Xplaint, Brodcom Jericho, etc.
			\end{itemize}
			
			\item RMT switches maintain stateful memories such
			as counters, registers and meters in a centralized manner.
			
			\item Accessing those centralized memories from multiple pipelines
			requires explicit synchronization which is not scalable.
			
			\item This is why, RMT switches are not suitable for implementing RCP.
			
			\item Recently RCP has been developed in a Cavium Xplaint switch \textit{[NSDI, 2017]}, but it's not clear from the paper if it can achieve line-rate.			
			
		\end{itemize}
		
	\end{frame}


	\section{Stateful programmable pipeline and Domino programming language}
	\begin{frame}{Stateful Programmable Pipeline}
		\begin{itemize}

			\item Recently Domino \textit{[SIGCOMM, 2016]} presents stateful programmable pipeline where each pipeline stages keeps a local state.
			
			\item This is why, this approach does not need to access centralized states. So it does not need any synchronization.
			
			\item This paper presents an implementation of RCP for stateful programmable pipeline.
			
			\item We use Domino programming language to program the stateful programmable pipeline.
			
			\begin{itemize}
				\item It is noteworthy that Domino paper also stated that they have implemented RCP in router data plane, but their version of RCP lacked many essential elements of RCP.
			\end{itemize}
			
		\end{itemize}
		
	\end{frame}
	
	\begin{frame}{Stateful Programmable Pipeline}
\begin{figure}[!t]
	\centering
	\includegraphics[width=6cm]{abstract-router-model}
	\caption{Abstract router model based on stateful pipeline}\label{router-hardware}
\end{figure}
		\begin{itemize}
			
			\item Atoms are circuitry developed based on ALU, MUX and
			so on.
			
			\item Atoms in a pipeline can operate on a packet in parallel. 
			
			\item Each atom can process one packet in each clock cycle.
			
			\item This is why, the pipeline can output $1$ packet in every clock cycle.
			
			\item If the atoms run at $1$ GHz, then the pipeline can process $1$ billion packets per second.
			

		\end{itemize}
		
	\end{frame}


	\begin{frame}{Stateful Programmable Pipeline}
		\begin{itemize}
			
			\item Currently actual hardware does not exist. 
			
			\item But such stateful programmable pipeline can be realized using a VLIW processor.	
			
			\item Domino has provided SystemVerilog implementation of the hardware pipeline showing that it can run at $1$ GHz with a 32-nm standard-cell library.
			
			\item Domino programming language has been developed to develop program for stateful pipeline.
			
			\item Domino-compiler enables us to evaluate a Domino program without needing the actual hardware.
			
			\item Domino-compiler follows \textit{all-or-nothing} approach.

			\begin{itemize}
				\item \textbf{A program compiled by domino-compiler is guarented to run on the hardware at line-rate. If the program cannot run on the hardware at line-rate, the compilation fails}.
			\end{itemize}
			
		\end{itemize}
		
	\end{frame}

	\section{Congestion in a router data-plane}
	
	\begin{frame}{A look inside actual router hardware}
\begin{figure}[!t]
	\centering
	\includegraphics[width=5cm]{router-linecard}
	\caption{VLIW processor implements the parser and the ingress and the egress pipeline. Switching fabric works as the queue between the ingress and the egress pipeline.}\label{router-linecard}
\end{figure}
		
	\end{frame}
	

	\begin{frame}{Congestion in a router data-plane}
\begin{figure}[!t]
	\centering
	\includegraphics[width=7cm]{router-congestion}
	\caption{Congestion occurs due to the inability of the egress pipeline to forward packets at the incoming rate. This is why, RCP program would be executed in the egress pipeline.}\label{router-congestion}
\end{figure}
		
	\end{frame}
	
	\section{Rate calculation in RCP}



	\begin{frame}{Rate calculation in RCP}
		\begin{itemize}
			
			\item {RCP protocol calculates the feedback rate as:
				\begin{equation}\label{rcp1}R(t) = R(t-RTT_{a}) + \frac{\alpha.S - \beta.\frac{Q(t)}{RTT_{a}}}{\hat{N(t)}}\end{equation} where
				
				$R(t)$ = Feedback rate \\
				$Q(t)$ = Queue size (for the line card) \\
				$C$ = Link capacity \\
				$RTT_a$ = Running average of $RTT$\\
				$S$ = Spare capacity\\
				$\hat{N(t)}$ = Estimated number of flows \\
				$\alpha$ and $\beta$ are stability constants.
			}
			
			\item If there a spare capacity ($S > 0$), RCP increases the feedback rate
			
			\item If there a persistent queue ($Q(t) > 0$), RCP decreases the feedback rate.
						
		\end{itemize}
		
	\end{frame}
	
	\begin{frame}{Rate calculation in RCP}
		\begin{itemize}
			
			\item Calculating $\hat{N(t)}$ however is very expensive.
			
			\item RCP approximates $\hat{N(t)}$ as $\hat{N(t)} = \frac{C}{R(t-RTT_{a})}$
			
			\item This is why, the RCP control rate becomes
\begin{equation}\label{rcp2}
\begin{split}
R(t) & = R(t-RTT_{a}) + \frac{\alpha.S - \beta.\frac{Q(t)}{RTT_{a}}}{\frac{C}{R(t-RTT_{a})}} \\
& = R(t-RTT_{a}) [1 + \frac{\alpha.S - \beta.\frac{Q(t)}{RTT_{a}}}{C}]
\end{split}
\end{equation}

\item Here $RTT_a$ is used as the control interval. 

\item However we want the control interval to be smaller than the $RTT_a$ so that the router can react to the spare capacity and queuing delay sooner.

\item RCP scales the aggregate change by $\frac{T}{RTT_a}$ where $T$ is the desired control interval.

\item All these equations are presented in original RCP paper.
			
		\end{itemize}
		
	\end{frame}
	
	\begin{frame}{Rate calculation in RCP}
		\begin{itemize}
			
			\item {RCP control equation becomes:
				\begin{equation}\label{rcp3}
				R(t) = R(t-T) [1 + \frac{\frac{T}{RTT_a}.(\alpha.S - \beta.\frac{Q(t)}{RTT_{a}})}{C}]
				\end{equation} 				 
				}
				
				\item We set $T$ = $50$ ms
				\item We set $\alpha=1.0$ and $\beta=0.5$ (chosen based on RCP's stability analysis).
				 \item $RTT_a$ is calculated as $RTT_a = .98 * RTT_a + .02 * RTT_{packet}$ for each incoming packet \\
				 \item $S$ is calculated as $S = C - \frac{B}{1000T}$ MB where $B$ is the number of bytes received during last $T$ milliseconds.

		\end{itemize}
		
	\end{frame}

	\section{Implementation}
	
	\begin{frame}{Implementation}
		\begin{itemize}			
			
			\item We have implemented RCP using Domino (\textit{details are in the paper}).
			
			\item Domino has notion called \textit{packet transaction} where we implement the packet processing logic as if the program will be applied on the packet at once.
			
			\item Domino-compiler generates a dependency graph from the packet transaction and maps it to stateful pipeline stages (\textit{the dependency graph would be found in paper})
			
			\item {It is noteworthy that packet size, RTT, queue length, time, etc are made available to packet transaction as packet-header and meta-data as following:
		\begin{figure}[!t]
			\centering
			\includegraphics[width=5cm]{metadata}
		\end{figure}				
				}

		\end{itemize}

	\end{frame}	

	\begin{frame}{Implementation}
		\begin{itemize}			
			
			\item Packet parser is responsible for parsing the packet header and producing RTT, feedback rate and so on.
			\begin{itemize}
				\item RCP sender and receiver put those information in RCP header. 
				\item Domino does not parse the packet. It simply assumes that packet is already parsed.
				
				\item Packet parsing is rather trivial (can be defined by a P4 program) 
			\end{itemize}
			
			\item Time, queue length, packet size, etc are made available as a meta-data. 
			\begin{itemize}
				\item In-band Network Telemetry (INT) standard adopted by several switch manufacturers
			\end{itemize}
			
			\item Domino has $7$ predefined atoms. We need to provide the atom type in order to compile the Domino program. Here we use SUB atom to compile our program.		   
			
		\end{itemize}
			
	\end{frame}
	
	\begin{frame}{SUB atom}
\begin{figure}[!t]
	\centering
	\includegraphics[width=5cm]{atom.png}
	\caption{Atom used for compiling RCP. Here MUX is a multiplexer. RELOP is a relational operator $(>, <, ==, !=)$. $x$ is a state variable. ALU is the arithmetic logic unit. $pkt.f1$
		and $pkt.f2$ are packet fields. $Const$ is a constant operand.}\label{atom}
\end{figure}
	\end{frame}
	
	\begin{frame}{Implementation}
		\begin{itemize}			
			
			\item It has shown that SUB atom can run at $1$ GHz.
			
			\item Domino-compiler internally uses SKETCH tool to map between a domino program and the underlying atom 
			
			\item Domino compiler can compile our program with a $15$ stage pipeline where each pipeline stage contains $2$ SUB atoms.  
			
			\item This shows that RCP can be implemented on a VLIW processor at line-rate.
			
		\end{itemize}
		
	\end{frame}

	\section{Execution of the RCP program in a pipeline}

	\begin{frame}{Execution of the RCP program in a $15$-stage pipeline}
\begin{figure}[!t]
	\centering
	\includegraphics[width=6cm]{pipeline.PNG}
\end{figure}
	\end{frame}
	
	
	\begin{frame}{Execution of the RCP program in a $15$-stage pipeline}
		\begin{itemize}			
			
			\item Here grey boxes represents a stateful atom (SUB atom). 
			
			\item White boxes are stateless atom which can be realized by a simple ALU.
			
			\item It is noteworthy that a Domino program is mapped to the pipeline during compilation. This is why it results a very deterministic results (unlike super scaler or general purpose processors)
			
		\end{itemize}
		
	\end{frame}
	
\begin{frame}{}
	\centering \Large
	\Huge{Thank You}
\end{frame}
	
\end{document}


